import argparse
import glob
import os
import shutil

import tqdm
import mne
import numpy as np
from mp_dip.obci_readmanager.signal_processing.read_manager import ReadManager
from mp_dip.obci_readmanager.signal_processing.signal.data_generic_write_proxy import SamplePacket
from mp_dip.obci_readmanager.signal_processing.signal.data_raw_write_proxy import DataRawWriteProxy

from mp_dip.obci_readmanager.signal_processing.signal.info_file_proxy import InfoFileWriteProxy
from mp_dip.obci_readmanager.signal_processing.tags.tags_file_writer import TagsFileWriter


def normalize_channel_name(channel):
    montage = mne.channels.make_standard_montage('standard_1005')
    ch_nammes = montage.ch_names
    chnames_dict = {i.lower(): i for i in ch_nammes}
    if channel.lower() in chnames_dict.keys():
        return chnames_dict[channel.lower()]
    else:
        return channel

def organize_epileptic_tags(raw):
    label = 'Glica/i+fala wolna'
    tags_names_in_annotations = set([i['description'] for i in raw.annotations])
    if tags_names_in_annotations == {'0', '201', '202'}:
        start_annotations = [i for i in raw.annotations if i['description']=='201']
        end_annotations = [i for i in raw.annotations if i['description']=='202']
        assert len(start_annotations) == len(end_annotations)
        new_annotations = []
        for start, end in zip(start_annotations, end_annotations):
            new_annotation = start.copy()
            new_annotation['duration'] = end['onset'] - start['onset']
            new_annotation['description'] = label
            new_annotations.append(new_annotation)
        new_annotations = mne.Annotations(onset=[i['onset'] for i in new_annotations],
                                          duration=[i['duration'] for i in new_annotations],
                                          description=[i['description'] for i in new_annotations],
                                          )
        raw.set_annotations(new_annotations)
        return raw
    else:
        return raw


def save_tags_from_rm(rm, tags_target_filename):
    tags = rm.get_tags()
    tag_writer = TagsFileWriter(tags_target_filename)
    for tag in tags:
        tag_writer.tag_received(tag)
    tag_writer.finish_saving(0.0)


def main():
    parser = argparse.ArgumentParser(description="Converting EDF, CURRY, CNT to Svarog RAW.")
    parser.add_argument('files', nargs='+', metavar='file', help="path edf files")
    parser.add_argument('-r', action='store_true', help="Remove channel prefixes")
    parser.add_argument('-m', action='store_true', help="MASS dataset name normalisation")
    parser.add_argument('-o', '--outdir', nargs='?', type=str, help='Output directory')
    parser.add_argument('-s', '--scale', type=float, help=('override signal scaling'
                                                           ' data from nowowiejska has corrupt headers?'
                                                           ' Apply 0.000001'),
                        default=1.0)

    namespace = parser.parse_args()
    if len(namespace.files) == 1:
        files_to_work = glob.glob(namespace.files[0])
    else:
        files_to_work = namespace.files
    for file in tqdm.tqdm(files_to_work):
        formats = [
            mne.io.read_raw_edf,
            mne.io.read_raw_curry,
            mne.io.read_raw_cnt,
        ]
        for format in formats:
            try:
                raw = format(file, preload=True)
                break
            except:
                pass
        try:
            raw.drop_channels(['STI 014'])
        except ValueError:
            pass

        raw.rename_channels(normalize_channel_name)
        raw = organize_epileptic_tags(raw)

        rm = ReadManager.from_mne(raw)
        del raw
        if namespace.outdir is None:
            outfile_raw = os.path.splitext(file)[0] + '.raw'
            outfile_info = os.path.splitext(file)[0] + '.xml'
            outfile_tag = os.path.splitext(file)[0] + '.tag'
            if not os.path.exists(outfile_tag):
                save_tags_from_rm(rm, outfile_tag)
        else:
            os.makedirs(namespace.outdir, exist_ok=True)
            outfile_raw_base = os.path.splitext(os.path.basename(file))[0]+'.raw'
            outfile_info_base = os.path.splitext(os.path.basename(file))[0] + '.xml'
            outfile_raw = os.path.join(namespace.outdir, outfile_raw_base)
            outfile_info = os.path.join(namespace.outdir, outfile_info_base)
            tagfile = os.path.splitext(file)[0] + '.tag'
            tagfile_target = os.path.join(namespace.outdir, os.path.basename(tagfile))
            # copy tags over
            try:
                shutil.copyfile(tagfile, tagfile_target)
            except:
                save_tags_from_rm(rm, tagfile_target)

        info_proxy = InfoFileWriteProxy(outfile_info)
        params = rm.get_params()
        if namespace.r:
            for i, ch, in enumerate(params['channels_names']):
                params['channels_names'][i] = params['channels_names'][i].split()[1]

        if namespace.m:
            for i, ch, in enumerate(params['channels_names']):
                if params['channels_names'][i].startswith("EEG"):
                    params['channels_names'][i] = params['channels_names'][i].split()[1].split('-')[0]
        info_proxy.finish_saving(params)

        data_proxy = DataRawWriteProxy(outfile_raw, False, rm.get_param('sample_type'))
        samples = rm.get_microvolt_samples().T * namespace.scale

        for i, name in enumerate(rm.get_param('channels_names')):
            if name.startswith('Resp'):
                samples[:, i] *= 1e-6

        packet = SamplePacket(samples, np.linspace(0, 1, int(rm.get_param('number_of_samples'))))
        data_proxy.data_received(packet)
        data_proxy.finish_saving()


if __name__ == '__main__':
    main()
