import getpass
import os
import sys
import time

import subprocess
import argparse
import platform

def ping(host):
    """
    Returns True if host (str) responds to a ping request.
    Remember that a host may not respond to a ping (ICMP) request even if the host name is valid.
    """

    # Option for the number of packets as a function of
    param = '-n' if platform.system().lower()=='windows' else '-c'

    # Building the command. Ex: "ping -c 1 google.com"
    command = ['ping', param, '1', host]

    return subprocess.call(command) == 0


def check_machines(machines):
    machines_good = []
    for machine in machines:
        ping_good = ping(machine)
        job = subprocess.Popen(['ssh', machine, '''echo machine_good'''])
        returncode = job.wait(60)
        returncode_good = (returncode==0)
        if ping_good and returncode_good:
            machines_good.append(machine)
    return machines_good
    

def main():
    parser = argparse.ArgumentParser(description='''Running a command in this venv on multiple machines, accssible through certificate based ssh login.
    
    
    example:
    
     python multi_ssh_processing.py "decompose_raw -m none -mp mmp3 -o /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/mp" /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/*.raw
    ''')
    parser.add_argument('command', action='store', help='run this command with params using this python (should be accessible)', )
    parser.add_argument('files', nargs='+', metavar='file', help="files to work with")
    namespace = parser.parse_args()

    files_to_do = [os.path.abspath(os.path.expanduser(i)) for i in namespace.files]

    # fabrizzio taken down for maintenance
    # machines = '''amanda,athlon,beck,creeper,gabor,goedel,fabrizzio,Rad'''.split(',')

    # machines = '''amanda,athlon,beck,creeper,gabor,goedel,fabrizzio'''.split(',')
    # machines = '''beck,creeper,gabor,goedel,fabrizzio'''.split(',')
    machines = '''beck,creeper,gabor,fabrizzio'''.split(',') # todo goedel crashuje sie fit dipoles to atoms
    # machines = '''creeper,gabor'''.split(',') # todo goedel crashuje sie fit dipoles to atoms
    # machines = '''beck,gabor,fabrizzio'''.split(',') # todo goedel crashuje sie fit dipoles to atoms

    # machines = '''amanda,athlon,beck,creeper,gabor,goedel'''.split(',')
    # machines = '''amanda,athlon,beck,creeper,gabor'''.split(',')
    # goedel crashes for some reason during dipole fit
    # machines = '''amanda,athlon,beck,creeper,gabor,fabrizzio'''.split(',')
    # machines = '''athlon,beck,fabrizzio'''.split(',')
    machines = [i+'.zfb.fuw.edu.pl' for i in machines]
    machines = check_machines(machines)

    print('\n\n\n\n\n\n\nGood machines: ', machines)
    time.sleep(1)
    machine_jobs = dict([(i, None) for i in machines])  

    while True:
        for machine in machine_jobs.keys():
            job = machine_jobs[machine]
            if job is None:
                try:
                    file = files_to_do.pop()
                except IndexError:
                    continue
                # assume we are in a venv
                this_activate = os.path.join(os.path.dirname(sys.executable), 'activate')
                command_template = '''source {}; nice {} "{}"'''.format(this_activate, namespace.command, file)
                print("Running {} on {}".format(command_template, machine))
                # print("Files to do", files_to_do)
                print("machine jobs", machine_jobs)
                job = subprocess.Popen(['ssh', machine, command_template])
                machine_jobs[machine] = job
            else:
                try:
                    job.wait(1)
                    print("Job {} done on machine {}".format(machine_jobs[machine].args, machine))
                    # print("Files to do", files_to_do)
                    machine_jobs[machine] = None
                except subprocess.TimeoutExpired:
                    print("Job {} is working on machine {}".format(machine_jobs[machine].args, machine))
                    pass
        files_to_do_done = (len(files_to_do) == 0)
        all_jobs_are_none = all([i is None for i in machine_jobs.values()])
        if files_to_do_done and all_jobs_are_none:
            break


if __name__ == '__main__':
    main()
