import argparse
import glob
import os

from mp_dip.obci_readmanager.signal_processing.tags import tags_file_writer as tags_writer

import mne
from tqdm import tqdm


tag_convert_dict = {'Sleep stage 1': "1",
                    'Sleep stage 2': "2",
                    'Sleep stage 3': "3",
                    'Sleep stage ?': None,
                    'Sleep stage R': "r",
                    'Sleep stage W': "w"}

SLEEP_TAG_DEFS = [dict(name="4",
                       description="Stadium 4",
                       fillColor="afafaf",
                       outlineColor="ffffff",
                       outlineWidth="1.0",
                       outlineDash="",
                       keyShortcut="typed 4",
                       marker="0",
                       visible="1",
                       ),
                  dict(name="3",
                       description="Stadium 3",
                       fillColor="bfbfbf",
                       outlineColor="ffffff",
                       outlineWidth="1.0",
                       outlineDash="",
                       keyShortcut="typed 3",
                       marker="0",
                       visible="1",
                       ),
                  dict(name="2",
                       description="Stadium 2",
                       fillColor="cfcfcf",
                       outlineColor="ffffff",
                       outlineWidth="1.0",
                       outlineDash="",
                       keyShortcut="typed 2",
                       marker="0",
                       visible="1",
                       ),

                  dict(name="1",
                       description="Stadium 1",
                       fillColor="dfdfdf",
                       outlineColor="ffffff",
                       outlineWidth="1.0",
                       outlineDash="",
                       keyShortcut="typed 1",
                       marker="0",
                       visible="1",
                       ),
                  dict(name="r",
                       description="REM",
                       fillColor="efefef",
                       outlineColor="ffffff",
                       outlineWidth="1.0",
                       outlineDash="",
                       keyShortcut="typed r",
                       marker="0",
                       visible="1",
                       ),

                  dict(name="w",
                       description="WAKE",
                       fillColor="fefefe",
                       outlineColor="ffffff",
                       outlineWidth="1.0",
                       outlineDash="",
                       keyShortcut="typed w",
                       marker="0",
                       visible="1",
                       ),

                  dict(name="m",
                       description="musc",
                       fillColor="ffffff",
                       outlineColor="ffffff",
                       outlineWidth="1.0",
                       outlineDash="",
                       keyShortcut="typed m",
                       marker="0",
                       visible="1",
                       ),
                  ]


    # <defGroup name="pageTags">
    #   <tagItem name="4" description="Stadium 4" fillColor="ffffff" outlineColor="ffffff" outlineWidth="1.0" outlineDash="" keyShortcut="typed 4" marker="0" visible="1"/>
    #   <tagItem name="3" description="Stadium 3" fillColor="ffffff" outlineColor="ffffff" outlineWidth="1.0" outlineDash="" keyShortcut="typed 3" marker="0" visible="1"/>
    #   <tagItem name="2" description="Stadium 2" fillColor="ffffff" outlineColor="ffffff" outlineWidth="1.0" outlineDash="" keyShortcut="typed 2" marker="0" visible="1"/>
    #   <tagItem name="1" description="Stadium 1" fillColor="ffffff" outlineColor="ffffff" outlineWidth="1.0" outlineDash="" keyShortcut="typed 1" marker="0" visible="1"/>
    #   <tagItem name="r" description="REM" fillColor="ffffff" outlineColor="ffffff" outlineWidth="1.0" outlineDash="" keyShortcut="typed r" marker="0" visible="1"/>
    #   <tagItem name="w" description="WAKE" fillColor="ffffff" outlineColor="ffffff" outlineWidth="1.0" outlineDash="" keyShortcut="typed w" marker="0" visible="1"/>
    #   <tagItem name="m" description="MUSC" fillColor="ffffff" outlineColor="ffffff" outlineWidth="1.0" outlineDash="" keyShortcut="typed m" marker="0" visible="1"/>
    # </defGroup>


def main():
    parser = argparse.ArgumentParser(description="Converting EDF sleep annotation files from MASS to Svarog tags.")
    parser.add_argument('files', nargs='+', metavar='file', help="path edf files with annotations")
    parser.add_argument('-o', '--outdir', nargs='?', type=str, help='Output directory')

    namespace = parser.parse_args()
    if len(namespace.files) == 1:
        files_to_work = glob.glob(namespace.files[0])
    else:
        files_to_work = namespace.files
    for file in tqdm(files_to_work):
        annotations = mne.read_annotations(file)
        tags = []
        for annotation in annotations:
            start = annotation['onset']
            stop = start + annotation['duration']
            label = tag_convert_dict.get(annotation['description'], None)
            if label is not None:
                tag = {"type": "CHANNEL",
                       'channelNumber': -1,
                       'start_timestamp': start,
                       'end_timestamp': stop,
                       'name': label,
                       'desc': {},
                       }
                tags.append(tag)

        output_dir = namespace.outdir

        os.makedirs(output_dir, exist_ok=True)
        file_out_name_base = os.path.join(output_dir,
                                          os.path.splitext(
                                              os.path.basename(
                                                  file))[
                                              0])
        tags_file = file_out_name_base + '.tag'
        tags_file = tags_file.replace('Base', 'PSG')
        tag_writer = tags_writer.TagsFileWriter(tags_file,
                                                p_defs=SLEEP_TAG_DEFS)
        for tag in tags:
            tag_writer.tag_received(tag)
        tag_writer.finish_saving(0.0)



if __name__ == '__main__':
    main()
