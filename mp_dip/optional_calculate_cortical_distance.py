import glob
import os
import sys

import argparse

import numpy as np
import pandas

from mp_dip.utils.cortical_distance import calculate_cortical_distance
from mp_dip.utils.utils import get_mp_book_paths

def main():
    # np.set_printoptions(precision=3)
    np.set_printoptions(suppress=True)
    np.set_printoptions(linewidth=120)
    parser = argparse.ArgumentParser(description="Analysing MP decompositions.")
    parser.add_argument('-d', '--mp-dir', nargs='?', type=str, help='MP Output directory')
    parser.add_argument('-m', '--montage', action='store', help='Montages: none, ears, transverse', default='none')
    parser.add_argument('-mp', '--mp-type', action='store', help='mp type: smp, mmp3', default='smp')
    parser.add_argument('-rc', '--ref-channel', action='store', help='electrode position used for reference, used only for dipole fitting in 1005 standard, actual physical electrode, not digital re-reference, can be multiple channels seperated by ;. Special case - average', default='average')
    parser.add_argument('files', nargs='+', metavar='file', help="path to *.raw")
    parser.add_argument('-u', '--unique-file-names', action='store', type=str,
                        default='y',
                        help='are raw filenames unique? If no MP folder will contain subfolders')

    namespace = parser.parse_args()
    if len(namespace.files) == 1:
        files_to_work = glob.glob(namespace.files[0])
    else:
        files_to_work = namespace.files
    if namespace.mp_type not in ['mmp3', 'mmp1']:
        print("Unsupported mp type for dipole fitting")
        sys.exit(3)
    mp_dir = namespace.mp_dir
    montage = namespace.montage
    mp_type = namespace.mp_type
    for raw_file in files_to_work:
        path_to_book, path_to_book_params = get_mp_book_paths(mp_dir, raw_file,
                                                                      montage, mp_type,
                                                                      unique_file_names=namespace.unique_file_names=='y')
        save_path = os.path.join(os.path.dirname(path_to_book),
                                 'dipole_fits',
                                 os.path.basename(path_to_book) + '_dipole_fits.pickle')
        print("Reading pickle")
        atoms_original_sort = pandas.read_pickle(save_path)
        print("Calculating")
        atoms_original_sort = calculate_cortical_distance(atoms_original_sort)
        print("Saving")
        atoms_original_sort.to_pickle(save_path)
        print("Done")





if __name__=='__main__':
    main()
    # python .\fit_dipoles_to_atoms.py -d D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\mp -m none -mp mmp3 D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\*.raw
    # python .\fit_dipoles_to_atoms.py -d /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/mp -m none -mp mmp3 /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/*.raw

    # nohup python multi_ssh_processing.py "fit_dipoles_to_atoms -d /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/mp -m none -mp mmp3" /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/*.raw &
    # python optional_calculate_cortical_distance.py fit_dipoles_to_atoms -d /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_stare_do_testu/converted/mp -m none -mp mmp3 /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_stare_do_testu/converted/*.raw &
