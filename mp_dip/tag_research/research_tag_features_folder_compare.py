import glob
import os
from collections import defaultdict

import numpy as np
import argparse

import pandas as pd
from scipy.stats import ks_2samp, mannwhitneyu, ttest_ind
from tqdm import tqdm
import pylab as pb
import seaborn

from mp_dip.tag_research.research_tag_features import LiterateDict

FEATURES_TO_COMPARE = ['phase', 'frequency', 'width', 'snr', 'amplitude', 'dip_distance_to_cortex_voxel', 'dip_gof']

technical_to_literate_dict = LiterateDict({'Spindle (wrzeciono)': "spindles",
                                           "amplitude": 'Amplitude [μV]',
                                           "frequency": 'Frequency [Hz]',
                                           "width": 'Duration [s]',
                                           "snr": 'SNR [dB]',
                                           'dip_gof': "GoF [%]",
                                           'dip_distance_to_cortex_voxel': 'DtC [mm]',
                                           'phase': 'Phase [rad]'

})

technical_to_literate_dict = LiterateDict({'Spindle (wrzeciono)': "spindles",
                                           "amplitude": 'Amplitude',
                                           "frequency": 'Frequency',
                                           "width": 'Duration',
                                           "snr": 'SNR',
                                           'dip_gof': "GoF",
                                           'dip_distance_to_cortex_voxel': 'DtC',
                                           'phase': 'Phase'

})


def compare_per_user_pval(files_to_work1, label1, files_to_work2, label2, output_dir, test):
    data = defaultdict(list)
    filenames_2 = [os.path.basename(i) for i in files_to_work2]
    for file in tqdm(files_to_work1, 'creating pvals per recording'):
        filename1 = os.path.basename(file)
        try:
            pair_index = filenames_2.index(filename1)
        except ValueError:
            print(file, 'doesnt have a pair!!! skipping')
            continue

        file_paired = files_to_work2[pair_index]

        all_stuff1 = defaultdict(list)
        macroatoms = pd.read_pickle(file)
        for macroatom, label in macroatoms:
            # atom_best = np.where(macroatom.channel_name == 'C3')[0][0]
            atom_best = np.argmax(macroatom.amplitude)
            atom = macroatom.iloc[atom_best]
            all_stuff1[label].append(atom)

        all_stuff2 = defaultdict(list)
        macroatoms = pd.read_pickle(file_paired)
        for macroatom, label in macroatoms:

            # atom_best = np.where(macroatom.channel_name == 'C3')[0][0]
            atom_best = np.argmax(macroatom.amplitude)
            atom = macroatom.iloc[atom_best]
            all_stuff2[label].append(atom)

        try:
            len_of_stuff1 = min([len(i) for i in all_stuff1.values()])
            len_of_stuff2 = min([len(i) for i in all_stuff2.values()])
        except ValueError:
            len_of_stuff1 = 0
            len_of_stuff2 = 0

        is_empty = (len_of_stuff1 == 0) or (len_of_stuff2 == 0)

        if is_empty:
            print(filename1, 'one of the pair items doesnt have spindles at all')
            continue
        data['filename'].append(filename1)
        for feature_label in FEATURES_TO_COMPARE:
            for label in np.unique(list(all_stuff1.keys()) + list(all_stuff2.keys())):
                atoms1 = all_stuff1[label]
                df1 = pd.DataFrame(atoms1)

                atoms2 = all_stuff2[label]
                df2 = pd.DataFrame(atoms2)

                feature1 = df1[feature_label].values
                feature2 = df2[feature_label].values

                mask_finite_1 = np.isfinite(feature1)
                mask_finite_2 = np.isfinite(feature2)

                feature1 = feature1[mask_finite_1]
                feature2 = feature2[mask_finite_2]

                if (len(feature1) > 0) and (len(feature2) > 0):
                    _, pval = test(feature1, feature2)
                else:
                    pval = np.nan
                data[feature_label].append(pval)

    df = pd.DataFrame.from_dict(data)
    df.to_csv(os.path.join(output_dir, 'pvalues_per_recording_{}_vs_{}.csv').format(label1, label2))

    fig = pb.figure(figsize=(7, 3))
    ax = fig.add_subplot(111)
    # df.boxplot(ax=ax)
    df_to_plot = df.rename(columns=technical_to_literate_dict)
    seaborn.stripplot(data=df_to_plot, ax=ax, size=2)
    seaborn.boxplot(data=df_to_plot, ax=ax, color='white', showfliers = False)
    ax.axhline(0.05, linestyle='--', color='lightgray')
    fig.savefig(os.path.join(output_dir, 'pvalues_per_recording_{}_vs_{}.png').format(label1, label2), dpi=300)
    
    for feature_label in FEATURES_TO_COMPARE:
        print(technical_to_literate_dict[feature_label],
              'p_val < 0.05', sum(df[feature_label] < 0.05), 'out of', len(df[feature_label]) )

    with open(os.path.join(output_dir, 'pvalues_summary.txt'), 'w') as summary:
        for feature_label in FEATURES_TO_COMPARE:
            out = (technical_to_literate_dict[feature_label] + ' p_val < 0.05 ' + str(sum(df[feature_label] < 0.05))
                   + ' out of ' + str(len(df[feature_label])) + '\n')
            summary.write(out)


def main():
    np.set_printoptions(precision=3)
    np.set_printoptions(suppress=True)
    np.set_printoptions(linewidth=120)
    parser = argparse.ArgumentParser(description="Analysing tags (from MMP) params.")
    parser.add_argument('folder1', type=str, help="path to folder with pickle files")
    parser.add_argument('label1', type=str, help="Label for folder 1")
    parser.add_argument('folder2', type=str, help="path to folder with pickle files")
    parser.add_argument('label2', type=str, help="label for folder2")

    namespace = parser.parse_args()

    files_to_work1 = glob.glob(os.path.join(namespace.folder1, '*.pickle'))
    files_to_work2 = glob.glob(os.path.join(namespace.folder2, '*.pickle'))
    output_dir = os.path.join(os.path.dirname(namespace.folder1), 'mmp_params_{}_{}_compare'.format(namespace.label1,
                                                                                          namespace.label2))

    for test_name, test in [['mannwhitney', mannwhitneyu], ['ks', ks_2samp], ['ttest', ttest_ind]]:
        output_dir_pval = os.path.join(output_dir, 'test_{}'.format(test_name))
        os.makedirs(output_dir_pval, exist_ok=True)
        compare_per_user_pval(files_to_work1, namespace.label1, files_to_work2, namespace.label2, output_dir_pval, test)

    all_stuff1 = defaultdict(list)
    for file in tqdm(files_to_work1):
        macroatoms = pd.read_pickle(file)
        for macroatom, label in macroatoms:
            atom_best = np.argmax(macroatom.amplitude)
            atom = macroatom.iloc[atom_best]
            all_stuff1[label].append(atom)

    all_stuff2 = defaultdict(list)
    for file in tqdm(files_to_work2):
        macroatoms = pd.read_pickle(file)
        for macroatom, label in macroatoms:
            atom_best = np.argmax(macroatom.amplitude)
            atom = macroatom.iloc[atom_best]
            all_stuff2[label].append(atom)

    for feature_label in tqdm(FEATURES_TO_COMPARE):

        for label in np.unique(list(all_stuff1.keys()) + list(all_stuff2.keys())):

            atoms1 = all_stuff1[label]
            df1 = pd.DataFrame(atoms1)

            atoms2 = all_stuff2[label]
            df2 = pd.DataFrame(atoms2)

            energy1 = df1['modulus'].values ** 2
            energy2 = df2['modulus'].values ** 2

            feature1 = df1[feature_label].values
            feature2 = df2[feature_label].values

            mask_finite_1 = np.isfinite(feature1)
            mask_finite_2 = np.isfinite(feature2)

            feature1 = feature1[mask_finite_1]
            feature2 = feature2[mask_finite_2]

            energy1 = energy1[mask_finite_1]
            energy2 = energy2[mask_finite_2]

            fig = pb.figure(figsize=(3, 3))
            if feature_label == 'dip_distance_to_cortex_voxel':
                pb.hist([np.abs(feature1), np.abs(feature2)], bins=100, label=[namespace.label1, namespace.label2])
            else:
                pb.hist([feature1, feature2], bins=100, label=[namespace.label1, namespace.label2])
            H, pval = ks_2samp(feature1, feature2)
            # pb.title(' '.join([label, feature_label]))
            os.makedirs(output_dir, exist_ok=True)
            pb.legend()
            fig.tight_layout()
            pb.savefig(os.path.join(output_dir, '{}_{}_density_{}_weighted_{}.png'.format(label, feature_label, False, False)), dpi=300)
            pb.close()

            fig = pb.figure(figsize=(3.4, 3.4))
            # pb.hist([feature1, feature2], bins=100, label=[namespace.label1, namespace.label2], density=True)
            if feature_label == 'dip_distance_to_cortex_voxel':
                pb.hist([np.abs(feature1), np.abs(feature2)], bins=100, label=[namespace.label1, namespace.label2], density=True)
            else:
                pb.hist([feature1, feature2], bins=100, label=[namespace.label1, namespace.label2], density=True)

            pb.ylabel('Density')
            pb.xlabel(technical_to_literate_dict[feature_label])

            # pb.title(' '.join([label, feature_label]))
            os.makedirs(output_dir, exist_ok=True)
            # pb.legend(loc='lower left')
            fig.tight_layout()
            if feature_label == 'amplitude':
                pb.axvline(12.5, color='gray', linestyle='--')
                ax = pb.gca()
                lim = ax.get_xlim()
                extraticks = [12.5, ]
                ax.set_xticks(list(ax.get_xticks()) + extraticks)
                ax.set_xlim(lim)
                # import IPython
                # IPython.embed()

            pb.savefig(os.path.join(output_dir, '{}_{}_density_{}_weighted_{}.png'.format(label, feature_label, True, False)), dpi=300)
            pb.close()

            fig = pb.figure(figsize=(2, 2))
            pb.hist([feature1, feature2], weights=[energy1, energy2],
                     bins=100, label=[namespace.label1, namespace.label2], density=False)
            pb.title(' '.join([label, feature_label]))
            os.makedirs(output_dir, exist_ok=True)
            pb.legend()
            fig.tight_layout()
            pb.savefig(os.path.join(output_dir, '{}_{}_density_{}_weighted_{}.png'.format(label, feature_label, False, True)), dpi=300)
            pb.close()

            fig = pb.figure(figsize=(2, 2))
            pb.hist([feature1, feature2], weights=[energy1, energy2],
                     bins=100, label=[namespace.label1, namespace.label2], density=True)
            pb.title(' '.join([label, feature_label]))
            os.makedirs(output_dir, exist_ok=True)
            pb.legend()
            fig.tight_layout()
            pb.savefig(os.path.join(output_dir, '{}_{}_density_{}_weighted_{}.png'.format(label, feature_label, True, True)), dpi=300)
            pb.close()


if __name__ == '__main__':
    main()

# python research_tag_features.py /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/tags_experiments/tags_spindles_no_dipole_gof_location_test_signal_based_snr/mass_dataset/tags_confirmations/tags_confirmed_snr_None_length_0.5_channel_C3_overlap_treshold_1.0/ "True Positive" /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/tags_experiments/tags_spindles_no_dipole_gof_location_test_signal_based_snr/mass_dataset/tags_confirmations/tags_unconfirmed_snr_None_length_0.5_channel_C3_overlap_treshold_1.0/ "False Positive"
