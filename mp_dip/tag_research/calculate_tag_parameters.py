import argparse
import os
from collections import defaultdict

import numpy as np
import pandas as pd
import pylab as pb
import tqdm
from datetimerange import DateTimeRange
from mne.preprocessing import ICA
from scipy.stats import pearsonr, scoreatpercentile
from sklearn import linear_model
from sklearn.exceptions import NotFittedError

from mp_dip.tag_research.tagged_spindle_dipole_analysis import get_tag_timewindows
from mp_dip.tag_research.compare_tag_folders import get_tag_file_pairs, filter_tags_list, \
    filter_tags_list_by_name, filter_tag_pairs_by_snr, filter_tags_by_minimum_length


def merge_same_tags(tags):
    tags_filtered = []

    for tag in tags:
        tags_set = set([(i['start_timestamp'], i['end_timestamp']) for i in tags_filtered])
        if (tag['start_timestamp'], tag['end_timestamp']) not in tags_set:
            tags_filtered.append(tag)
    return tags_filtered


def plot_corr(df, ax,
              xn='density_gc_spindle_per_min', yn='density_pred_spindle_per_min',
              title='Spindle density [spindle/min]', size=2):

    pb.sca(ax)
    reg = linear_model.LinearRegression()
    x_fit_raw_values = df[xn].values
    y_fit_raw_values = df[yn].values
    good_mask_x = np.isfinite(df[xn].values)
    good_mask_y = np.isfinite(df[yn].values)
    good_mask = good_mask_x * good_mask_y
    x_fit = x_fit_raw_values[good_mask].reshape(-1, 1)
    y_fit = y_fit_raw_values[good_mask]
    try:
        reg.fit(x_fit, y_fit)
        slope = reg.coef_[0]
    except:
        slope = np.nan

    min = np.nanmin(df[xn].values)
    max = np.nanmax(df[xn].values)
    X = np.linspace(min, max, 1000)
    try:
        r, pval = pearsonr(np.squeeze(x_fit), y_fit)
    except:
        r = np.nan
        pval = np.nan
    pb.title('{},\ncorr={:0.2f}\nslope={:0.2f}'.format(title, r, slope))
    pb.scatter(df[xn], df[yn], s=size)

    pb.xlabel("Experts annotations")
    pb.ylabel("Prediction")
    try:
        fitted_y = reg.predict(X.reshape(-1, 1))
    except:
        fitted_y = np.array([np.nan] * X.shape[0])
    pb.plot(X, fitted_y)
    return ax


def main():
    np.set_printoptions(precision=3)
    np.set_printoptions(suppress=True)
    np.set_printoptions(linewidth=120)
    parser = argparse.ArgumentParser(description="Compare tags")
    parser.add_argument('folder_truth', metavar='file', help="folder with human marked tags")
    parser.add_argument('folder_compare', metavar='file', help="folder with generated tags")
    parser.add_argument('-c', '--filter-channel', action='store',
                        help='limit tag comparing to one channel. Filters only generated tags. By using referenceAsReadable tag')
    parser.add_argument('-s', '--snr-filter', action='store', type=str, default=None,
                        help='Filter tags by SNR, semicolon (;) seperated list, use with equals sign, example:\n-s="-60;-50;-40;-30;-20;-10;-5"')
    parser.add_argument('-l', '--filter-length', action='store',
                        help='filters ground truth tags to a minimum length in secodns',
                        type=str, default=None)

    namespace = parser.parse_args()

    analyzed_tag_name = 'Spindle (wrzeciono)'

    tag_set_pairs, filenames = get_tag_file_pairs(namespace.folder_truth, namespace.folder_compare, False,
                                                  filter_channel=namespace.filter_channel,
                                                  )

    tag_set_pairs_list = []
    if namespace.snr_filter is None:
        snrs = [None]
        tag_set_pairs_list.append(tag_set_pairs)
    else:
        snrs = [float(i) for i in namespace.snr_filter.strip().strip(';').split(';')]
        for snr in snrs:
            tag_set_pairs_snr = filter_tag_pairs_by_snr(tag_set_pairs, snr)
            tag_set_pairs_list.append(tag_set_pairs_snr)



    overlap_tresholds = [0.01, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 95]
    snr_f1_summary = []
    for tag_set_pairs, snr in zip(tag_set_pairs_list, snrs):
        data_labels = 'filename,density_gc_spindle_per_min,density_pred_spindle_per_min,length_gc_sec,lenth_pred_sec'
        data = []
        overlaps_all = []
        f1_stats_all = defaultdict(list)

        for pair, filename in tqdm.tqdm(zip(tag_set_pairs, filenames), desc='analyzing pairs'):
            print(filename)
            ground_truth_tags = pair[0]
            predicted_tags = pair[1]

            valid_time_windows_tag = get_tag_timewindows(ground_truth_tags, 'segmentViewed')
            if len(valid_time_windows_tag) == 0:
                valid_time_windows_tag = [[0, np.inf]]
            ground_truth_tags = filter_tags_list_by_name(ground_truth_tags, analyzed_tag_name)
            if namespace.filter_length:
                ground_truth_tags = filter_tags_by_minimum_length(ground_truth_tags, float(namespace.filter_length))
            predicted_tags = filter_tags_list_by_name(predicted_tags, analyzed_tag_name)
            ground_truth_tags = merge_same_tags(ground_truth_tags)
            predicted_tags = merge_same_tags(predicted_tags)

            amount_of_tags_predict = 0
            amount_of_tags_ground_truth = 0
            amount_of_seconds = 0

            predicted_tags_filtered_all = []
            ground_truth_tags_filtered_all = []
            for time_window in valid_time_windows_tag:
                time_window_length = time_window[1] - time_window[0]
                amount_of_seconds += time_window_length

                predicted_tags_filtered = filter_tags_list(predicted_tags, [time_window, ])
                amount_of_tags_predict += len(predicted_tags_filtered)

                ground_truth_tags_filtered = filter_tags_list(ground_truth_tags, [time_window, ])
                amount_of_tags_ground_truth += len(ground_truth_tags_filtered)

                predicted_tags_filtered_all.extend(predicted_tags_filtered)
                ground_truth_tags_filtered_all.extend(ground_truth_tags_filtered)

            tag_average_length_predict = np.mean([i['end_timestamp'] - i['start_timestamp'] for i in predicted_tags_filtered_all])
            tag_average_length_ground_truth = np.mean([i['end_timestamp'] - i['start_timestamp'] for i in ground_truth_tags_filtered_all])
            data.append([filename,
                         amount_of_tags_ground_truth / (amount_of_seconds / 60),
                         amount_of_tags_predict / (amount_of_seconds / 60),
                         tag_average_length_ground_truth,
                         tag_average_length_predict,
                         ])

            # for each tag we'll find an overlap, each side...
            overlaps = []
            true_positives = defaultdict(int)  # per overlap percentage
            false_positives = defaultdict(int)  # per overlap percentage
            false_negatives = defaultdict(int)  # per overlap percentage
            f1_stat = defaultdict(int)  # per overlap percentage
            for tag_predict in tqdm.tqdm(predicted_tags_filtered_all, desc='calculating overlaps 1/2'):
                do_overlap = False
                tag_predict_range = DateTimeRange(tag_predict['start_timestamp'], tag_predict['end_timestamp'])
                for tag_gt in ground_truth_tags_filtered_all:
                    tag_gt_range = DateTimeRange(tag_gt['start_timestamp'], tag_gt['end_timestamp'])
                    do_overlap = tag_gt_range.is_intersection(tag_predict_range)
                    if do_overlap:
                        encompass_range = tag_gt_range.encompass(tag_predict_range)
                        intersect_range = tag_gt_range.intersection(tag_predict_range)
                        overlap = intersect_range.get_timedelta_second() / encompass_range.get_timedelta_second()
                        overlaps.append(overlap)
                        for overlap_treshold in overlap_tresholds:
                            if overlap * 100 > overlap_treshold:
                                true_positives[overlap_treshold] += 1
                            else:
                                false_positives[overlap_treshold] += 1
                        break
                if not do_overlap:
                    overlaps.append(0)
                    for overlap_treshold in overlap_tresholds:
                        false_positives[overlap_treshold] += 1

            for tag_gt in tqdm.tqdm(ground_truth_tags_filtered_all, desc='calculating overlaps 2/2'):
                do_overlap = False
                tag_gt_range = DateTimeRange(tag_gt['start_timestamp'], tag_gt['end_timestamp'])
                for tag_predict in predicted_tags_filtered_all:
                    tag_predict_range = DateTimeRange(tag_predict['start_timestamp'], tag_predict['end_timestamp'])
                    do_overlap = tag_gt_range.is_intersection(tag_predict_range)
                    if do_overlap:
                        encompass_range = tag_gt_range.encompass(tag_predict_range)
                        intersect_range = tag_gt_range.intersection(tag_predict_range)
                        overlap = intersect_range.get_timedelta_second() / encompass_range.get_timedelta_second()
                        overlaps.append(overlap)
                        break
                if not do_overlap:
                    overlaps.append(0)
                    for overlap_treshold in overlap_tresholds:
                        false_negatives[overlap_treshold] += 1

            for overlap_treshold in overlap_tresholds:
                try:
                    f1_stat[overlap_treshold] = true_positives[overlap_treshold] / (true_positives[overlap_treshold] + 0.5 * (false_positives[overlap_treshold] + false_negatives[overlap_treshold] ))
                except ZeroDivisionError:
                    f1_stat[overlap_treshold] = 0

            f1_stats_all['filename'].append(filename)
            for overlap_treshold in overlap_tresholds:
                f1_stats_all['tp_{}'.format(overlap_treshold)].append(true_positives[overlap_treshold])
                f1_stats_all['fp_{}'.format(overlap_treshold)].append(false_positives[overlap_treshold])
                f1_stats_all['fn_{}'.format(overlap_treshold)].append(false_negatives[overlap_treshold])
                f1_stats_all['f1_{}'.format(overlap_treshold)].append(f1_stat[overlap_treshold])
            overlaps_all.append(overlaps)

        f1_stats_all['filename'].append("overall")
        for overlap_treshold in overlap_tresholds:
            sum_tp = np.sum(f1_stats_all['tp_{}'.format(overlap_treshold)])
            f1_stats_all['tp_{}'.format(overlap_treshold)].append(sum_tp)
            sum_fp = np.sum(f1_stats_all['fp_{}'.format(overlap_treshold)])
            f1_stats_all['fp_{}'.format(overlap_treshold)].append(sum_fp)
            sum_fn = np.sum(f1_stats_all['fn_{}'.format(overlap_treshold)])
            f1_stats_all['fn_{}'.format(overlap_treshold)].append(sum_fn)
            try:
                f1_overall = sum_tp / (sum_tp + 0.5 * (sum_fp + sum_fn))
            except ZeroDivisionError:
                f1_overall = 0
            f1_stats_all['f1_{}'.format(overlap_treshold)].append(f1_overall)



        data_dict = {}
        for nr, column_name in enumerate(data_labels.split(',')):
            data_column = []
            for i in data:
                data_column.append(i[nr])
            data_dict[column_name] = data_column

        df = pd.DataFrame(data_dict)
        output_dir = os.path.join(namespace.folder_compare, 'tag_params_compare_summary_channel_{}_snr_{}_min_length_{}'.format(namespace.filter_channel, snr, namespace.filter_length))
        os.makedirs(output_dir, exist_ok=True)
        df.to_csv(os.path.join(output_dir, 'params_summary.csv'))

        df_f1 = pd.DataFrame(f1_stats_all)
        df_f1.to_csv(os.path.join(output_dir, 'f1_summary.csv'))

        fig_all = pb.figure(figsize=(7, 3))
        fig_f1_overlap = pb.figure(figsize=(4, 4))
        f1_x_all = overlap_tresholds
        f1_y = []
        for f1_x in f1_x_all:
            overall = df_f1.iloc[-1]
            f1_y.append(overall['f1_{}'.format(f1_x)])
        ax = fig_f1_overlap.add_subplot(111)
        pb.plot(f1_x_all, f1_y, '-o')
        pb.title("F1 vs Overlap[%]")
        pb.xlabel("Overlap [%]")
        pb.ylabel("F1 score")
        pb.xlim([0, 100])
        pb.ylim([0, 1])
        fig_f1_overlap.tight_layout()
        fig_f1_overlap.savefig(os.path.join(output_dir, 'f1_vs_overlap.png'), dpi=300)

        overall = df_f1.iloc[-1]
        snr_f1_summary.append(overall['f1_{}'.format(overlap_tresholds[0])])

        ax = fig_all.add_subplot(131)
        pb.sca(ax)
        overlaps_all_all = []
        for a in overlaps_all:
            overlaps_all_all.extend(a)
        overlaps_all_all = np.array(overlaps_all_all) * 100
        bin_values, bins, artist = pb.hist(overlaps_all_all, bins=20, color='orange')
        pb.ylim([0, np.max(bin_values[1:]) * 1.1])
        pb.xlim([0,100])
        pb.title("Overlap")
        pb.ylabel("Spindle count")
        pb.xlabel("Overlap [%]")
        pb.text(0, np.max(bin_values[1:])*0.95, '{}'.format(int(bin_values[0])))

        ax = fig_all.add_subplot(132)
        plot_corr(df, ax, title='Density [spindle/min]')

        ax = fig_all.add_subplot(133)
        ax = plot_corr(df, ax, 'length_gc_sec', 'lenth_pred_sec', "Duration [s]")
        fig_all.tight_layout()
        fig_all.savefig(os.path.join(output_dir, 'summary_of_comparison_with_gt.png'), dpi=300)
        pb.close(fig_all)
        pb.close(fig_f1_overlap)

    if len(snrs) > 1:
        fig = pb.figure(figsize=(5, 3.5))
        pb.plot(snrs, snr_f1_summary, '-o', markersize=3)
        # pb.title("F1 vs SNR (at smallest overlap)")
        pb.ylabel('F1')
        pb.ylim([0, 1])
        pb.xlabel("SNR [dB]")
        fig.tight_layout()
        fig.savefig(os.path.join(output_dir, 'snr_vs_f1.png'), dpi=300)
        df_snr_f1 = pd.DataFrame(data=np.array([snrs, snr_f1_summary]).T, columns=['snr', 'f1'])
        df_snr_f1.to_csv(os.path.join(output_dir, 'snr_vs_f1.csv'))


if __name__ == '__main__':
    main()

# python calculate_tag_parameters.py -c C3 /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/tags_moda_exp_gc /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/tags_experiments/tags_spindles_testing/
# python calculate_tag_parameters.py -c C3 /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/tags_moda_exp_gc /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/tags_experiments/tags_spindles_no_dipole_gof_location_compound_snr-30__spindles_fix/mass_dataset/

# python calculate_tag_parameters.py -c C3 /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/tags_moda_exp_gc /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/tags_experiments/tags_spindles_no_dipole_gof_location_test_signal_based_snr/mass_dataset/tags_confirmations/tags_confirmed_snr_None_length_0.5_channel_C3_overlap_treshold_1.0/