import argparse
import os
import pickle
from collections import defaultdict
from mp_dip.obci_readmanager.signal_processing.tags import tags_file_writer as tags_writer
import numpy as np
import pandas as pd
import pylab as pb
import tqdm
from datetimerange import DateTimeRange
from mne.preprocessing import ICA
from scipy.stats import pearsonr, scoreatpercentile
from sklearn import linear_model
from sklearn.exceptions import NotFittedError

from mp_dip.tag_research.tagged_spindle_dipole_analysis import get_tag_timewindows
from mp_dip.tag_research.calculate_tag_parameters import merge_same_tags
from mp_dip.tag_research.compare_tag_folders import get_tag_file_pairs, filter_tags_list, \
    filter_tags_list_by_name, filter_tag_pairs_by_snr, filter_tags_by_minimum_length
from mp_dip.utils.utils import TAG_DEFS


def main():
    np.set_printoptions(precision=3)
    np.set_printoptions(suppress=True)
    np.set_printoptions(linewidth=120)
    parser = argparse.ArgumentParser(description="Filter tags into 2 new sets - one only with true positives, another only with false positives")
    parser.add_argument('folder_truth', metavar='file', help="folder with human marked tags")
    parser.add_argument('folder_compare', metavar='file', help="folder with generated tags")
    parser.add_argument('-o', '--output', metavar='file', help='Folder where to dump 2 folders of tags')
    parser.add_argument('-c', '--filter-channel', action='store',
                        help='limit tag comparing to one channel. Filters only generated tags. By using referenceAsReadable tag')
    parser.add_argument('-s', '--snr-filter', action='store', type=str, default=None,
                        help='Filter tags by SNR, semicolon (;) seperated list, use with equals sign, example:\n-s="-60;-50;-40;-30;-20;-10;-5"')
    parser.add_argument('-l', '--filter-length', action='store',
                        help='filters ground truth tags to a minimum length in secodns',
                        type=str, default=None)
    parser.add_argument('-t', '--treshold', action='store',
                        help='overlap treshold in % [0-100]',
                        type=str, default=1.0)

    namespace = parser.parse_args()

    overlap_treshold = namespace.treshold
    analyzed_tag_name = 'Spindle (wrzeciono)'

    tag_set_pairs, filenames = get_tag_file_pairs(namespace.folder_truth, namespace.folder_compare, False,
                                                  filter_channel=namespace.filter_channel,
                                                  )

    tag_set_pairs_list = []
    if namespace.snr_filter is None:
        snrs = [None]
        tag_set_pairs_list.append(tag_set_pairs)
    else:
        snrs = [float(i) for i in namespace.snr_filter.strip().strip(';').split(';')]
        for snr in snrs:
            tag_set_pairs_snr = filter_tag_pairs_by_snr(tag_set_pairs, snr)
            tag_set_pairs_list.append(tag_set_pairs_snr)

    for tag_set_pairs, snr in zip(tag_set_pairs_list, snrs):
        for pair, filename in tqdm.tqdm(zip(tag_set_pairs, filenames), desc='analyzing pairs'):


            tags_confirmed_by_ground_truth = []
            tags_unconfirmed_by_ground_truth = []

            macroatoms_confirmed_by_ground_truth = []
            macroatoms_unconfirmed_by_ground_truth = []

            print(filename)
            macroatoms = pd.read_pickle(os.path.join(namespace.folder_compare,
                                                     os.path.splitext(filename)[0] + '.pickle'))
            macroatoms_filtered = [i[0] for i in macroatoms if i[1] == analyzed_tag_name]
            macroatoms_index = [macroatom.mmp3_atom_ids.values[0] for macroatom in macroatoms_filtered]
            ground_truth_tags = pair[0]
            predicted_tags = pair[1]

            valid_time_windows_tag = get_tag_timewindows(ground_truth_tags, 'segmentViewed')
            if len(valid_time_windows_tag) == 0:
                valid_time_windows_tag = [[0, np.inf]]
            ground_truth_tags = filter_tags_list_by_name(ground_truth_tags, analyzed_tag_name)
            if namespace.filter_length:
                ground_truth_tags = filter_tags_by_minimum_length(ground_truth_tags, float(namespace.filter_length))
            predicted_tags = filter_tags_list_by_name(predicted_tags, analyzed_tag_name)
            ground_truth_tags = merge_same_tags(ground_truth_tags)
            predicted_tags = merge_same_tags(predicted_tags)

            predicted_tags_filtered_all = []
            ground_truth_tags_filtered_all = []
            for time_window in valid_time_windows_tag:

                predicted_tags_filtered = filter_tags_list(predicted_tags, [time_window, ])
                predicted_tags_filtered_all.extend(predicted_tags_filtered)
                ground_truth_tags_filtered = filter_tags_list(ground_truth_tags, [time_window, ])
                ground_truth_tags_filtered_all.extend(ground_truth_tags_filtered)
            # don't filter predictions by time windows

            # for each tag we'll find an overlap, each side...
            for tag_predict in tqdm.tqdm(predicted_tags_filtered_all, desc='calculating overlaps 1/2'):
                tag_predict_range = DateTimeRange(tag_predict['start_timestamp'], tag_predict['end_timestamp'])
                do_overlap = False
                for tag_gt in ground_truth_tags_filtered_all:
                    tag_gt_range = DateTimeRange(tag_gt['start_timestamp'], tag_gt['end_timestamp'])
                    do_overlap = tag_gt_range.is_intersection(tag_predict_range)
                    if do_overlap:
                        encompass_range = tag_gt_range.encompass(tag_predict_range)
                        intersect_range = tag_gt_range.intersection(tag_predict_range)
                        overlap = intersect_range.get_timedelta_second() / encompass_range.get_timedelta_second() * 100

                        macroatom = macroatoms_filtered[macroatoms_index.index(int(tag_predict['desc']['example_nr']))]
                        tag_center = (tag_predict['end_timestamp'] + tag_predict['start_timestamp']) / 2
                        macroatom_pos = macroatom.absolute_position.values[0]
                        try:
                            assert np.isclose(tag_center, macroatom_pos, atol=0.001, rtol=0)  # just making sure
                        except AssertionError:
                            print(tag_center, macroatom_pos)
                            import IPython
                            IPython.embed()
                        # in percentage
                        if overlap >= overlap_treshold:
                            tags_confirmed_by_ground_truth.append(tag_predict)
                            macroatoms_confirmed_by_ground_truth.append([macroatom, analyzed_tag_name])
                        else:
                            tags_unconfirmed_by_ground_truth.append(tag_predict)
                            macroatoms_unconfirmed_by_ground_truth.append([macroatom, analyzed_tag_name])
                        break
                if not do_overlap:
                    macroatom = macroatoms_filtered[macroatoms_index.index(int(tag_predict['desc']['example_nr']))]
                    tag_center = (tag_predict['end_timestamp'] + tag_predict['start_timestamp']) / 2
                    macroatom_pos = macroatom.absolute_position.values[0]
                    try:
                        assert np.isclose(tag_center, macroatom_pos, atol=0.001, rtol=0)  # just making sure
                    except AssertionError:
                        print(tag_center, macroatom_pos)
                        import IPython
                        IPython.embed()
                    tags_unconfirmed_by_ground_truth.append(tag_predict)
                    macroatoms_unconfirmed_by_ground_truth.append([macroatom, analyzed_tag_name])


            output_dir_base = namespace.output
            output_dir_confirmed = os.path.join(output_dir_base,
                                                'tags_confirmed_snr_{}_length_{}_channel_{}_overlap_treshold_{}'.format(
                                                    snr,
                                                    namespace.filter_length,
                                                    namespace.filter_channel,
                                                    overlap_treshold
                                                ))
            output_dir_unconfirmed = os.path.join(output_dir_base,
                                                'tags_unconfirmed_snr_{}_length_{}_channel_{}_overlap_treshold_{}'.format(
                                                    snr,
                                                    namespace.filter_length,
                                                    namespace.filter_channel,
                                                    overlap_treshold
                                                ))
            os.makedirs(output_dir_unconfirmed, exist_ok=True)
            os.makedirs(output_dir_confirmed, exist_ok=True
                        )
            # saving confirmed
            file_out_name_base = os.path.join(output_dir_confirmed,
                                              os.path.splitext(
                                                  os.path.basename(
                                                      filename))[
                                                  0])
            writer = tags_writer.TagsFileWriter(file_out_name_base + '.tag',
                                                p_defs=TAG_DEFS)
            for tag in tags_confirmed_by_ground_truth:
                writer.tag_received(tag)
            writer.finish_saving(0.0)

            with open(file_out_name_base + '.pickle', 'wb') as macroatoms_save_file:
                pickle.dump(macroatoms_confirmed_by_ground_truth, macroatoms_save_file)

            # saving unconfirmed
            file_out_name_base = os.path.join(output_dir_unconfirmed,
                                              os.path.splitext(
                                                  os.path.basename(
                                                      filename))[
                                                  0])
            writer = tags_writer.TagsFileWriter(file_out_name_base + '.tag',
                                                p_defs=TAG_DEFS)
            for tag in tags_unconfirmed_by_ground_truth:
                writer.tag_received(tag)
            writer.finish_saving(0.0)

            with open(file_out_name_base + '.pickle', 'wb') as macroatoms_save_file:
                pickle.dump(macroatoms_unconfirmed_by_ground_truth, macroatoms_save_file)


if __name__ == '__main__':
    main()

# python filter_tags_ground_truth_accordance.py -l 0.5 -c C3 -o /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/tags_experiments/tags_spindles_no_dipole_gof_location_test_signal_based_snr/mass_dataset/tags_confirmations /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/tags_moda_exp_gc /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/tags_experiments/tags_spindles_no_dipole_gof_location_test_signal_based_snr/mass_dataset/
# python filter_tags_ground_truth_accordance.py -l 0.5 -c C3 -o /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/tags_experiments/tags_spindles_testing/tags_confirmations /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/tags_moda_exp_gc /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/tags_experiments/tags_spindles_testing/