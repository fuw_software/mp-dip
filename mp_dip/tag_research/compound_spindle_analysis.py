import argparse
import glob
import os
from collections import defaultdict

import mne
import numpy as np
import pandas as pd
import seaborn
from mne.datasets import fetch_fsaverage
from tqdm import tqdm
import pylab as pb


def main():
    np.set_printoptions(precision=3)
    np.set_printoptions(suppress=True)
    np.set_printoptions(linewidth=120)
    parser = argparse.ArgumentParser(description="Analysing tags (from MMP) params.")
    parser.add_argument('files', nargs='+', metavar='file', help="path to *.pickle")
    namespace = parser.parse_args()

    dir = fetch_fsaverage()
    subjects_dir = os.path.join(dir, '..')
    transform_path = os.path.join(dir, 'bem', 'fsaverage-trans.fif')
    transform = mne.read_trans(transform_path, return_all=False, )

    if len(namespace.files) == 1:
        files_to_work = glob.glob(namespace.files[0])
    else:
        files_to_work = namespace.files

    distances = defaultdict(list)
    distances_all = []
    frequency_distances = []
    phases_distances = []
    compund_size = []
    for file in tqdm(files_to_work):
        compounds = pd.read_pickle(file)
        for compound in compounds:
            compund_size.append(len(compound))
            if len(compound) == 3:  # really it's 2, we always have a copy in compound for some reason
                compound = compound[1:]
                gofs = np.array([i[0]['dip_gof'].values[0] for i in compound])
                positions = [[i[0]['dip_posx'].values[0], i[0]['dip_posy'].values[0], i[0]['dip_posz'].values[0]]
                             for i in compound]
                frequencies = np.array([i[0]['frequency'].values[0] for i in compound])
                phases = np.array([i[0]['phase'].values[0] for i in compound])

                mni_dip_pose = mne.head_to_mni(positions, 'fsaverage', transform,
                                               subjects_dir=subjects_dir, verbose=None)
                mni_dip_pose = np.array(mni_dip_pose)

                if (gofs > 90).all():
                    label = '90-'
                elif np.logical_and(gofs > 80, gofs <= 90).all():
                    label = '80-90'
                elif np.logical_and(gofs > 70, gofs <= 80).all():
                    label = '70-80'
                elif (gofs > 80).all():
                    label = '80-'
                elif np.logical_and(gofs > 70, gofs <= 90).all():
                    label = '70-90'
                elif np.logical_and(gofs > 0, gofs <= 50).all():
                    label = '0-50'
                else:
                    label = 'rest'
                distances_compound = []
                first = mni_dip_pose[0]
                others = mni_dip_pose[1:]
                for other in others:
                    distance = np.linalg.norm(first - other)
                    distances_compound.append(distance)
                distances[label].append(np.max(distances_compound))
                distances_all.append(np.max(distances_compound))

                first = frequencies[0]
                others = frequencies[1:]
                distances_compound = []
                for other in others:
                    distance = first - other
                    distances_compound.append(distance)
                frequency_distances.append(np.max(distances_compound))

                first = phases[0]
                others = phases[1:]
                distances_compound = []
                for other in others:
                    distance = first - other
                    distances_compound.append(distance)
                phases_distances.append(np.max(distances_compound))

    outdir = os.path.join(os.path.dirname(file), 'img_compound_analysis')
    os.makedirs(outdir, exist_ok=True)
    fig = pb.figure(figsize=(5, 5))
    keys = list(distances.keys())
    pb.hist([distances[i]for i in keys], bins=50, label=keys, density=True)
    pb.xlabel('Distance [mm]')
    pb.legend()
    fig.savefig(os.path.join(outdir, 'distance_by_gof_densityTrue.png'))

    fig = pb.figure(figsize=(5, 5))
    pb.hist([distances[i]for i in keys], bins=50, label=keys)
    pb.legend()
    pb.xlabel('Distance [mm]')
    fig.savefig(os.path.join(outdir, 'distance_by_gof_densityFalse.png'))

    fig = pb.figure(figsize=(5, 5))
    pb.hist(distances_all, bins=50)
    pb.xlabel('Distance [mm]')
    fig.savefig(os.path.join(outdir, 'distance.png'))

    fig = pb.figure(figsize=(5, 5))
    pb.hist(frequency_distances, bins=50)
    pb.xlabel('Frequency gap [Hz]')
    fig.savefig(os.path.join(outdir, 'freq_gap.png'))


    fig = pb.figure(figsize=(5, 5))
    frequency_distances = np.array(frequency_distances)
    pb.hist([frequency_distances[frequency_distances >= 0],
             np.abs(frequency_distances[frequency_distances < 0])],
            bins=50,
            label=['frequency falling',
                   'frequency rising'])
    pb.xlabel('Frequency gap [Hz]')
    pb.legend()
    fig.savefig(os.path.join(outdir, 'freq_gap_rising_vs_falling.png'))


    fig = pb.figure(figsize=(5, 5))
    pb.hist(phases_distances, bins=50)
    pb.xlabel('Phase gap [rad]')
    fig.savefig(os.path.join(outdir, 'phase_gap.png'))


    pb.figure()
    pb.hist(compund_size, bins=50)

    fig = pb.figure(figsize=(5, 5))
    df = pd.DataFrame.from_dict(data={'Frequency gap [Hz]': frequency_distances, 'Distance [mm]': distances_all})
    seaborn.histplot(df, x="Frequency gap [Hz]", y="Distance [mm]", cbar=True)
    fig.savefig(os.path.join(outdir, 'frequency_distance_heatmap.png'))

    # pb.show()



if __name__ == '__main__':
    main()