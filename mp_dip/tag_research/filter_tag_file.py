import argparse
import glob
import os

from tqdm import tqdm


def main():
    parser = argparse.ArgumentParser(description="Compare tags")
    parser.add_argument('folder_tags', metavar='file', help="folder with human marked tags")
    parser.add_argument('-s', '--snr', action='store', type=str, default=None, help="snr to filter tags out by")
    parser.add_argument('folder_tags_output', metavar='file', help="folder with generated tags")
    namespace = parser.parse_args()

    files = glob.glob(os.path.join(namespace.folder_tags, '*.tag'))
    os.makedirs(namespace.folder_tags_output, exist_ok=True)
    snr_threshold = float(namespace.snr)
    for tagfile in tqdm(files):
        tag_text = open(tagfile, 'r').read()
        split_text = '<tag channelNumber'
        tags_split = tag_text.split(split_text)
        tags_new = []
        intro = tags_split.pop(0)
        tags_new.append(intro)
        for tag in tags_split[:-1]:
            tag_snr = float(tag.split('<snr>')[-1].split('<')[0])
            if tag_snr < snr_threshold:
                tags_new.append(tag)
        tags_new.append(tags_split[-1])
        new_tags_text = split_text.join(tags_new)
        new_file_name = os.path.join(namespace.folder_tags_output, os.path.basename(tagfile))
        file_out = open(new_file_name, 'w')
        file_out.write(new_tags_text)
        file_out.close()


if __name__ == '__main__':
    main()
