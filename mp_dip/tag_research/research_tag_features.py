import glob
import pylab as pb
import os
from collections import defaultdict

import numpy as np
import argparse

import pandas as pd
import seaborn
from scipy.stats import mannwhitneyu, ks_2samp, ttest_ind
from tqdm import tqdm

from mp_dip.obci_readmanager.signal_processing.tags.read_tags_source import FileTagsSource
from mp_dip.research.somnambulism_analysis import get_tag_timewindows, filter_macroatoms_list
from mp_dip.utils.utils import get_all_atoms, read_raw, preanalyse_raw, get_mp_book_paths
from mp_dip.utils.data_preprocessing_utils import ATOM_FEATURES

#
#
# FEATURES_TO_COMPARE = ['phase', 'frequency', 'width', 'amplitude_z_score', 'snr',
#                        'amplitude', 'dip_distance_to_cortex_voxel', 'dip_gof']


FEATURES_TO_COMPARE = ['amplitude', 'frequency', 'width', 'dip_gof', 'dip_distance_to_cortex_voxel', 'snr']


class LiterateDict(dict):
    def __getitem__(self, value):
        try:
            return super().__getitem__(value)
        except:
            return value

technical_to_literate_dict = LiterateDict({'Spindle (wrzeciono)': "spindles",
                                           "amplitude": 'Amplitude [μV]',
                                           "frequency": 'Frequency [Hz]',
                                           "width": 'Duration [s]',
                                           "snr": 'SNR [dB]',
                                           'dip_gof': "GoF [%]",
                                           'dip_distance_to_cortex_voxel': 'DtC [mm]'

})


def filter_macroatoms_list_with_labels(macroatoms, valid_time_windows):
    filtered = []
    for macroatom in tqdm(macroatoms, desc='filtering atoms in time windows (tags)'):
        position = macroatom[0].absolute_position.values[0]
        for time_window in valid_time_windows:
            if position >= time_window[0] and position <= time_window[1]:
                filtered.append(macroatom)
    return filtered


def main():
    np.set_printoptions(precision=3)
    np.set_printoptions(suppress=True)
    np.set_printoptions(linewidth=120)
    parser = argparse.ArgumentParser(description="Analysing tags (from MMP) params.")
    parser.add_argument('-t', '--tag-folder', action='store', type=str,
                        default='',
                        help='folders with tags')
    parser.add_argument('-f', '--tagname', action='store', type=str,
                        default='',
                        help='filter by tag name, empty for just coloring, valid tagnames w,r,1,2,3,4,m,'
                             'denoting sleep stages or muscle activity. Can provide comma seperated list of tagnames.')
    parser.add_argument('files', nargs='+', metavar='file', help="path to *.pickle")
    namespace = parser.parse_args()
    tagname = namespace.tagname

    if len(namespace.files) == 1:
        files_to_work = glob.glob(namespace.files[0])
    else:
        files_to_work = namespace.files
    print(files_to_work)
    all_stuff = defaultdict(list)
    for file in tqdm(files_to_work, 'reading macroatoms'):

        if namespace.tag_folder:
            name = os.path.splitext(os.path.basename(file))[0]
            tag_candidates = glob.glob(os.path.join(namespace.tag_folder, name + '*.tag'))
            try:
                assert len(tag_candidates) == 1
            except:
                import IPython
                IPython.embed()
            tags = FileTagsSource(tag_candidates[0])
        else:
            tags = None

        macroatoms = pd.read_pickle(file)
        if tags:
            if tagname:
                tagnames = tagname.split(',')
                valid_time_windows = []
                for tagneme_split in tagnames:
                    valid_time_windows_tag = get_tag_timewindows(tags, tagneme_split)
                    valid_time_windows.extend(valid_time_windows_tag)
                valid_time_windows = sorted(valid_time_windows)
                macroatoms = filter_macroatoms_list_with_labels(macroatoms, valid_time_windows)
        for macroatom, label in macroatoms:
            # atom_best = np.where(macroatom.channel_name == 'C3')[0][0]
            atom_best = np.argmax(macroatom.amplitude)
            atom = macroatom.iloc[atom_best]
            all_stuff[label].append(atom)

    output_dir = os.path.join(os.path.dirname(file), 'mmp_params_histograms_{}'.format(tagname))

    for test_name, test in [['mannwhitney', mannwhitneyu], ['ks', ks_2samp], ['ttest', ttest_ind]]:
        output_dir_pval = os.path.join(output_dir, 'test_{}'.format(test_name))

        os.makedirs(output_dir_pval, exist_ok=True)

    full_figure_normal = pb.figure(figsize=(7.08, 7.08 * 2 / 3))
    full_figure_density = pb.figure(figsize=(7.08, 7.08 * 2 / 3))
    full_figure_weighted = pb.figure(figsize=(7.08, 7.08 * 2 / 3))
    full_figure_density_weighted = pb.figure(figsize=(7.08, 7.08 * 2 / 3))
    for graph_nr, feature_label in tqdm(enumerate(FEATURES_TO_COMPARE), 'drawing features',
                                        total=len(FEATURES_TO_COMPARE)):
        for label in np.unique(list(all_stuff.keys())):

            atoms = all_stuff[label]
            df = pd.DataFrame(atoms)

            energy = df['modulus'].values ** 2
            feature = df[feature_label].values
            if feature_label == "dip_distance_to_cortex_voxel":
                feature = np.abs(feature)
            mask_finite = np.isfinite(feature)
            feature = feature[mask_finite]
            energy = energy[mask_finite]

            ax = full_figure_normal.add_subplot(2, 3, graph_nr + 1)
            # pb.hist([feature], weights=[weights], bins=100, normed=True)
            pb.sca(ax)
            pb.hist(feature, bins=100,)
            pb.xlabel(technical_to_literate_dict[feature_label])
            pb.ylabel('Number of {}'.format(technical_to_literate_dict[label]))
            ax.ticklabel_format(axis='y', style='scientific', scilimits=[0,0])


            ax = full_figure_density.add_subplot(3, 2, graph_nr + 1)
            pb.sca(ax)

            # pb.hist([feature], weights=[weights], bins=100, normed=True)
            pb.hist(feature, bins=100, density=True)
            pb.xlabel(technical_to_literate_dict[feature_label])
            pb.ylabel('Density histogram of {}'.format(technical_to_literate_dict[label]))
            ax.ticklabel_format(axis='y', style='scientific', scilimits=[0, 0])


            ax = full_figure_weighted.add_subplot(3, 2, graph_nr + 1)
            pb.sca(ax)

            # pb.hist([feature], weights=[weights], bins=100, normed=True)
            pb.hist(feature, weights=energy,
                    bins=100, density=False)
            pb.xlabel(technical_to_literate_dict[feature_label])
            pb.ylabel('Energy weighted number of {}'.format(technical_to_literate_dict[label]))
            ax.ticklabel_format(axis='y', style='scientific', scilimits=[0, 0])

            ax = full_figure_density_weighted.add_subplot(3, 2, graph_nr + 1)
            pb.sca(ax)

            # pb.hist([feature], weights=[weights], bins=100, normed=True)
            pb.hist(feature, weights=energy,
                    bins=100, density=True)
            pb.xlabel(technical_to_literate_dict[feature_label])
            pb.ylabel('Normalised energy weighted number of {}'.format(technical_to_literate_dict[label]))
            ax.ticklabel_format(axis='y', style='scientific', scilimits=[0, 0])

    os.makedirs(output_dir, exist_ok=True)
    full_figure_normal.tight_layout()
    full_figure_density.tight_layout()
    full_figure_weighted.tight_layout()
    full_figure_density_weighted.tight_layout()

    full_figure_normal.savefig(os.path.join(output_dir, 'density_{}_weighted_{}.png'.format(False, False)), dpi=300)
    full_figure_density.savefig(os.path.join(output_dir, 'density_{}_weighted_{}.png'.format(True, False)), dpi=300)
    full_figure_weighted.savefig(os.path.join(output_dir, 'density_{}_weighted_{}.png'.format(False, True)), dpi=300)
    full_figure_density_weighted.savefig(os.path.join(output_dir, 'density_{}_weighted_{}.png'.format(True, True)), dpi=300)


if __name__ == '__main__':
    main()


# python /dmj/fizmed/mdovgialo/coma_structures2/coma_structures/coma_structures/tag_research/research_tag_features.py -f 2,3 -t /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/ /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/tags_experiments/tags_spindles_signal_based_snr_multichannel_snr_criterion_only_with_gof_with_cort_distance/mass_dataset/*.pickle