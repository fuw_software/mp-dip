import glob
import pylab as pb
import os
from collections import defaultdict

import numpy as np
import argparse

import pandas as pd
import seaborn
from scipy.stats import mannwhitneyu, ks_2samp, ttest_ind
from tqdm import tqdm

from mp_dip.utils.utils import get_all_atoms, read_raw, preanalyse_raw, get_mp_book_paths
from mp_dip.utils.data_preprocessing_utils import ATOM_FEATURES

snr_threshold = -30
FEATURES_TO_COMPARE = ['phase', 'frequency', 'width', 'amplitude_z_score', 'snr',
                       'amplitude', 'dip_distance_to_cortex_voxel', 'dip_gof']


def compare_per_user_pval(files_to_work, output_dir, test):
    data = defaultdict(list)

    for file in tqdm(files_to_work, 'creating pvals per recording'):
        all_stuff = defaultdict(list)
        macroatoms = pd.read_pickle(file)
        for macroatom, label in macroatoms:
            # atom_best = np.where(macroatom.channel_name == 'C3')[0][0]
            atom_best = np.argmax(macroatom.amplitude)
            atom = macroatom.iloc[atom_best]
            all_stuff[label].append(atom)

        try:
            len_of_stuff = min([len(i) for i in all_stuff.values()])
        except ValueError:
            len_of_stuff = 0

        is_empty = (len_of_stuff == 0)

        if is_empty:
            print(file, 'one of the pair items doesnt have spindles at all')
            continue
        data['filename'].append(os.path.basename(file))
        for feature_label in FEATURES_TO_COMPARE:
            for label in np.unique(list(all_stuff.keys())):
                atoms = all_stuff[label]
                df = pd.DataFrame(atoms)
                snr_low_mask = df['snr'] <= snr_threshold
                snr_high_mask = df['snr'] > snr_threshold

                df_low = df[snr_low_mask]
                df_high = df[snr_high_mask]

                feature_low = df_low[feature_label].values
                feature_high = df_high[feature_label].values

                mask_finite_low = np.isfinite(feature_low)
                mask_finite_high = np.isfinite(feature_high)

                feature_low = feature_low[mask_finite_low]
                feature_high = feature_high[mask_finite_high]

                if (len(feature_low) > 0) and (len(feature_high) > 0):
                    U, pval = test(feature_low, feature_high)
                else:
                    pval = np.nan

                data[feature_label].append(pval)

    df = pd.DataFrame.from_dict(data)
    df.to_csv(os.path.join(output_dir, 'pvalues_per_recording_{}_vs_{}.csv').format('snr_low', 'snr_high'))
    fig = pb.figure(figsize=(20, 10))
    ax = fig.add_subplot(111)
    # df.boxplot(ax=ax)
    seaborn.stripplot(data=df, ax=ax)
    seaborn.boxplot(data=df, ax=ax, color='white', showfliers=False)
    ax.axhline(0.05, linestyle='--', color='lightgray')
    fig.savefig(os.path.join(output_dir, 'pvalues_per_recording_{}_vs_{}.png').format('snr_low', 'snr_high'))

    with open(os.path.join(output_dir, 'pvalues_summary.txt'), 'w') as summary:
        for feature_label in FEATURES_TO_COMPARE:
            out = (feature_label + ' p_val < 0.05 ' + str(sum(df[feature_label] < 0.05))
                   + ' out of ' + str(len(df[feature_label])) + '\n')
            summary.write(out)


def main():
    np.set_printoptions(precision=3)
    np.set_printoptions(suppress=True)
    np.set_printoptions(linewidth=120)
    parser = argparse.ArgumentParser(description="Analysing tags (from MMP) params.")
    parser.add_argument('files', nargs='+', metavar='file', help="path to *.pickle")
    namespace = parser.parse_args()

    if len(namespace.files) == 1:
        files_to_work = glob.glob(namespace.files[0])
    else:
        files_to_work = namespace.files
    print(files_to_work)
    all_stuff = defaultdict(list)
    for file in tqdm(files_to_work, 'reading macroatoms'):
        macroatoms = pd.read_pickle(file)
        for macroatom, label in macroatoms:
            # atom_best = np.where(macroatom.channel_name == 'C3')[0][0]
            atom_best = np.argmax(macroatom.amplitude)
            atom = macroatom.iloc[atom_best]
            all_stuff[label].append(atom)
    output_dir = os.path.join(os.path.dirname(file), 'mmp_params_max_snr_compare')

    for test_name, test in [['mannwhitney', mannwhitneyu], ['ks', ks_2samp], ['ttest', ttest_ind]]:
        output_dir_pval = os.path.join(output_dir, 'test_{}'.format(test_name))

        os.makedirs(output_dir_pval, exist_ok=True)
        compare_per_user_pval(files_to_work, output_dir_pval, test)

    for feature_label in tqdm(FEATURES_TO_COMPARE, 'comparing features'):

        for label in np.unique(list(all_stuff.keys())):

            atoms = all_stuff[label]
            df = pd.DataFrame(atoms)

            snr_low_mask = df['snr'] <= snr_threshold
            snr_high_mask = df['snr'] > snr_threshold

            df_low = df[snr_low_mask]
            df_high = df[snr_high_mask]

            energy_low = df_low['modulus'].values ** 2
            energy_high = df_high['modulus'].values ** 2

            feature_low = df_low[feature_label].values
            feature_high = df_high[feature_label].values

            mask_finite_low = np.isfinite(feature_low)
            mask_finite_high = np.isfinite(feature_high)

            feature_low = feature_low[mask_finite_low]
            feature_high = feature_high[mask_finite_high]

            energy_low = energy_low[mask_finite_low]
            energy_high = energy_high[mask_finite_high]

            pb.figure(figsize=(10, 10))
            # pb.hist([feature], weights=[weights], bins=100, normed=True)
            pb.hist([feature_low, feature_high], bins=100, label=['snr low', 'snr high'])
            pb.title(' '.join([label, feature_label]))
            os.makedirs(output_dir, exist_ok=True)
            pb.legend()
            pb.savefig(os.path.join(output_dir, '{}_{}_density_{}_weighted_{}.png'.format(label, feature_label, False, False)))
            pb.close()

            pb.figure(figsize=(10, 10))
            # pb.hist([feature], weights=[weights], bins=100, normed=True)
            pb.hist([feature_low, feature_high], bins=100, label=['snr low', 'snr high'], density=True)
            pb.title(' '.join([label, feature_label]))
            os.makedirs(output_dir, exist_ok=True)
            pb.legend()
            pb.savefig(os.path.join(output_dir, '{}_{}_density_{}_weighted_{}.png'.format(label, feature_label, True, False)))
            pb.close()

            pb.figure(figsize=(10, 10))
            # pb.hist([feature], weights=[weights], bins=100, normed=True)
            pb.hist([feature_low, feature_high], weights=[energy_low, energy_high],
                    bins=100, label=['snr low', 'snr high'], density=False)
            pb.title(' '.join([label, feature_label]))
            os.makedirs(output_dir, exist_ok=True)
            pb.legend()
            pb.savefig(os.path.join(output_dir, '{}_{}_density_{}_weighted_{}.png'.format(label, feature_label, False, True)))
            pb.close()

            pb.figure(figsize=(10, 10))
            # pb.hist([feature], weights=[weights], bins=100, normed=True)
            pb.hist([feature_low, feature_high], weights=[energy_low, energy_high],
                    bins=100, label=['snr low', 'snr high'], density=True)
            pb.title(' '.join([label, feature_label]))
            os.makedirs(output_dir, exist_ok=True)
            pb.legend()
            pb.savefig(os.path.join(output_dir, '{}_{}_density_{}_weighted_{}.png'.format(label, feature_label, True, True)))
            pb.close()


if __name__ == '__main__':
    main()


