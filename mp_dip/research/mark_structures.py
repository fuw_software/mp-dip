import glob

import argparse
import json

import os
import tqdm
import numpy as np
import pandas as pd

from mp_dip.utils.db_atoms_utils import read_db_atoms
from mp_dip.utils.utils import read_raw, preanalyse_raw, get_rms_percentile, filter_selected_atoms, \
    svarog_tags_writer, get_mp_to_raw_channel_map, get_mp_book_paths


def get_df_structure(structure, atoms, ch_id, signal, raw):
    freq_spindle_range = [10, 16]
    freq_swa_range = [0.2, 2]
    freq_coma_pseudospindle_range = [17, 32]
    
    window_width = 0.5

    x = signal[:, ch_id]
    
    if structure == 'Spindle (wrzeciono)':
        rms, rms_ampli = get_rms_percentile(x, freq_spindle_range, window_width, 99., raw.info['sfreq'])
        df = filter_selected_atoms(atoms, ch_id, freq_spindle_range, [0.4, np.inf], 1.0, [rms_ampli, 100], structure)
    elif structure == 'SWA':
        rms_ampli = 7 * np.sqrt(np.median(x ** 2))
        df = filter_selected_atoms(atoms, ch_id, freq_swa_range, [0.5, 6], 1.0, [rms_ampli, 600], structure)
    elif structure == 'coma_pseudospindle':
        rms, rms_ampli = get_rms_percentile(x, freq_coma_pseudospindle_range, window_width, 99., raw.info['sfreq'])
        # df = filter_selected_atoms(atoms, ch_id, freq_coma_pseudospindle_range, [0.08, 0.28], 1.0, [0, 600], structure)
        df = filter_selected_atoms(atoms, ch_id, freq_coma_pseudospindle_range, [0.08, 1.0], 1.0, [20, 200],
                                   structure, 3)
    return df


def get_all_structures(signal, atoms, raw, mp_params):
    structures = ['Spindle (wrzeciono)', "SWA", "coma_pseudospindle"]
    main_dataframe = None
    for ch_id, channel_name in tqdm.tqdm(enumerate(mp_params['channel_names'])):
        for structure in structures:
            df = get_df_structure(structure, atoms, ch_id, signal, raw)
            if main_dataframe is None:
                main_dataframe = df
            else:
                main_dataframe = pd.concat([main_dataframe, df])

    return main_dataframe


def main():
    # https://docs.google.com/document/d/1p3231YP2YQu9ukfRQvDbwN4giGcyUmFFUkcRX9z3lA0/edit?disco=AAAADvQdOqQ&ts=5dc2aa8c&usp_dm=true
    parser = argparse.ArgumentParser(description="Mark structures for files which have been decomposed.")
    parser.add_argument('files', nargs='+', metavar='file', help="path to *.raw .bin or edf files")
    parser.add_argument('-d', '--mp-dir', nargs='?', type=str, help='MP Output directory')
    parser.add_argument('-o', '--outdir', nargs='?', type=str, help='Tag Output directory')
    parser.add_argument('-m', '--montage', action='store', help='Montages: none, ears, transverse', default='none')
    parser.add_argument('-mp', '--mp-type', action='store', help='mp type: smp, mmp3', default='smp')

    namespace = parser.parse_args()

    if len(namespace.files) == 1:
        files_to_work = glob.glob(namespace.files[0])
    else:
        files_to_work = namespace.files

    for path_to_raw in files_to_work:
        raw = read_raw(path_to_raw)
        raw_original_channels = raw.copy()
        raw = preanalyse_raw(raw, namespace.montage)

        core_of_path = path_to_raw[:-4]
        if namespace.outdir is None:
            tag_out_path = core_of_path + '_structures.tag'
        else:
            os.makedirs(namespace.outdir, exist_ok=True)
            basename = os.path.basename(core_of_path)
            tag_out_path = os.path.join(namespace.outdir, basename + '_structures.tag')

        path_to_book, path_to_book_params = get_mp_book_paths(namespace.mp_dir, path_to_raw,
                                                                           namespace.montage, namespace.mp_type)

        mp_params = json.load(open(path_to_book_params))
        
        x = raw.get_data().T * 1e6 # to microvolts
        atoms = read_db_atoms(path_to_book)
        df = get_all_structures(x, atoms, raw, mp_params)

        channel_map = get_mp_to_raw_channel_map(raw_original_channels, mp_params)
        svarog_tags_writer(df, tag_out_path, channel_map)
