import json
import os
from math import ceil

import pandas
from matplotlib.widgets import Slider, RadioButtons
# will not work without this imported
from mpl_toolkits.mplot3d import Axes3D

import mne
from mne.datasets import fetch_fsaverage
from scipy.stats import scoreatpercentile

from mp_dip.utils.multivariate_atom_analysis import parametrise_atom, \
    get_kernels, amplitude_signs

try:
    import pylab as pb
except:
    pass
import numpy as np
import argparse

from mp_dip.utils.utils import get_atoms_with_primary_parametrisation
from mp_dip.utils.signal_reconstruction import gabor

fs_dir = fetch_fsaverage(verbose='CRITICAL')
subjects_dir = os.path.join(fs_dir, '..')
transform_path = os.path.join(fs_dir, 'bem', 'fsaverage-trans.fif')
transform = mne.read_trans(transform_path, return_all=False, )


def reconstruct(example_atoms, timeline):
    reconstruction = example_atoms.amplitude.values[:, np.newaxis] / 2 * gabor(timeline,
          example_atoms.width.values[:, np.newaxis],
          example_atoms.absolute_position.values[:, np.newaxis],
          example_atoms.frequency.values[:, np.newaxis],
          example_atoms.phase.values[:, np.newaxis],
          ) * example_atoms.atom_importance.values[:, np.newaxis]
    return np.sum(reconstruction, axis=0)


class AtomVisualiser3D:
    SEGMENT_LENGTH = 20
    # microvolt_per_sec = 281.6
    microvolt_per_sec = 50.6
    SAMPLE_FREQ = 128

    def __init__(self,  atoms, mp_params_l, mp_path, mp_type, montage, channels_to_ignore):
        self.segment_nr = 0
        # used to display atoms
        atoms['atom_importance'] = False
        self.atoms = atoms
        self.mp_params = mp_params_l
        self.mp_path = mp_path
        self.mp_type = mp_type
        self.montage = montage
        self.channel_num = 0
        self.ch_names = self.mp_params['channel_names']
        self.channels_to_ignore = channels_to_ignore

        def _set_names(value):
            channel_name = self.ch_names[value['ch_id'] - 1]
            return channel_name
        atoms_chnames = self.atoms.apply(_set_names, axis=1)
        self.atoms['ch_name'] = atoms_chnames

        self.create_figure()

    def create_figure(self):
        fig = pb.figure()
        self.fig = fig
        self.scatter_ax = fig.add_axes([0.05, 0.3, 0.6, 0.60])
        self.reconstruction_ax = fig.add_axes([0.05, 0.05, 0.6, 0.25], sharex=self.scatter_ax)
        self.interpolation_ax = fig.add_axes([0.65, 0.65, 0.29, 0.35])
        self.dipole_ax = fig.add_axes([0.65, 0.30, 0.29, 0.35], projection='3d')

        offset_sliders = -0.05
        self.slider_gof_ax = fig.add_axes([0.75, 0.1 + offset_sliders, 0.2, 0.02])
        self.slider_dist_min_ax = fig.add_axes([0.75, 0.15 + offset_sliders, 0.2, 0.02])
        self.radio_selector_ax = fig.add_axes([0.75, 0.23 + offset_sliders, 0.2, 0.08])
        self.slider_dist_max_ax = fig.add_axes([0.75, 0.20 + offset_sliders, 0.2, 0.02])
        self.slider_segment_ax = fig.add_axes([0.1, 0.0, 0.8, 0.02])
        max_time_segment = ceil(self.atoms.absolute_position.max() / self.SEGMENT_LENGTH)

        self.filter_radio = RadioButtons(self.radio_selector_ax , ('None', 'custom filter', 'pw004 epilepsy', 'spindles', 'alpha', 'swa'))

        self.gof_slider = Slider(
            ax=self.slider_gof_ax,
            label='Dip GOF',
            valmin=0,
            valmax=100,
            valinit=90,
        )

        self.cortical_distance_slider_min = Slider(
            ax=self.slider_dist_min_ax,
            label='Cort_dist, min, mm',
            valmin=-20,
            valmax=50,
            valinit=-5,
        )

        self.cortical_distance_slider_max = Slider(
            ax=self.slider_dist_max_ax,
            label='Cort_dist, max, mm',
            valmin=-20,
            valmax=50,
            valinit=5,
        )

        self.segment_slider = Slider(
            ax=self.slider_segment_ax,
            label='Segment',
            valmin=0,
            valmax=(max_time_segment + 1),
            valinit=0,
        )

        # register the update function with each slider
        def render(slider_value):
            self.render(show_head=False)

        def new_time_pos(segment):
            self.segment_nr = int(segment)
            self.render(False)

        self.gof_slider.on_changed(render)
        self.cortical_distance_slider_min.on_changed(render)
        self.cortical_distance_slider_max.on_changed(render)
        self.segment_slider.on_changed(new_time_pos)
        self.filter_radio.on_clicked(render)

        fig.canvas.mpl_connect('button_release_event', self.onclick)
        fig.canvas.mpl_connect('key_release_event', self.on_button)
        fig.tight_layout()

    def onclick(self, event):
        if event.inaxes == self.scatter_ax:
            print('%s click: button=%d, x=%d, y=%d, xdata=%f, ydata=%f' %
                  ('double' if event.dblclick else 'single', event.button,
                   event.x, event.y, event.xdata, event.ydata))
            x, y = event.xdata, event.ydata
            xs = self.example_atoms_filtered['absolute_position']
            ys = self.example_atoms_filtered['frequency']
            distances = (((xs - x) ** 2 + (ys - y) ** 2) ** 0.5).values
            selected_atom_id = np.argmin(distances)
            selected_atom_mmp3_atom_ids = self.example_atoms_filtered.iloc[selected_atom_id].mmp3_atom_ids
            self.render_atom_id = selected_atom_mmp3_atom_ids
            selected_macroatom = self.atoms[self.atoms['mmp3_atom_ids'] == selected_atom_mmp3_atom_ids]

            # print("\n\n\n" + "=" * 80 + "Selected atom:")
            # print(selected_atom.to_string())
            print("\n" + "=" * 80 + "\nSelected macroatom params:")
            print(parametrise_atom(selected_macroatom))

            for param in ['amplitude', 'amplitude_normalized_to_textbook']:
                print(param)
                print('\t'.join([str(i) for i in selected_macroatom['ch_name'].values]))
                print('\t'.join(['{}'.format(int(i)) for i in selected_macroatom[param].values]))
                print('{:0.2f} +- {:0.2f} min {:0.2f}, max {:0.2f}'.format(np.mean(selected_macroatom[param]),
                                                                       np.std(selected_macroatom[param]),
                                                                       selected_macroatom[param].values.min(),
                                                                       selected_macroatom[param].values.max()
                                                                       )
                      )
                print()
            print("=" * 80)

            if event.button == 1: # only change activeness when left
                macroatom_mask = self.atoms['mmp3_atom_ids'] == selected_atom_mmp3_atom_ids
                new_importance_value = np.logical_not(self.atoms[macroatom_mask]["atom_importance"])
                self.atoms.loc[macroatom_mask, 'atom_importance'] = new_importance_value
                self.render(show_head=True)
            else:
                self.render(show_head=True)

    def on_button(self, event):
        print(event.key)
        if event.key == 'right':
            self.segment_nr = self.segment_nr + 1
            self.render(show_head=False)
        if event.key == 'left':
            self.segment_nr = self.segment_nr - 1
            if self.segment_nr < 0:
                self.segment_nr = 0
            else:
                self.render(show_head=False)
        if event.key == 'pageup':
            self.channel_num -= 1
            if self.channel_num < 0:
                self.channel_num = 0
            else:
                self.render(show_head=False)
        if event.key == 'pagedown':
            self.channel_num += 1
            if self.channel_num >= len(self.ch_names):
                self.channel_num = len(self.ch_names) - 1
            else:
                self.render(show_head=False)

        if event.key == 'subtract' or event.key == '-':
            self.microvolt_per_sec = self.microvolt_per_sec * 1.1
            self.render(show_head=False)
        if event.key == 'add' or event.key == '+':
            self.microvolt_per_sec = self.microvolt_per_sec * 0.9
            self.render(show_head=False)

        if event.key == 'end':
            pass

    def get_mp_ch_id(self, channel_number):
        mp_params = self.mp_params
        mp_channel = mp_params['channel_names'][channel_number]
        for raw_ch_id, raw_channel in enumerate(self.ch_names):
            if mp_channel == raw_channel:
                return raw_ch_id

    def render(self, show_head=False):
        time_start = self.segment_nr * self.SEGMENT_LENGTH
        time_stop = self.segment_nr * self.SEGMENT_LENGTH + self.SEGMENT_LENGTH
        timeline = np.arange(time_start, time_stop, 1 / self.SAMPLE_FREQ)
        tags_mask_time = (self.atoms.absolute_position >= time_start) & (self.atoms.absolute_position <= time_stop)
        channel_mask = (self.atoms.ch_id == (self.get_mp_ch_id(self.channel_num) + 1))
        gof_mask = self.atoms['dip_gof'] >= self.gof_slider.val
        cort_dist_mask = np.logical_and(self.cortical_distance_slider_min.val <= self.atoms['dip_distance_to_cortex_voxel'],
                                        self.atoms['dip_distance_to_cortex_voxel'] <= self.cortical_distance_slider_max.val)
        current_atoms = self.atoms[tags_mask_time & channel_mask]

        # none
        if self.filter_radio.value_selected == 'None':
            current_atoms_filtered = self.atoms[tags_mask_time & channel_mask]
        # custom
        if self.filter_radio.value_selected == 'custom filter':
            current_atoms_filtered = self.atoms[tags_mask_time & gof_mask & cort_dist_mask & channel_mask]
        # epileptic activity
        if self.filter_radio.value_selected == 'pw004 epilepsy':
            current_atoms_filtered = self.atoms[tags_mask_time & channel_mask]
            ##################
            from mp_dip.utils.sanity_checking import sanity_check_spikes_slow_politech004
            def sanity_check_spikes_slow_politech004_(atom):
                return sanity_check_spikes_slow_politech004(atom, None)

            new_mask = current_atoms_filtered.apply(sanity_check_spikes_slow_politech004_, axis=1)
            current_atoms_filtered = current_atoms_filtered[new_mask]

        if self.filter_radio.value_selected == 'spindles':
            current_atoms_filtered = self.atoms[tags_mask_time & channel_mask]
            ##################
            def sanity_check_spindle_(atom):
                from mp_dip.utils.sanity_checking import sanity_check_spindle
                return sanity_check_spindle(atom, None)
            new_mask = current_atoms_filtered.apply(sanity_check_spindle_, axis=1)
            current_atoms_filtered = current_atoms_filtered[new_mask]

        if self.filter_radio.value_selected == 'swa':
            current_atoms_filtered = self.atoms[tags_mask_time & channel_mask]
            ##################
            def sanity_check_swa_(atom):
                from mp_dip.utils.sanity_checking import sanity_check_swa
                return sanity_check_swa(atom, None)
            new_mask = current_atoms_filtered.apply(sanity_check_swa_, axis=1)
            current_atoms_filtered = current_atoms_filtered[new_mask]

        if self.filter_radio.value_selected == 'alpha':
            current_atoms_filtered = self.atoms[tags_mask_time & channel_mask]
            ##################
            def sanity_check_alpha_(atom):
                from mp_dip.utils.sanity_checking import sanity_check_alpha
                return sanity_check_alpha(atom, None)
            new_mask = current_atoms_filtered.apply(sanity_check_alpha_, axis=1)
            current_atoms_filtered = current_atoms_filtered[new_mask]


        self.example_atoms = current_atoms
        self.example_atoms_filtered = current_atoms_filtered
        self.timeline = timeline
        reconsctruction = reconstruct(self.example_atoms,
                                      self.timeline,)
        reconstruct_full = self.example_atoms.copy()
        reconstruct_full['atom_importance'] = True
        reconsctruction_full = reconstruct(reconstruct_full,
                                      self.timeline, )

        reconstruct_full_filtered = self.example_atoms_filtered.copy()
        reconstruct_full_filtered['atom_importance'] = True
        reconsctruction_full_filtered = reconstruct(reconstruct_full_filtered,
                                           self.timeline, )


        vertical_lines_prepare = timeline % self.SEGMENT_LENGTH
        vertical_lines = timeline[vertical_lines_prepare < 1.9 / self.SAMPLE_FREQ]

        signal_step = scoreatpercentile(reconsctruction_full, 99) * 2

        self.reconstruction_ax.clear()
        self.scatter_ax.clear()
        self.interpolation_ax.clear()
        self.dipole_ax.clear()
        for line in vertical_lines:
            self.reconstruction_ax.axvline(line, color='red', linewidth=0.5)
        self.reconstruction_ax.plot(self.timeline, reconsctruction - signal_step * 2, color='red', linewidth=0.5,)
        self.reconstruction_ax.plot(self.timeline, reconsctruction, color='red', linewidth=0.5,)
        self.reconstruction_ax.plot(self.timeline, reconsctruction_full_filtered - signal_step, color='gray', linewidth=0.5,)
        self.reconstruction_ax.plot(self.timeline, reconsctruction_full, color='blue', linewidth=0.5,)

        self.scatter_ax.scatter(self.example_atoms_filtered['absolute_position'],
                                self.example_atoms_filtered['frequency'],
                                s=self.example_atoms_filtered['modulus'],
                                c=self.example_atoms_filtered['atom_importance'],
                                vmin=0,
                                vmax=1,
                                alpha=0.6,
                                )
        scale = (self.timeline[-1] - self.timeline[0]) * self.microvolt_per_sec
        self.reconstruction_ax.set_ylim([-scale/2, scale/2])

        channel = self.ch_names[self.channel_num]

        self.scatter_ax.set_title('{} segment: {}'.format(channel, self.segment_nr + 1))
        # mmp1/3 only
        if show_head:
            amp_visualisation_atom = self.atoms[self.atoms.mmp3_atom_ids == self.render_atom_id]
            try:
                topomap_atom = amp_visualisation_atom.copy()
                topomap_atom_mask = np.ones(len(topomap_atom), dtype=bool)

                for channel_to_ignore in self.channels_to_ignore:
                    topomap_atom_mask *= amp_visualisation_atom.ch_name != channel_to_ignore
                topomap_atom = topomap_atom[topomap_atom_mask]

                amplitudes = topomap_atom.amplitude.values
                signs = amplitude_signs(topomap_atom)

                fake_info = mne.create_info(list(topomap_atom.ch_name), 128, ch_types='eeg')
                montage = mne.channels.make_standard_montage('standard_1005')
                fake_info.set_montage(montage)
                mne.viz.plot_topomap(amplitudes * signs, pos=fake_info, outlines='skirt',
                                     axes=self.interpolation_ax)
            except OSError:
                pass
            # draw dipole
            dipole_atom = amp_visualisation_atom.iloc[0]
            dipole_pos = [dipole_atom['dip_posx'], dipole_atom['dip_posy'],
                          dipole_atom['dip_posz']]
            dipole_ori = [dipole_atom['dip_orix'], dipole_atom['dip_oriy'],
                          dipole_atom['dip_oriz']]
            gof = dipole_atom['dip_gof']
            cort_dist = dipole_atom['dip_distance_to_cortex_voxel']
            amplitude = dipole_atom['dip_amplitude']
            dip = mne.Dipole([0], [dipole_pos], [amplitude, ], [dipole_ori], [gof],
                             name=None, conf=None, khi2=None, nfree=None,
                             verbose=None)
            dip.plot_locations(transform, 'fsaverage', subjects_dir,
                                     show=False, fig=None, ax=self.dipole_ax, block=False)
            closest_cortical_label = dipole_atom['dip_closest_cortex_voxel_label']
            pb.suptitle('Dipole: GOF: %.1f%%, %.1fnAm, Cort_dist: %.1f mm\n' % (gof, amplitude * 1e9, cort_dist) + closest_cortical_label + ' (%0.1f, %0.1f, %0.1f) mm' % tuple(np.array(dipole_pos) * 100))

        self.fig.canvas.draw()

    def show_atoms(self):
        self.render_atom_id = 0
        self.render(show_head=False)
        pb.show()
        self.create_figure()


def filter_atoms_by_iteration(atoms, max_iter=175):
    return atoms[atoms.iteration <= max_iter]


def main():
    np.set_printoptions(precision=3)
    np.set_printoptions(suppress=True)
    np.set_printoptions(linewidth=120)
    parser = argparse.ArgumentParser(description="Analysing MP decompositions.")
    parser.add_argument('-i', '--ignored-channels', action='store',
                        help='comma seperated list of channels to be dropped before dipole fitting', type=str,
                        default='')
    parser.add_argument('-d', '--mp-dir', nargs='?', type=str, help='MP Output directory')
    parser.add_argument('-m', '--montage', action='store', help='Montages: none, ears, transverse', default='none')
    parser.add_argument('-mp', '--mp-type', action='store', help='mp type: smp, mmp3', default='smp')
    parser.add_argument('file',  action='store', metavar='file', help="path to *.raw")
    parser.add_argument('-u', '--unique-file-names', action='store', type=str,
                        default='y',
                        help='are raw filenames unique? If no MP folder will contain subfolders')

    namespace = parser.parse_args()

    file = namespace.file


    # fast track:
    # path_to_book, path_to_book_params = get_mp_book_paths(namespace.mp_dir, file,
    #                                                       namespace.montage, namespace.mp_type,
    #                                                       unique_file_names=namespace.unique_file_names == 'y')
    # save_path = os.path.join(os.path.dirname(path_to_book),
    #                          'dipole_fits',
    #                          os.path.basename(path_to_book) + '_dipole_fits.pickle')
    # print("Reading pickle")
    # try:
    #     atoms_original_sort = pandas.read_pickle(save_path)
    # except FileNotFoundError:
    #     # in case it's legacy book
    #     save_path = os.path.join(os.path.dirname(path_to_book),
    #                              'dipole_fits',
    #                              os.path.basename(path_to_book)[:-2] + 'b' + '_dipole_fits.pickle')
    #     atoms_original_sort = pandas.read_pickle(save_path)
    # mp_params = json.load(open(path_to_book_params))

    # slow track, more atom params
    atoms_original_sort, raw, preanalysed_raw, mp_params = get_atoms_with_primary_parametrisation(
        file, None,
        namespace.montage,
        namespace.mp_dir,
        namespace.mp_type,
        '', max_iter=175, unique_file_names=namespace.unique_file_names == 'y')

    channels_to_ignore = [i for i in namespace.ignored_channels.split(',') if i]
    del raw
    del preanalysed_raw
    atom_vis = AtomVisualiser3D(atoms_original_sort, mp_params,
                                mp_path=namespace.mp_dir,
                                mp_type=namespace.mp_type,
                                montage=namespace.montage,
                                channels_to_ignore=channels_to_ignore,
                             )

    atom_vis.show_atoms()

if __name__ == '__main__':
    main()
   # python atom_visualiser_3d.py -d D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\mp\ -m transverse -mp mmp3 D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\*.raw
   # python atom_visualiser_3d.py -d D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\mp\ -m transverse -mp mmp3 D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\*.raw
   # python atom_visualiser_3d.py -d D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\mp\ -m none -mp mmp3 D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\*.raw
   # python atom_visualiser_3d.py -d D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\mp\ -m none -mp mmp1 D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\*.raw
   # python atom_visualiser_3d.py -u n -d /dmj/fizmed/mdovgialo/projekt_doktorat/dane_do_odtworzenia_eeg_z_eeg/mp/ -m none -mp mmp1 /dmj/fizmed/mdovgialo/projekt_doktorat/dane_do_odtworzenia_eeg_z_eeg/patients/p300-wzrokowe/M/mirek_P300_wz2_2016_Jul_21_1110.obci.raw
   # python atom_visualiser_3d.py -u n -d /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/mp_0.9.6/ -m none -mp mmp1 /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/e1.raw
   # python atom_visualiser_3d.py -u n -d /repo/coma/NOWY_OPUS_COMA/DANE_POLITECHNIKA/DANE_PACJENTOW_CLEAN/DANE/MP -m none -mp mmp1 /repo/coma/NOWY_OPUS_COMA/DANE_POLITECHNIKA/DANE_PACJENTOW_CLEAN/DANE/PW-EEG-004/EEG/A2.raw
   # python atom_visualiser_3d.py -i A1,A2 -u n -d /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/mp -m none -mp mmp1 /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/01-05-0015 PSG.raw
    # fix ipython:
    # globals().update(locals())

    # todo read stuff about 3d localisation:
    # https://www.frontiersin.org/articles/10.3389/fnins.2021.695668/full