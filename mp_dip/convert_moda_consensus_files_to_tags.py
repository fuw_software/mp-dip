import argparse
import glob
import os

from mp_dip.obci_readmanager.signal_processing.tags import tags_file_writer as tags_writer

import mne
from tqdm import tqdm
import pandas as pd

from mp_dip.utils.utils import TAG_DEFS

tag_convert_dict = {'spindle': 'Spindle (wrzeciono)',
                    'segmentViewed': 'segmentViewed',
                    }

def main():
    parser = argparse.ArgumentParser(description="Converting  MODA MASS spindle detection consensus files to tags.")
    parser.add_argument('files', nargs='+', metavar='file', help="path txt files with annotations")
    parser.add_argument('-o', '--outdir', nargs='?', type=str, help='Output directory')

    namespace = parser.parse_args()
    if len(namespace.files) == 1:
        files_to_work = glob.glob(namespace.files[0])
    else:
        files_to_work = namespace.files
    for file in tqdm(files_to_work):
        annotations = pd.read_csv(file, sep='\t')
        tags = []

        for id, annotation in annotations.iterrows():
            start = annotation['startSec']
            stop = start + annotation['durationSec']
            label = tag_convert_dict.get(annotation['eventName'], None)
            if label is not None:
                tag = {"type": "CHANNEL",
                       'channelNumber': -1,
                       'start_timestamp': start,
                       'end_timestamp': stop,
                       'name': label,
                       'desc': {},
                       }
                tags.append(tag)

        def tag_sort_key(tag):
            return tag['start_timestamp']

        tags.sort(key=tag_sort_key)

        output_dir = namespace.outdir

        os.makedirs(output_dir, exist_ok=True)
        tag_file_name = os.path.basename(file).split('_')[0] + ' PSG.tag'
        tags_file = os.path.join(output_dir, tag_file_name)

        tag_writer = tags_writer.TagsFileWriter(tags_file,
                                                p_defs=TAG_DEFS)
        for tag in tags:
            tag_writer.tag_received(tag)
        tag_writer.finish_saving(0.0)



if __name__ == '__main__':
    main()
