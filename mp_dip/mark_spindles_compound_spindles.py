import glob
from copy import copy
import numpy as np
import argparse
import pandas
from tqdm import tqdm
from tqdm.contrib.concurrent import process_map
import pickle
from mp_dip.mark_structures import RawChannelStandin, create_tag
from mp_dip.utils.data_preprocessing_utils import convert_montage_channel_nr_to_raw
from mp_dip.obci_readmanager.signal_processing.tags import tags_file_writer as tags_writer

from mp_dip.utils.sanity_checking import sanity_check_spindle, sanity_check_spikes_slow_politech004, \
    sanity_check_alpha, sanity_check_swa, sanity_check_spindle_compound_candidate
from mp_dip.utils.utils import TAG_DEFS, classes_d, get_mp_book_paths, get_atoms_with_primary_parametrisation, split_to_macroatoms


def create_tag_from_compound(label, macroatoms, preanalysed_raw, raw, snr_treshold=-30):
    if snr_treshold is not None:
        tags_detected = []
        channels = macroatoms[0].channel_name.values
        for channel_id, channel in enumerate(channels):
            atoms = [macroatom[macroatom['channel_name']==channel] for macroatom in macroatoms]

            compound_tags = []
            skip_until = 0
            for nr, atom in enumerate(atoms):
                if nr < skip_until:
                    continue
                if (atom.snr < snr_treshold).all():
                    continue
                touching_tags = [atom]
                touching_tag_nr = nr
                cluster_end = (atom.absolute_position + atom.width / 2).values[0]
                cluster_start = (atom.absolute_position - atom.width / 2).values[0]
                for next_nr, next_tag in enumerate(atoms[nr+1:]):
                    next_macroatom_start = (next_tag.absolute_position - next_tag.width / 2).values[0]
                    next_macroatom_end = (next_tag.absolute_position + next_tag.width / 2).values[0]
                    time_criterium = (next_macroatom_start - cluster_end) <= 0
                    snr_criterium = (next_tag.snr >= snr_treshold).all()
                    if time_criterium and snr_criterium:
                        touching_tags.append(next_tag)
                        cluster_end = next_macroatom_end
                        touching_tag_nr = nr + next_nr
                    else:
                        break

                compound_length = cluster_end - cluster_start

                # mark usage
                sanity_check_spindle
                if 0.5 <= compound_length <= 3.5:
                    compound_tags.append([cluster_start, cluster_end, touching_tags])
                    skip_until = touching_tag_nr

            # save the tag
            for start, end, atoms in compound_tags:
                if len(atoms) == 1:
                    continue
                iteration = [i['iteration'].values[0] for i in atoms]
                frequency = ', '.join(['{:0.1f}'.format(i['frequency'].values[0]) for i in atoms])
                max_amp = np.max(sum([list(i['amplitude'].values) for i in atoms], []))
                common_desc = {
                    'example_nr': str(0),
                    'frequency': str(frequency),
                    'amplitude_P2P': str(max_amp),
                    'width': str(end - start),
                    'iteration': iteration,
                }

                gofs = []
                dip_amplitude = []
                for atom in atoms:
                    gofs.append(atom['dip_gof'].values[0])
                    dip_amplitude.append(atom['dip_amplitude'].values[0] * 1000000000)
                snrs = [float(i['snr']) for i in atoms]
                desc = common_desc.copy()
                channel_for_tag = convert_montage_channel_nr_to_raw(raw,
                                                                    preanalysed_raw,
                                                                    channel_id)
                channel_montaged = preanalysed_raw.ch_names[channel_id]
                desc['referenceAsReadable'] = str(channel_montaged)
                snr = max(snrs)
                desc['snr'] = str(snr)
                desc['snrs'] = str(', '.join(['{:0.1f}'.format(float(i)) for i in snrs]))
                desc['compound'] = str('True')

                tag = {'channelNumber': channel_for_tag,
                       'start_timestamp': start,
                       'end_timestamp': end,
                       'name': label,
                       'desc': desc,
                       }
                tags_detected.append(tag)




    else:
        start = macroatoms[0].absolute_position.values[0] - macroatoms[0].width.values[0] / 2
        stop = macroatoms[-1].absolute_position.values[0] + macroatoms[-1].width.values[0] / 2

        channels_all = set()
        for macroatom in macroatoms:
            max_amp = np.max(macroatom['amplitude'])
            amp_threshold = max_amp * 0.5
            amp_mask = (macroatom['amplitude'].values > amp_threshold)
            # channels where structure is strong enough to count as visible
            channels = np.arange(0, len(preanalysed_raw.ch_names))[amp_mask]
            channels_all.update(list(channels))

        max_amp = np.max(sum([list(i['amplitude'].values) for i in macroatoms], []))
        frequency = ', '.join(['{:0.1f}'.format(i['frequency'].values[0]) for i in macroatoms])

        iteration = [i['iteration'].values[0] for i in macroatoms]
        common_desc = {
                       'example_nr': str(0),
                       'frequency': str(frequency),
                       'amplitude_P2P': str(max_amp),
                       'width': str(stop - start),
                       'iteration': iteration,
                       }

        # don't add dipole labels to "nones" - takes to long

        gofs = []
        dip_amplitude = []
        for macroatom in macroatoms:
            gofs.append(macroatom['dip_gof'].values[0])
            dip_amplitude.append(macroatom['dip_amplitude'].values[0] * 1000000000)

        tags_detected = []
        for channel in list(channels_all):
            snrs = [i['snr'].values[channel] for i in macroatoms]
            snr = max(snrs)
            desc = common_desc.copy()

            channel_for_tag = convert_montage_channel_nr_to_raw(raw,
                                                                preanalysed_raw,
                                                                channel)
            channel_montaged = preanalysed_raw.ch_names[channel]
            desc['referenceAsReadable'] = str(channel_montaged)
            desc['snr'] = str(snr)
            desc['snrs'] = str(', '.join(['{:0.1f}'.format(float(i)) for i in snrs]))
            desc['compound'] = str('True')

            tag = {'channelNumber': channel_for_tag,
                   'start_timestamp': start,
                   'end_timestamp': stop,
                   'name': label,
                   'desc': desc,
                   }
            tags_detected.append(tag)
    return tags_detected


def analyze_macroatoms(macroatom):
    main_atom_id = np.argmax(macroatom.amplitude.values)
    main_atom = macroatom.iloc[main_atom_id]
    # todo atom can be only one thing now?
    if sanity_check_spindle(main_atom, None):
        tagname = 'Spindle (wrzeciono)'
        return [macroatom, tagname]

    if sanity_check_spindle_compound_candidate(main_atom, None):
        tagname = 'Spindle (wrzeciono);compound_candidate'
        return [macroatom, tagname]



def main():
    np.set_printoptions(precision=3)
    np.set_printoptions(suppress=True)
    np.set_printoptions(linewidth=120)
    parser = argparse.ArgumentParser(description="Mark_using nn")
    parser.add_argument('-d', '--mp-dir', nargs='?', type=str, help='MP Output directory')
    parser.add_argument('-m', '--montage', action='store', help='Montages: none, ears, transverse', default='none')
    parser.add_argument('-mp', '--mp-type', action='store', help='mp type: smp, mmp3', default='smp')
    parser.add_argument('-o', '--output',  action='store', help='tags_output', default='')
    parser.add_argument('-s', '--save-marked-macroatoms',  action='store',
                        help='Saves a pickle of selected macroatoms along tags',
                        default='y')
    parser.add_argument('-u', '--unique-file-names', action='store', type=str,
                        default='y',
                        help='are raw filenames unique? If no MP folder will contain subfolders')

    parser.add_argument('files', nargs='+', metavar='file', help="path to *.raw")

    namespace = parser.parse_args()
    if len(namespace.files) == 1:
        files_to_work = glob.glob(namespace.files[0])
    else:
        files_to_work = namespace.files
    print(files_to_work)
    for file in files_to_work:

        if namespace.unique_file_names == 'y':
            output_dir = namespace.output
        else:
            patiend_id = os.path.basename(os.path.abspath(os.path.join(os.path.dirname(file), '..')))
            output_dir = os.path.join(namespace.output, patiend_id)
        file_out_name_base = os.path.join(output_dir,
                                          os.path.splitext(
                                              os.path.basename(
                                                  file))[
                                              0])
        # check for overwriting
        if os.path.exists(file_out_name_base + '.tag'):
            print("Will not everwrite ", file_out_name_base + '.tag', 'skipping')
            continue



        # check if dipoles exist

        path_to_book, path_to_book_params = get_mp_book_paths(namespace.mp_dir, file,
                                                              namespace.montage, namespace.mp_type,
                                                              unique_file_names=namespace.unique_file_names == 'y')
        dipole_path = os.path.join(os.path.dirname(path_to_book),
                                   'dipole_fits',
                                   os.path.basename(path_to_book) + '_dipole_fits.pickle')
        if not os.path.exists(dipole_path):
            print("No dipole fits for ", file, 'skipping', "Dipole path:", dipole_path)
            continue

        if not os.path.exists(path_to_book_params):
            print("no mp for ", file, 'skipping', path_to_book_params)
            continue

        ##############################

        try:
            atoms, raw, preanalysed_raw, mp_params = get_atoms_with_primary_parametrisation(
                file, None,
                namespace.montage,
                namespace.mp_dir,
                namespace.mp_type,
                '', max_iter=175, unique_file_names=namespace.unique_file_names == 'y', copy=True)
            preanalysed_raw_standin = RawChannelStandin(preanalysed_raw)
            raw_standin = RawChannelStandin(raw)
            del raw
            del preanalysed_raw
            preanalysed_raw = preanalysed_raw_standin
            raw = raw_standin
        except FileNotFoundError as e:
            print(e)
            continue

        macroatoms = split_to_macroatoms(atoms)
        tagged_macroatoms = process_map(analyze_macroatoms, macroatoms,
                                        desc='naming, classifying macroatoms', chunksize=1024)

        # # debug
        # tagged_macroatoms = map(analyze_macroatoms, macroatoms,)
        tagged_macroatoms = list(filter(lambda x: x is not None, tqdm(tagged_macroatoms, desc='filtering empty tags')))

        # tagged macroatoms is sorted

        compound_tags = []
        skip_until = 0
        for nr, tagged_macroatom in tqdm(enumerate(tagged_macroatoms), desc='compounds?'):
            if nr < skip_until:
                continue
            if 'Spindle (wrzeciono);compound_candidate' == tagged_macroatom[1]:
                touching_tags = [tagged_macroatom]
                cluster_end = tagged_macroatom[0].absolute_position.values[0] + tagged_macroatom[0].width.values[0] / 2
                cluster_start = tagged_macroatom[0].absolute_position.values[0] - tagged_macroatom[0].width.values[0] / 2
                for next_nr, next_tag in enumerate(tagged_macroatoms[nr:]):
                    next_macroatom_start = next_tag[0].absolute_position.values[0] - next_tag[0].width.values[0] / 2
                    next_macroatom_end = next_tag[0].absolute_position.values[0] + next_tag[0].width.values[0] / 2
                    time_criterium = (next_macroatom_start - cluster_end) <= 0
                    if time_criterium:  # todo add snr criterium??
                        touching_tags.append(next_tag)
                        cluster_end = next_macroatom_end
                        touching_tag_nr = nr + next_nr
                    else:
                        break

                compound_length = cluster_end - cluster_start

                # mark usage
                sanity_check_spindle
                if 0.5 <= compound_length <= 3.5:
                    compound_tags.append(touching_tags)
                    skip_until = touching_tag_nr

        # compound_tag_length = (compound_tag[-1][0].absolute_position.values[0] + compound_tag[-1][0].width.values[0] / 2) - (compound_tag[0][0].absolute_position.values[0] - compound_tag[0][0].width.values[0] / 2)

        tagged_macroatoms = list(filter(lambda x: x[1] == 'Spindle (wrzeciono)', tqdm(tagged_macroatoms, desc='filtering pure spindles')))

        def create_tags_for_selected_macroatoms(item):
            macroatom, tagname = item
            tags_this = create_tag(tagname, macroatom, macroatom['mmp3_atom_ids'].values[0], preanalysed_raw, raw)
            return tags_this

        tags = sum(list(map(create_tags_for_selected_macroatoms, tqdm(tagged_macroatoms, desc='creating tags'))), [])


        def create_tags_for_selected_compound_macroatoms(item):
            macroatoms = [i[0] for i in item]
            tags_this = create_tag_from_compound("Spindle (wrzeciono)", macroatoms, preanalysed_raw, raw)
            return tags_this

        tags_compound = sum(list(map(create_tags_for_selected_compound_macroatoms, tqdm(compound_tags, desc='creating compound tags'))), [])

        tags.extend(tags_compound)
        del tags_compound

        #############################
        def tag_sort_key(tag):
            return tag['start_timestamp']

        tags.sort(key=tag_sort_key)
        tags_all_filtered = []
        for tag in tags:
            if tag['start_timestamp'] > 0:
                tags_all_filtered.append(tag)


        if namespace.unique_file_names == 'y':
            output_dir = namespace.output
        else:
            patiend_id = os.path.basename(os.path.abspath(os.path.join(os.path.dirname(file), '..')))  # legacy
            # patiend_id = os.path.basename(os.path.abspath(os.path.join(os.path.dirname(file))))  # n400 slowa stuff
            output_dir = os.path.join(namespace.output, patiend_id)

        os.makedirs(output_dir, exist_ok=True)
        file_out_name_base = os.path.join(output_dir,
                                          os.path.splitext(
                                              os.path.basename(
                                                  file))[
                                              0])
        writer = tags_writer.TagsFileWriter(file_out_name_base + '.tag',
                                            p_defs=TAG_DEFS)
        for tag in tags_all_filtered:
            writer.tag_received(tag)
        writer.finish_saving(0.0)
        del tags_all_filtered

        if namespace.save_marked_macroatoms == 'y':
            with open(file_out_name_base + '.pickle', 'wb') as macroatoms_save_file:
                pickle.dump(tagged_macroatoms, macroatoms_save_file)
        del tagged_macroatoms

        if namespace.save_marked_macroatoms == 'y':
            with open(file_out_name_base + '_compound_tags.pickle', 'wb') as macroatoms_save_file:
                pickle.dump(compound_tags, macroatoms_save_file)
        del compound_tags

# https://github.com/raphaelvallat/yasa


if __name__ == '__main__':
    # turn off gpu for now
    import os
    os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
    main()



   # nice python mark_spindles_compound_spindles.py -d /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/mp -u n -m none -mp mmp1 -o /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/tags_experiments/tags_spindles_no_dipole_gof_location_compound_spindles /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/*.raw
