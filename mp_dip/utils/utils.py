import json
import pickle
from functools import partial

import mne
import os

import pandas
import pandas as pd
import numpy as np
import scipy.signal as ss
from tqdm import tqdm
from tqdm.contrib.concurrent import process_map

from mp_dip.obci_readmanager.signal_processing.read_manager import ReadManager
from scipy.stats import scoreatpercentile
from mp_dip.obci_readmanager.signal_processing.tags import tags_file_writer as tags_writer


from mp_dip.old.config import RESAMPLING_FREQUENCY
from mp_dip.utils.db_atoms_utils import read_db_atoms
from mp_dip.utils.data_preprocessing_utils import ATOM_FEATURES, in_features, convolve_data
from mp_dip.utils.multivariate_atom_analysis import get_kernels, parametrise_atom
from mp_dip.utils.snr_calculation import calculate_atom_neighborhood_snr_signal


class ReferenceException(Exception):
    pass

# todo remove this,
# used in classification and normalised
# todo remove this??
in_channels_mmp3 = ["Fp1", "Fp2", "F3", "F4", "C3", "C4", "P3", "P4", "O1", "O2", "F7", "F8", "T3", "T4", "T5", "T6"]

# must be sorted alphabetically
classes_d = {'Kompleks K': 0,
 'None': 1,
 'Spindle (wrzeciono)': 2,
 'Wierzchołkowa fala ostra': 3}


ATTR_DICT = [dict(code="example_nr", displayName="example_nr", visible="false"),
             dict(code="pred", displayName="sureness", visible="false"),
             dict(code="new_pred", displayName="sureness_new", visible="false"),
             dict(code="pred_raw", displayName="pred_raw", visible="false"),
             dict(code="pred_2nd", displayName="pred_2nd", visible="false"),
             dict(code="frequency", displayName="frequency", visible="false"),
             dict(code="amplitude", displayName="amplitude", visible="false"),
             dict(code="amplitude_P2P", displayName="amplitude_P2P", visible="false"),
             dict(code="iteration", displayName="iteration", visible="false"),
             dict(code="width", displayName="width", visible="false"),
             dict(code="source", displayName="source", visible="false"),
             dict(code="dip_gof", displayName="dip_gof", visible="false"),
             dict(code="dip_amplitude", displayName="dip_amplitude", visible="false"),
             dict(code="snr", displayName="snr", visible="false"),
             dict(code="snrs", displayName="snrs", visible="false"),
             dict(code="compound", displayName="compound", visible="false"),
             ]

TAG_DEFS = [dict(name="segmentViewed",
                 description="Segment checked by MODA experiment",
                 fillColor="C8E8DC",
                 outlineColor="C8E8DC",
                 outlineWidth="1.0",
                 outlineDash="",
                 keyShortcut="M",
                 marker="0",
                 visible="1",
                 attributes=ATTR_DICT,
                 ),
            dict(name="SWA",
                 description="SWA",
                 fillColor="B0FFE1",
                 outlineColor="B0FFE1",
                 outlineWidth="1.0",
                 outlineDash="",
                 keyShortcut="D",
                 marker="0",
                 visible="1",
                 attributes=ATTR_DICT,
                 ),
            dict(name="alpha",
                 description="Alpha Pack",
                 fillColor="0000ff",
                 outlineColor="ff0006",
                 outlineWidth="1.0",
                 outlineDash="",
                 keyShortcut="A",
                 marker="0",
                 visible="1",
                 attributes=ATTR_DICT,
                 ),

            dict(name="Wierzchołkowa fala ostra",
                 description="Vertex sharp wave",
                 fillColor="ff0011",
                 outlineColor="ff0006",
                 outlineWidth="1.0",
                 outlineDash="",
                 keyShortcut="W",
                 marker="0",
                 visible="1",
                 attributes=ATTR_DICT,
                 ),
            dict(name="None",
                 description="MP atom not marked",
                 fillColor="FAFAFA",
                 outlineColor="FAFAFA",
                 outlineWidth="1.0",
                 outlineDash="",
                 keyShortcut="N",
                 marker="0",
                 visible="1",
                 attributes=ATTR_DICT,
                 ),

            dict(name="Spindle (wrzeciono)",
                 description="coma spindle",
                 fillColor="0099ff",
                 outlineColor="008dff",
                 outlineWidth="1.0",
                 outlineDash="",
                 keyShortcut="S",
                 marker="0",
                 visible="1",
                 attributes=ATTR_DICT,
                 ),
            dict(name="Kompleks K",
                 description="K-complex (jak we śnie)",
                 fillColor="00fffb",
                 outlineColor="00f9ff",
                 outlineWidth="1.0",
                 outlineDash="",
                 keyShortcut="K",
                 marker="0",
                 visible="1",
                 attributes=ATTR_DICT,
                 )
            ]


def reference_raw(raw, montage):
    if montage == 'ears':
        references = [['M1', 'M2'], ['A1', 'A2', ], ['EEG A1', 'EEG A2']]
        for i in references:
            if set(i).issubset(set(raw.ch_names)):
                raw = raw.set_eeg_reference(ref_channels=i)
                raw = raw.drop_channels(i)
                return raw
    elif montage == 'none':
        return raw
    elif montage == 'transverse':
        anodes = 'Fp2,F8,T4,T6,Fp1,F7,T3,T5,Fp2,F4,C4,P4,Fp1,F3,C3,P3'.split(',')
        cathodes = 'F8,T4,T6,O2,F7,T3,T5,O1,F4,C4,P4,O2,F3,C3,P3,O1'.split(',')
        new = mne.set_bipolar_reference(raw, anode=anodes,cathode=cathodes)
        bipolars = [i['ch_name'] for i in new.info['chs'] if i['coil_type'] == 5]
        new = new.pick_channels(bipolars)
        return new
    raise ReferenceException("Cant set reference")


def preanalyse_raw(raw, montage, resample=True, copy=False, misc=False):  # misc is for Nowowiejska files with montaged channels
    if copy:
        raw = raw.copy()
    raw = raw.pick_types(meg=False, eeg=True, stim=False, eog=False,
                         ecg=False, emg=False, ref_meg='auto', misc=misc,
                         resp=False, chpi=False, exci=False, ias=False, syst=False,
                         seeg=False, dipole=False, gof=False, bio=False, ecog=False,
                         fnirs=False)
    raw.notch_filter(50)
    # https://mne.tools/stable/auto_tutorials/discussions/plot_background_filtering.html#sphx-glr-auto-tutorials-discussions-plot-background-filtering-py
    raw.filter(0.5, 30, method='iir')
    if resample:
        raw.resample(RESAMPLING_FREQUENCY)
    raw = reference_raw(raw, montage)
    return raw


def read_raw(path_to_raw, tags=None):
    if path_to_raw[-3:] in ['raw', 'bin']:
        rm = ReadManager(path_to_raw[:-3] + 'xml', path_to_raw, tags)
        raw = rm.get_mne_raw()
    
    elif path_to_raw[-3:] in ["edf"]:
        raw = mne.io.read_raw_edf(path_to_raw, preload=True)
        maping = {}
        for chnl in raw.ch_names:
            if chnl.startswith('EEG'):
                maping[chnl] = 'eeg'
            elif chnl.startswith('ECG'):
                maping[chnl] = 'ecg'
            elif chnl.startswith('EOG'):
                maping[chnl] = 'eog'
            elif chnl.startswith('Resp'):
                maping[chnl] = 'resp'
            elif chnl.startswith('EMG'):
                maping[chnl] = 'emg'
            else:
                maping[chnl] = 'misc'
        raw.set_channel_types(maping)
        try:
            raw = raw.drop_channels(["EEG A1", "EEG A2"])
        except ValueError:
            raw = raw.drop_channels(["A1", "A2"])
    else:
        raise Exception("Unknown file type")
    return raw


def get_rms_percentile(x, freq_range, window_width, percentile, fs):
    d, c = ss.butter(2, np.array(freq_range) / (fs / 2), btype='bandpass')
    sig = ss.filtfilt(d, c, x)
    rms = []
    window = int(window_width * fs)
    for i in range(0, sig.shape[0] - window, int(window)):
        s = sig[i:i + window]
        rms.append(np.sqrt(np.mean(np.array(s) ** 2)))
    rms = np.array(rms)
    rms_ampli = scoreatpercentile(rms, percentile) * 2 * np.sqrt(2)
    return rms, rms_ampli


def filter_selected_atoms(atoms, ch_id, freq_range, width_range, width_coeff, amplitude_range, structure_name='',
                          minimum_oscillations=None):
    columns = ['iteration', 'modulus', 'amplitude', 'width', 'frequency', 'struct_len', 'absolute_position', 'offset',
               'ch_id', 'structure_name']
    dtypes = {'iteration': int, 'modulus': float, 'amplitude': float,
              'width': float, 'frequency': float,
              'struct_len': float,
              'absolute_position': float,
              'offset': float,
              'ch_id': int,
              'structure_name': str

    }
    chosen = []
    for atom in atoms:
        channel = int(atom[0])
        if channel == ch_id:
            iteration = atom[1]
            modulus = atom[2]
            amplitude = 2 * atom[3]
            position = atom[4]
            width = atom[5]
            frequency = atom[6]
            struct_len = width_coeff * width
            if (width_range[0] <= width <= width_range[1]) and (amplitude_range[0] <= amplitude <= amplitude_range[1]) and (freq_range[0] <= frequency <= freq_range[1]):
                if minimum_oscillations is not None:
                    if not (struct_len >= 1 / frequency * minimum_oscillations):
                        continue

                offset = position - struct_len / 2
                chosen.append([iteration - 1, modulus, amplitude, width, frequency, struct_len, position, offset, ch_id, structure_name])

    if not chosen:
        df = pd.DataFrame(columns=columns)
    else:
        df = pd.DataFrame(chosen, columns=columns)

    return df.astype(dtype=dtypes)


def get_all_atoms(atoms, raw, mp_params, width_coeff=1, structure_name='', atoms_importance_mask=None):
    columns = ['iteration', 'modulus', 'amplitude', 'amplitude_z_score', 'width', 'frequency', 'phase', 'struct_len', 'absolute_position', 'offset',
               'ch_id', 'structure_name', 'atom_importance', 'channel_name']
    dtypes = {'iteration': int, 'modulus': float, 'amplitude': float,
              'amplitude_z_score': float,
              'width': float, 'frequency': float, 'phase': float,
              'struct_len': float,
              'absolute_position': float,
              'offset': float,
              'ch_id': int,
              'structure_name': str,
              'atom_importance': bool,
              'channel_name': str,

    }

    eeg_data = raw.get_data().T * 1e6 # to microvolts
    means_per_channels = eeg_data.mean(axis=0)
    std_per_channels = eeg_data.std(axis=0)

    chosen = []
    for atom_id, atom in enumerate(tqdm(atoms, desc='primary atom parametrisation')):
        channel = int(atom[0])
        iteration = atom[1]
        modulus = atom[2]
        amplitude = 2 * atom[3]  # load atoms as p2p amplitude
        position = atom[4]
        width = atom[5]
        frequency = atom[6]
        phase = atom[7]
        struct_len = width_coeff * width
        offset = position - struct_len / 2
        mp_chnames = mp_params["channel_names"]
        mp_channel_id = channel - 1
        mp_channel_name = mp_chnames[mp_channel_id]
        mp_channel_in_raw_id = raw.ch_names.index(mp_channel_name)
        amplitude_z_score = (amplitude - means_per_channels[mp_channel_in_raw_id]) / std_per_channels[mp_channel_in_raw_id]
        if atoms_importance_mask is not None:
            atom_importance = atoms_importance_mask[atom_id]
        else:
            atom_importance = False
        chosen.append([iteration - 1, modulus,
                       amplitude, amplitude_z_score, width, frequency, phase, struct_len, position, offset,
                       channel, structure_name,
                       atom_importance, mp_channel_name])

    if not chosen:
        df = pd.DataFrame(columns=columns)
    else:
        df = pd.DataFrame(chosen, columns=columns)

    return df.astype(dtype=dtypes)


def svarog_tags_writer(df, f_name, channel_map=None):
    writer = tags_writer.TagsFileWriter(f_name)
    df = df.sort_values('absolute_position', ascending=True)
    amps = df['amplitude']
    offsets = df['offset']
    widths = df['width']
    bids = df['iteration']
    ch_ids = df['ch_id']
    names = df['structure_name']
    for amp, offset, width, bid, ch_id, name in zip(amps, offsets, widths, bids, ch_ids, names):
        tag_ch_id = ch_id - 1
        if channel_map is not None:
            try:
                tag_ch_ids = channel_map[tag_ch_id]
            except IndexError:
                tag_ch_ids = [-1]
        for tag_ch_id in tag_ch_ids:
            tag = {'channelNumber': tag_ch_id,
                   'start_timestamp': offset,
                   'end_timestamp': offset + width,
                   'name': name,
                   'desc': {'amplitude': amp, 'width': width, 'iteration': bid, 'name': name}}
            writer.tag_received(tag)
    writer.finish_saving(0.0)


def get_mp_to_raw_channel_map(raw, mp_params):
    map = {}
    raw_channel_names = raw.ch_names
    for mp_channel_id, mp_channel_name in enumerate(mp_params["channel_names"]):
        try:
            # in case of bipolar reference
            mp_channels = [i.strip() for i in mp_channel_name.split('-')]
            raw_ids = []
            for mp_channel in mp_channels:
                raw_id = raw_channel_names.index(mp_channel)
                raw_ids.append(raw_id)
            map[mp_channel_id] = raw_ids
        except:
            pass
    return map


def get_mp_book_paths(dirname, path_to_raw, montage, mp_type, unique_file_names=False, force_db=False):
    core_of_path = path_to_raw[:-4]
    if unique_file_names:
        core_of_name = os.path.basename(core_of_path)
    else:
        upfolder1 = os.path.basename(os.path.abspath(os.path.join(core_of_path,
                                                                  '..')))
        upfolder2 = os.path.basename(
            os.path.abspath(os.path.join(core_of_path, '..', '..')))
        core_of_name = os.path.join(upfolder2, upfolder1, os.path.basename(core_of_path))
    core_of_path_dirname = os.path.dirname(core_of_path)
    subfolder = montage + '_' + mp_type
    if dirname is None:
        path_to_book = os.path.join(core_of_path_dirname, subfolder, core_of_name + '_mp.db')
        if not force_db:
            if not os.path.exists(path_to_book):
                # in case of reading legacy book
                path_to_book = os.path.join(core_of_path_dirname, subfolder, core_of_name + '_mp.b')
        path_to_book_params = os.path.join(core_of_path_dirname, subfolder, core_of_name + '_mp_params.json')
        os.makedirs(os.path.join(core_of_path_dirname, subfolder), exist_ok=True)
    else:
        os.makedirs(dirname, exist_ok=True)
        os.makedirs(os.path.join(dirname, subfolder), exist_ok=True)
        os.makedirs(os.path.join(dirname, subfolder, core_of_name),
                    exist_ok=True)
        path_to_book = os.path.join(dirname, subfolder, core_of_name + '_mp.db')
        if not force_db:
            if not os.path.exists(path_to_book):
                # in case of reading legacy book
                path_to_book = os.path.join(dirname, subfolder, core_of_name + '_mp.b')
        path_to_book_params = os.path.join(dirname, subfolder, core_of_name + '_mp_params.json')
    return path_to_book, path_to_book_params


def example_normalisation(examples, mp_params):
    examples = np.copy(examples)
    mmp = (len(examples.shape) == 4)
    mask = []
    for i in ATOM_FEATURES:
        mask.append(i in in_features)
    if mmp:
        examples = examples[:, :, :, mask]
    else:
        examples = examples[:, :, mask]

    for nr, example in enumerate(examples):
        if mmp:
            examples[nr] = examples[nr] - np.array([0, 0, 0, 0, 0, examples[nr, 0, 0, in_features.index('absolute_position')]])
            examples[nr] = examples[nr] / np.array([100, 1, 1, 30, np.pi, 1])
        else:
            examples[nr] = examples[nr] - np.array([0, 0, 0, 0, 0, examples[nr, 0, in_features.index('absolute_position')]])
            examples[nr] = examples[nr] / np.array([100, 1, 1, 30, np.pi, 1])

    return examples


def merge_tags(tags):
    unique_tags = list(set([tag['name'] for tag in tags]))
    timeline = np.arange(0, tags[-1]['end_timestamp'], 0.1)
    tags_coding = np.zeros((len(unique_tags), len(timeline)), dtype=bool)
    for tag in tags:
        index = unique_tags.index(tag['name'])
        start = tag['start_timestamp']
        stop = tag['end_timestamp']
        tags_coding[index, (timeline >= start) * (timeline < stop)] = True

    tags_coding_longer = np.concatenate([np.zeros((len(unique_tags), 1)),
                                         tags_coding,
                                         np.zeros((len(unique_tags), 1))], axis=1)
    starts_stops = np.diff(tags_coding_longer, axis=1)
    merged_tags = []
    for nr, tag_name in enumerate(unique_tags):
        starts = timeline[starts_stops[nr][1:] == 1]
        stops = timeline[starts_stops[nr][1:] == -1]
        for start, stop in zip(starts, stops):
            tag = {'channelNumber': -1,
                   'start_timestamp': start,
                   'end_timestamp': stop,
                   'name': tag_name,
                   'desc': {}}
            merged_tags.append(tag)
    merged_tags = sorted(merged_tags, key=lambda x: x['start_timestamp'])
    return merged_tags

def print_cm(cm, labels, hide_zeroes=False, hide_diagonal=False, hide_threshold=None):
    """pretty print for confusion matrixes"""
    columnwidth = max([len(x) for x in labels]+[5]) # 5 is value length
    empty_cell = " " * columnwidth
    # Print header
    print ("    " + empty_cell, end=' ')
    for label in labels:
        print ("%{0}s".format(columnwidth) % label, end=' ')
    print()
    # Print rows
    for i, label1 in enumerate(labels):
        print ("%{0}s".format(columnwidth) % label1, end=' ')
        for j in range(len(labels)):
            cell = "%{0}.1f".format(columnwidth) % cm[i, j]
            if hide_zeroes:
                cell = cell if float(cm[i, j]) != 0 else empty_cell
            if hide_diagonal:
                cell = cell if i != j else empty_cell
            if hide_threshold:
                cell = cell if cm[i, j] > hide_threshold else empty_cell
            print (cell, end=' ')
        print()


def read_cache_or_create_parametrised_and_save(file, namespace):
    file_base_name = os.path.basename(file)
    cachefile_examples_nn = file_base_name + '_first_level_feature_cache.bz2'
    cachefile_examples_nn_full_path = os.path.join(namespace.cache, cachefile_examples_nn)
    pickles_raws_path = os.path.join(namespace.cache,  file_base_name + '_raws.pickle')
    pickles_macroatoms_path = os.path.join(namespace.cache,  file_base_name + '_macroatoms.pickle')
    if namespace.cache and os.path.exists(cachefile_examples_nn_full_path):
        print("Reading from cache", namespace.cache)
        examples_nn = pandas.read_pickle(cachefile_examples_nn_full_path)
        with open(pickles_raws_path, 'rb') as pickes_raws_file:
            raw, preanalysed_raw = pickle.load(pickes_raws_file)
        with open(pickles_macroatoms_path, 'rb') as pickles_macroatoms_file:
            macroatoms = pickle.load(pickles_macroatoms_file)
        return examples_nn, macroatoms, raw, preanalysed_raw
    else:
        if namespace.cache:
            os.makedirs(namespace.cache, exist_ok=True)
        examples_nn, macroatoms, raw, preanalysed_raw = create_parametrised_data(file, namespace)
        if namespace.cache:
            examples_nn.to_pickle(cachefile_examples_nn_full_path)
            with open(pickles_raws_path, 'wb') as pickes_raws_file:
                pickle.dump([raw, preanalysed_raw], pickes_raws_file)
            with open(pickles_macroatoms_path, 'wb') as pickles_macroatoms_file:
                pickle.dump(macroatoms, pickles_macroatoms_file)
        return examples_nn, macroatoms, raw, preanalysed_raw


def create_parametrised_data(file, namespace):
    atoms, raw, preanalysed_raw, mp_params = get_atoms_with_primary_parametrisation(file, None,
                                                                                    namespace.montage,
                                                                                    namespace.mp_dir,
                                                                                    namespace.mp_type,
                                                                                    '', max_iter=175)

    macroatoms = split_to_macroatoms(atoms)

    kernels = get_kernels(namespace.montage)

    examples_convolve = convolve_data(macroatoms, mp_params)

    examples_nn = parametrise_atoms(macroatoms, examples_convolve, kernels, raw)
    return examples_nn, macroatoms, raw, preanalysed_raw


def parametrise_atoms(macroatoms, examples_convolve, kernels, raw):
    fn = partial(_parametrize_macroatoms_internal, kernels=kernels, raw=raw)
    examples_nn = process_map(fn, list(zip(macroatoms, examples_convolve)), desc='parametrising atoms (convolve with kernels)', chunksize=512, max_workers=4)
    return pandas.DataFrame(examples_nn)


def _parametrize_macroatoms_internal(iter, kernels, raw):
    example, datas = iter
    datas = np.squeeze(datas)
    datas[np.logical_not(np.isfinite(datas))] = 0
    example_for_visualisation = parametrise_atom(example, raw.info['ch_names'], kernels, datas)
    return example_for_visualisation


def get_atoms_with_primary_parametrisation(raw_file, tagfile, montage, mp_dir, mp_type, atom_signal_cache, max_iter=175, unique_file_names=True, resample=True, copy=False):
    raw = read_raw(raw_file, tagfile)
    preanalysed_raw = preanalyse_raw(raw, montage, resample, copy=copy)
    path_to_book, path_to_book_params = get_mp_book_paths(mp_dir, raw_file,
                                                                       montage, mp_type, unique_file_names=unique_file_names)

    mp_params = json.load(open(path_to_book_params))
    atoms = read_db_atoms(path_to_book)
    dir_to_load = os.path.dirname(atom_signal_cache)
    mp_filename = '{}_{}_{}.txt'.format(os.path.basename(raw_file),
                                        montage,
                                        mp_type)
    file_to_load = os.path.join(dir_to_load, mp_filename)
    atoms_importance_mask = np.zeros(atoms.shape[0], dtype=bool)
    if os.path.exists(file_to_load):
        mask_candidate = np.loadtxt(file_to_load).astype(np.bool)
        if mask_candidate.shape == atoms_importance_mask.shape:
            print('Using {} file to load atom importance'.format(file_to_load))
            atoms_importance_mask = mask_candidate

    labels_filename = '{}_{}_{}_atom_labels_manual.txt.npy'.format(os.path.basename(raw_file),
                                        montage,
                                        mp_type)
    file_to_load = os.path.join(dir_to_load, labels_filename)
    labels_manual = np.array(['unknown'] * atoms.shape[0], dtype=np.object)
    if os.path.exists(file_to_load):
        labels_manual_candidate = np.load(file_to_load, allow_pickle=True)
        if labels_manual_candidate.shape == atoms_importance_mask.shape:
            print('Using {} file to load atom manual labeling'.format(file_to_load))
            labels_manual = labels_manual_candidate
    atoms = get_all_atoms(atoms, preanalysed_raw, mp_params, atoms_importance_mask=atoms_importance_mask)

    dipole_path = os.path.join(os.path.dirname(path_to_book),
                             'dipole_fits',
                             os.path.basename(path_to_book) + '_dipole_fits.pickle')
    if os.path.exists(dipole_path):
        print("reading dipole fits")
        # assuming that dipolne fits are in a pickle with exactly same ordering as atoms
        with open(dipole_path, 'rb') as dipole_pickle_file:
            atoms_with_dipoles = pd.read_pickle(dipole_pickle_file)

        missing_columns = []
        for i in list(set(atoms_with_dipoles.columns) - set(atoms.columns)):
            if i.startswith('dip_'):
                missing_columns.append(i)
        missing_columns.sort()
        for missing_column in missing_columns:
            atoms[missing_column] = atoms_with_dipoles[missing_column]
    else:
        print("\n\n\n no dipole fits! \n\n\n")

    atoms = atoms.assign(label_manual_marking=labels_manual)
    atoms = atoms.sort_values(['absolute_position', 'iteration', 'ch_id'])

    # atoms = calculate_atom_neighborhood_snr(atoms)
    atoms = filter_atoms_by_iteration(atoms, max_iter=max_iter)
    atoms = calculate_atom_neighborhood_snr_signal(atoms, preanalysed_raw)
    atoms = calculate_atom_normalised_amplitude(atoms)
    # we have to drop it after calculating neighborhood

    if mp_type in ['mmp3', 'mmp1']:
        mmp3_atom_ids = []
        for i in range(int(atoms.shape[0] / len(mp_params['channel_names']))):
            mmp3_atom_id = [i] * len(mp_params['channel_names'])
            mmp3_atom_ids.extend(mmp3_atom_id)
        atoms = atoms.assign(mmp3_atom_ids=mmp3_atom_ids)
    return atoms, raw, preanalysed_raw, mp_params


def filter_atoms_by_iteration(atoms, max_iter=175):
    return atoms[atoms.iteration <= max_iter]


def calculate_atom_normalised_amplitude(atoms):
    # std_per_channels.mean()  inb14 - "normal eeg"
    # Out[2]: 23.30182361322579
    sigma_of_exemplary_eeg = 23.30182361322579
    atoms['amplitude_normalized_to_textbook'] = atoms.amplitude_z_score * sigma_of_exemplary_eeg
    return atoms


def split_to_macroatoms(atoms_df):
    amount_of_macroatoms = atoms_df.mmp3_atom_ids.values[-1] + 1
    macroatoms_l = np.empty(amount_of_macroatoms, dtype=np.object)
    macroatoms = atoms_df.groupby(by='mmp3_atom_ids')
    for macroatom_id, macroatom in enumerate(tqdm(macroatoms, desc='splitting atoms to macroatoms')):
        macroatoms_l[macroatom_id] = macroatom[1]
    return macroatoms_l
