import numpy as np
from tqdm import tqdm

from mp_dip.utils.interpolate_head_features import get_positions, interpolate_feature


def prepare_data(examples, mp_params):
    """for mmp3"""
    # [ number of examples, number of atoms, number of channels, number of atom features

    # bad, it should be "same features, vs per tag features]"
    # the amplitude should be convolved?
    # amplitude z score?
    # do we need phase?
    # phase per channel??
    non_convolved_features = ['width', 'frequency', 'absolute_position']

    non_convolved_mask = np.zeros(len(in_features)).astype(bool)
    for nr, feature in enumerate(in_features):
        non_convolved_mask[nr] = (feature in non_convolved_features)

    convolved_mask = np.zeros(len(in_features)).astype(bool)
    for nr, feature in enumerate(in_features):
        convolved_mask[nr] = feature in convolved_features

    non_convolved_examples = examples[:, :, 0, non_convolved_mask]
    convolved_examples = examples[:, :, :, convolved_mask]

    convolved_examples = interpolate_features_for_convolution(convolved_examples, mp_params)

    additional_non_convolved_examples = np.max(examples[:, :, :, convolved_mask], axis=2)
    # max amplitude of the structure per channel
    non_convolved_examples = np.concatenate([non_convolved_examples, additional_non_convolved_examples], axis=-1)
    non_convolved_examples = non_convolved_examples.reshape((len(non_convolved_examples), -1))

    return non_convolved_examples, convolved_examples


def interpolate_features_for_convolution(convolve_examples, mp_params):
    pos = get_positions(tuple(mp_params["channel_names"]))
    return interpolate_feature(convolve_examples, pos)


def convert_montage_channel_nr_to_raw(raw, preanalysed_raw, channel):
    channel_name_montage = preanalysed_raw.ch_names[channel]
    raw_channels_in_montage = channel_name_montage.split('-')
    channel_for_raw = raw_channels_in_montage[0]
    return raw.ch_names.index(channel_for_raw)


convolved_features = ['amplitude', ]
ATOM_FEATURES = ['iteration', 'modulus', 'amplitude', 'amplitude_z_score',
                'width', 'frequency', 'phase', 'absolute_position', 'ch_id']
in_features = ['amplitude', 'amplitude_z_score', 'width', 'frequency', 'phase', 'absolute_position']
convolved_features_vmax = [100 / 100, 2.5]  # amplitudes are normalised by 100 in the "examples_norm"
convolved_features_vmin = [0, -2.5]


def convolve_data(macroatoms, mp_params):
    """for mmp3"""
    # [ number of examples, number of atoms, number of channels, number of atom features

    # bad, it should be "same features, vs per tag features]"
    # the amplitude should be convolved?
    # amplitude z score?
    # do we need phase?
    # phase per channel??
    channel_count = len(mp_params['channel_names'])
    # (N_examples, N_atoms, channels, features)
    features_to_convolve = np.empty((len(macroatoms), 1, channel_count, len(convolved_features)))
    for macroatom_nr, macroatom in enumerate(tqdm(macroatoms, desc='interpolating macroatoms')):
        features_to_convolve[macroatom_nr][0] = macroatom[convolved_features].values

    convolved_examples = interpolate_features_for_convolution(features_to_convolve, mp_params)
    return convolved_examples
