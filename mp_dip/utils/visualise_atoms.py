import pandas as pd
import pylab as pb
import numpy as np
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.metrics import roc_curve, roc_auc_score

from mp_dip.utils.interpolate_head_features import get_positions, interpolate_feature
from mp_dip.utils.data_preprocessing_utils import convolved_features
from mp_dip.utils.signal_reconstruction import gabor


def stupid_auc_calc(examples, classes):
    classes_binary = (np.array(classes) == 'None').astype(int)
    examples_lda = examples.reshape((examples.shape[0], -1))

    lda = LinearDiscriminantAnalysis(solver='lsqr', shrinkage='auto')
    lda.fit(examples_lda, classes_binary)
    distances = lda.decision_function(examples_lda)
    auc = roc_auc_score(classes_binary, distances)
    return auc


def visualise_examples(examples, classes):
    classes_binary = (np.array(classes) == 'None').astype(int)
    examples_lda = examples.reshape((examples.shape[0], -1))

    lda = LinearDiscriminantAnalysis(solver='lsqr', shrinkage='auto')
    lda.fit(examples_lda, classes_binary)
    distances = lda.decision_function(examples_lda)
    predictions = lda.predict(examples_lda)

    pb.hist([distances[classes_binary==1], distances[classes_binary==0]], normed=True, label=['nontagged', 'tagged'], bins=100)
    pb.legend()
    pb.figure()
    fpr, tpr, tresholds = roc_curve(classes_binary, distances)
    auc = roc_auc_score(classes_binary, distances)
    return auc
    pb.plot(fpr, tpr)
    pb.title('AUC {}'.format(auc))
    pb.show()
    return predictions

def visualise_atoms(atom_classes):
    # fig, axs = pb.subplots(2, 2)
    import IPython
    IPython.embed()

    features = ['width', 'amplitude', 'amplitude_z_score', 'frequency']
    atoms = pd.concat(atom_classes.values()).drop(labels=['iteration', 'struct_len', 'offset', 'absolute_position'],
                                                     axis=1)

    atoms_features = atoms.drop(labels=['structure_name'], axis=1).values
    atoms_features = atoms.drop(labels=['structure_name', 'amplitude', 'modulus', 'ch_id', ], axis=1).values
    atoms_labels = (atoms.structure_name == '').astype(np.float32)
    # atoms.groupby('structure_name').hist(alpha=0.4)

    lda = LinearDiscriminantAnalysis(solver='lsqr', shrinkage='auto')
    lda.fit(atoms_features, atoms_labels)
    distances = lda.decision_function(atoms_features)

    pb.hist([distances[atoms_labels==1], distances[atoms_labels==0]], normed=True, label=['nontagged', 'tagged'], bins=50)
    pb.legend()
    pb.show()
    for feature in features:
        for nr, key in enumerate(atom_classes.keys()):
            axs[nr].set_title(key)


def reconstruct(example_atoms, timeline):
    reconsctruction = np.zeros_like(timeline)

    for atom in example_atoms.itertuples():
        # atoms are loaded as p2p amplitudes
        atom_s = atom.amplitude / 2 * gabor(timeline,
                                        atom.width,
                                        atom.absolute_position,
                                        atom.frequency,
                                        atom.phase,
                                        ) * atom.atom_importance
        reconsctruction += atom_s
    return reconsctruction


def interpolate_feature_for_visualisation(atom_df, channels, interp_item='amplitude'):
    convolved_mask = np.zeros(len(atom_df.columns)).astype(bool)
    for nr, feature in enumerate(atom_df.columns):
        convolved_mask[nr] = feature in convolved_features

    convolve_examples = atom_df.values.T[convolved_mask].astype(np.float32).T[None, None, :]  # amp normalisation as expected
    pos = get_positions(tuple(channels))
    try:
        return interpolate_feature(convolve_examples, pos)[0, 0, :, :, convolved_features.index(interp_item)]
    except:
        import traceback
        traceback.print_exc()
        import IPython
        IPython.embed()


def reconstruct_multichannel_atom(example_atoms, timeline):
    reconsctruction = np.zeros((timeline.shape[0], example_atoms.amplitude.shape[0]))

    for nr, atom in enumerate(example_atoms.itertuples()):
        # atom amplitudes are loaded as p2p
        atom_s = atom.amplitude / 2.0 * gabor(timeline,
                                        atom.width,
                                        atom.absolute_position,
                                        atom.frequency,
                                        atom.phase,
                                        )
        reconsctruction[:, nr] = atom_s
    return reconsctruction
