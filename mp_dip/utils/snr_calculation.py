from functools import partial
from multiprocessing import cpu_count
import numpy as np
from tqdm import tqdm
from tqdm.contrib.concurrent import process_map


def _new_snr_calculation(i, sfreq, atoms_starts, atoms_stops, atoms_energy, channel_data):
    start = int(atoms_starts[i] * sfreq)
    if start < 0:
        start = 0
    stop = int(atoms_stops[i] * sfreq)
    if stop <= start:
        stop = start + 4
    neighbourhood_signal = channel_data[start:stop]
    neighbourhood_energy = np.sum((neighbourhood_signal - np.mean(neighbourhood_signal)) ** 2)  # depends on sfreq??
    atom_energy = atoms_energy[i]
    return 10 * np.log(atom_energy / neighbourhood_energy)


def calculate_atom_neighborhood_snr_signal(atoms, preanalyzed_raw):
    channel_counts = atoms.ch_id.max()
    all_snrs = np.zeros(atoms.shape[0])

    for channel in tqdm(range(1, channel_counts + 1), desc='calculating SNR per channel', total=channel_counts):
        fitting_channel_mask = atoms.ch_id == channel
        fitting_atoms = atoms.loc[fitting_channel_mask]
        atoms_starts = (fitting_atoms.absolute_position - fitting_atoms.width / 2).values
        atoms_stops = (fitting_atoms.absolute_position + fitting_atoms.width / 2).values
        atoms_energy = fitting_atoms.modulus.values ** 2

        channel_data = np.squeeze(preanalyzed_raw.get_data(fitting_atoms.channel_name.values[0], units='uV'))
        to_map_range = list(range(len(atoms_starts)))
        fn = partial(_new_snr_calculation, sfreq=preanalyzed_raw.info['sfreq'],
                     atoms_starts=atoms_starts, atoms_stops=atoms_stops, atoms_energy=atoms_energy,
                     channel_data=channel_data)
        # snrs = np.array(list(map(fn, to_map_range))) # debug singlethread
        snrs = np.array(list(process_map(fn, to_map_range,
                                         desc="calculating snr for channel {}/{}".format(channel, channel_counts),
                                         chunksize=10000, max_workers=cpu_count())))
        all_snrs[fitting_channel_mask] = snrs
    atoms_with_snr = atoms.assign(snr=all_snrs)
    return atoms_with_snr


# def calculate_atom_neighborhood_snr_signal(atoms, preanalyzed_raw):
#     channel_counts = atoms.ch_id.max()
#     all_snrs = np.zeros(atoms.shape[0])
#     for channel in tqdm(range(1, channel_counts + 1), desc='calculating SNR per channel', total=channel_counts):
#         fitting_channel_mask = atoms.ch_id == channel
#         fitting_atoms = atoms.loc[fitting_channel_mask]
#         atoms_starts = (fitting_atoms.absolute_position - fitting_atoms.width / 2).values
#         atoms_stops = (fitting_atoms.absolute_position + fitting_atoms.width / 2).values
#         atoms_energy = fitting_atoms.modulus.values ** 2
#         snrs = np.zeros(len(atoms_starts))
#         channel_data = np.squeeze(preanalyzed_raw.get_data(fitting_atoms.channel_name.values[0], units='uV'))
#         for i in tqdm(range(len(atoms_starts))):
#             start = int(atoms_starts[i] * preanalyzed_raw.info['sfreq'])
#             if start < 0:
#                 start = 0
#             stop = int(atoms_stops[i] * preanalyzed_raw.info['sfreq'])
#             if stop <= start:
#                 stop = start + 4
#             neighbourhood_signal = channel_data[start:stop]
#             neighbourhood_energy = np.sum((neighbourhood_signal-np.mean(neighbourhood_signal))**2)
#             atom_energy = atoms_energy[i]
#             snrs[i] = 10 * np.log(atom_energy / neighbourhood_energy)
#         all_snrs[fitting_channel_mask] = snrs
#     atoms_with_snr = atoms.assign(snr=all_snrs)
#     return atoms_with_snr


def calculate_atom_neighborhood_snr(atoms):
    """calculates SNR for every atom, looking at it's neighborhood per channel.
    ATOMS MUST BE SORTED BY ABSOLUTE TIME!!!!
    """

    """take into account atoms which have neighbors inside atom width x2
    TODO: additionally grab other atoms which are big, so big, that they are outside of this atom?
    """
    channel_counts = atoms.ch_id.max()
    all_snrs = np.zeros(atoms.shape[0])
    for channel in tqdm(range(1, channel_counts + 1), desc='calculating SNR per channel', total=channel_counts):
        fitting_channel_mask = atoms.ch_id == channel
        fitting_atoms = atoms.loc[fitting_channel_mask]
        atoms_starts = (fitting_atoms.absolute_position - fitting_atoms.width).values
        atoms_stops = (fitting_atoms.absolute_position + fitting_atoms.width).values
        atoms_centers = fitting_atoms.absolute_position.values
        atoms_energy = fitting_atoms.modulus.values ** 2

        snrs = np.zeros(len(atoms_starts))
        for i in range(len(atoms_starts)):
            ind_start, ind_stop = np.searchsorted(atoms_centers, [atoms_starts[i], atoms_stops[i]])
            neighbourhood_energy = np.sum(atoms_energy[ind_start:ind_stop])
            own_energy = atoms_energy[i]
            snrs[i] = 10 * np.log(own_energy/neighbourhood_energy)
        all_snrs[fitting_channel_mask] = snrs
    atoms_with_snr = atoms.assign(snr=all_snrs)
    return atoms_with_snr