from functools import partial

import numpy as np
import pandas
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.utils import compute_class_weight
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow_addons.losses import SigmoidFocalCrossEntropy
from tqdm import tqdm
from tqdm.contrib.concurrent import process_map

from mp_dip.utils.sanity_checking import atom_sanity_check
from mp_dip.utils.utils import print_cm


def drop_multichannel_features(examples_df):
    # convolutional_filter  - spatial patterns for atoms
    to_remove = [i for i in examples_df.columns if i.startswith('cf_')]
    to_remove += ['center_of_mass_x', 'center_of_mass_y']
    examples_df.drop(to_remove, axis=1, inplace=True, errors='ignore')
    return examples_df


# TOKEN_WINDOWS = [[-90, -15], [-15, -5], [-5, -1.5], [-1.5, 0], [0, 1.5], [1.5, 5], [5, 15], [15, 90]]
TOKEN_WINDOWS = [[-60, -15], [-15, -5], [-5, -1.5], [-1.5, 0], [0, 1.5], [1.5, 5], [5, 15], [15, 60]]
# TOKEN_WINDOWS = [[-15, -5], [-5, -1.5], [-1.5, 0], [0, 1.5], [1.5, 5], [5, 15]]
# TOKEN_WINDOWS = [[-5, -1.5], [-1.5, 0], [0, 1.5], [1.5, 5]]


# needs to be 2 different functions, otherwise WAY TOO SLOW
def second_level_parametrisation_train(full_atom_lists, model, original_atoms, examples_df):
    """original atoms - in order of examplles, examples_df - dataframe used to train model (to extract the same columns)

    """

    feature_columns = examples_df.columns
    second_level_examples = []

    preds_l = []
    for full_atom_list in full_atom_lists:
        preds_full = model.predict(full_atom_list[examples_df.columns].values)
        preds_l.append(preds_full)
    examples_preds = model.predict(examples_df[feature_columns].values.astype(np.float64))

    for example in tqdm(original_atoms.iterrows(), total=len(original_atoms), desc='second level parametrisation'):
        example_nr = example[0]
        id = example[1].file_id
        atoms = full_atom_lists[id]
        position = example[1].absolute_position

        this_token = examples_preds[example_nr]
        masked_token_l = []

        for token_winow in TOKEN_WINDOWS:
            start_ind, end_ind = np.searchsorted(atoms.absolute_position, [position + token_winow[0], position + token_winow[1]])
            if end_ind > start_ind:
                probs_masked = preds_l[id][start_ind:end_ind]
                max_ids = np.argmax(probs_masked, axis=0)
                masked_token = np.array([probs_masked[j, i] for i, j in enumerate(max_ids)])
            else:
                masked_token = np.zeros(preds_l[id].shape[1])
            masked_token_l.append(masked_token)

        new_example = np.concatenate(masked_token_l + [this_token])
        second_level_examples.append(new_example)
    return np.array(second_level_examples)


def _second_level_parametrisation_predict_internal(example, examples_df, preds_full):
    example_nr = example[0]
    atoms = examples_df
    position = example[1].absolute_position

    this_token = preds_full[example_nr]
    masked_token_l = []
    for token_winow in TOKEN_WINDOWS:
        start_ind, end_ind = np.searchsorted(atoms.absolute_position,
                                             [position + token_winow[0], position + token_winow[1]])
        if end_ind > start_ind:
            probs_masked = preds_full[start_ind:end_ind]
            max_ids = np.argmax(probs_masked, axis=0)
            masked_token = np.array([probs_masked[j, i] for i, j in enumerate(max_ids)])
        else:
            masked_token = np.zeros(preds_full.shape[1])
        masked_token_l.append(masked_token)

    new_example = np.concatenate(masked_token_l + [this_token])
    return new_example

def second_level_parametrisation_predict_optimized(examples_df, model, feature_column_names, preds_full=None):
    """examples_df - parametrised macroatoms (one row per macroatom)"""


    if preds_full is None:
        preds_full = model.predict(examples_df[feature_column_names].values)

    fn = partial(_second_level_parametrisation_predict_internal, examples_df=examples_df, preds_full=preds_full)
    second_level_examples = process_map(fn, list(examples_df.iterrows()), chunksize=int(len(examples_df) / 32), desc='second level parametrisation')
    return np.array(second_level_examples)


def train_on_atoms(examples_df, one_hot_labels, labels, original_atoms, full_atom_lists):
    # import IPython
    # IPython.embed()

    num_classes = len(np.unique(one_hot_labels.columns))
    classes_nn = one_hot_labels.to_numpy().astype(np.float64)

    # label smoothing
    label_smoothing = 0.1
    classes_nn = classes_nn * (1 - label_smoothing) + label_smoothing / num_classes
    examples = examples_df.to_numpy()

    # phase variability can be NaN sometimes
    examples[np.isnan(examples)] = 0

    print("\n\n\n\n\nDTYPE OF EXAMPLES: {}".format(examples.dtype))
    print("\nexamples.shape: {}\n".format(examples.shape))
    print("\nAmount of examples: {}\n\n\n\n\n".format(np.unique(labels, return_counts=True)))

    class_biases = one_hot_labels.sum().values.astype(np.float32)
    class_biases = class_biases / np.sum(class_biases)

    def class_biases_init(shape, dtype=None):
        return class_biases

    normalisation_layer = layers.experimental.preprocessing.Normalization()

    # this gets me quite high accuracy!
    inputs = keras.Input(examples.shape[1:])
    x = normalisation_layer(inputs)
    x = layers.Dense(20, activation="relu")(x)
    x = layers.Dropout(0.25)(x)
    x = layers.Dense(10, activation="relu")(x)
    x = layers.Dropout(0.25)(x)

    # x = layers.Dense(75, activation="relu")(x)
    # x = layers.Dropout(0.25)(x)
    # x = layers.Dense(50, activation="relu")(x)
    # x = layers.Dropout(0.25)(x)
    # x = layers.Dense(100, activation="relu")(x)
    # x = layers.Dropout(0.5)(x)
    # x = layers.Dense(50, activation="relu")(x)
    # x = layers.Dropout(0.5)(x)
    # x = layers.Dense(50, activation="relu")(x)
    # x = layers.Dropout(0.5)(x)
    # outputs = layers.Dense(num_classes, activation="softmax", bias_initializer=class_biases_init)(x)
    outputs = layers.Dense(num_classes, activation="softmax")(x)
    model = keras.Model(inputs=inputs, outputs=outputs)

    model.compile(optimizer=keras.optimizers.RMSprop(learning_rate=4e-3),
                  loss=keras.losses.CategoricalCrossentropy(),
                  metrics=['categorical_accuracy'],
                  weighted_metrics=['categorical_accuracy'])

    inputs_second_layer = keras.Input((num_classes * (len(TOKEN_WINDOWS) + 1)))
    x = layers.Dense(20, activation="relu")(inputs_second_layer)
    x = layers.Dropout(0.25)(x)
    x = layers.Dense(20, activation="relu")(x)
    x = layers.Dropout(0.25)(x)
    # x = layers.Dense(20, activation="relu")(x)
    # x = layers.Dropout(0.25)(x)
    outputs_second_layer = layers.Dense(num_classes, activation="softmax")(x)

    model_second_layer = keras.Model(inputs=inputs_second_layer, outputs=outputs_second_layer)
    model_second_layer.compile(optimizer=keras.optimizers.RMSprop(learning_rate=1e-3),
                               loss=SigmoidFocalCrossEntropy(),
                  # loss=keras.losses.CategoricalCrossentropy(),
                  metrics=['categorical_accuracy'],
                  weighted_metrics=['categorical_accuracy'])


    # class_weights = compute_class_weight('balanced', sorted(np.unique(labels)), labels)
    #
    unique_labels, counts = np.unique(labels, return_counts=True)
    # class_weights = 1 / counts
    class_weights = compute_class_weight('balanced', sorted(np.unique(labels)), labels)
    # class_weights = class_weights * 1 / np.min(class_weights)
    class_weights_d = dict(zip(unique_labels, class_weights))

    # WEIGHTING PROBLEMATIC CLASSES
    class_weights_d['Kompleks K'] = class_weights_d['Kompleks K'] * 10
    class_weights_d['Wierzchołkowa fala ostra'] = class_weights_d['Wierzchołkowa fala ostra'] * 12
    #
    sample_weights = labels.copy()

    for key in class_weights_d:
        sample_weights[sample_weights == key] = class_weights_d[key]
    sample_weights = sample_weights.astype(np.float64)

    # full training?
    splitter = StratifiedShuffleSplit(n_splits=1, test_size=0.01)
    # splitter = StratifiedShuffleSplit(n_splits=1, test_size=0.25)
    # splitter = StratifiedShuffleSplit(n_splits=1, test_size=0.25, random_state=150)

    train_ind, test_ind = next(splitter.split(examples, labels))

    # balancing None for CM
    labels_unique = np.unique(labels)
    counts_test = []
    for label in labels_unique:
        print(np.sum(labels[test_ind]==label))
        counts_test.append(np.sum(labels[test_ind]==label))
    min_labels = np.min(counts_test)
    biggest_label = labels_unique[np.argmax(counts_test)]
    new_test_ind = []
    for label in labels_unique:
        if label != biggest_label:
            new_test_ind.extend(test_ind[labels[test_ind]==label])
        else:
            to_extend = test_ind[labels[test_ind] == label][:min_labels]
            new_test_ind.extend(to_extend)
    # uncomment for balanced CM
    # test_ind = np.array(new_test_ind)

    train = True
    while train:
        normalisation_layer.adapt(examples[train_ind])
        history = model.fit(
                            # examples, classes_nn,
                            examples[train_ind], classes_nn[train_ind],
                            batch_size=128, epochs=500, use_multiprocessing=True, workers=8,
                            validation_data=(examples[test_ind], classes_nn[test_ind]),
                            # )
                            sample_weight=sample_weights[train_ind])
                            # sample_weight=sample_weights)

        # get 2nd level parametrisation
        # based on tokens
        # train 2nd level nn

        second_level_examples = second_level_parametrisation_train(full_atom_lists, model, original_atoms, examples_df)

        # phase variability might be none
        second_level_examples[np.isnan(second_level_examples)] = 0

        history_second_layer = model_second_layer.fit(
                            second_level_examples[train_ind], classes_nn[train_ind],
                            batch_size=128, epochs=500, use_multiprocessing=True, workers=8,
                            validation_data=(second_level_examples[test_ind], classes_nn[test_ind]),
                            # )
                            sample_weight=sample_weights[train_ind])


        # train = ('y' in input("Train more?"))
        train = False
        preds_first_level = model.predict(examples[test_ind])
        preds = model_second_layer.predict(second_level_examples[test_ind])
        labels_pred_raw = np.argmax(preds, axis=-1)
        preds_l = []
        for full_atom_list in full_atom_lists:
            preds_full = model.predict(full_atom_list[examples_df.columns].values)
            preds_l.append(preds_full)

        for sureness_threshold in [0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.8,]:
        # for sureness_threshold in [0.6, 0.7, 0.8,]:
        # for sureness_threshold in [0.8,]:

            print("\n\n\n\n\nSanity check sureness ", sureness_threshold)





            labels_pred = []
            # test with 1 or 2 levels (context aware nn)
            for atom_item, pred in tqdm(zip(original_atoms.iloc[test_ind].iterrows(), preds_first_level), total=len(preds), desc='sanity checking'):
            # for atom_item, pred in tqdm(zip(original_atoms.iloc[test_ind].iterrows(), preds), total=len(preds), desc='sanity checking'):
                atom = atom_item[1]
                all_macroatoms = full_atom_lists[atom.file_id]
                all_macroatoms_preds = preds_l[atom.file_id]
                pseudomacroatom = pandas.DataFrame([atom])
                # TODO przekazywać listę atomów wokół tego i poprawiać prawdopodobieństwa Vertex Sharp Vawe/K-complex jeśli jest blizko Spindle
                class_, sureness, new_pred = atom_sanity_check(pseudomacroatom, pred, debug=False,
                                                     sureness_threshold=sureness_threshold, only_use_sureness=False, all_macroatoms=all_macroatoms, all_macroatoms_preds=all_macroatoms_preds)
                labels_pred.append(np.argmax(labels_unique == class_))

            labels_true = np.argmax(classes_nn[test_ind], axis=-1)
            print()
            print('Neural net:')
            print("Cm with sanity check")
            cm = confusion_matrix(labels_true, labels_pred)
            print_cm(cm, [i[6:] for i in one_hot_labels.columns])
            print("NN accuracy sanity check: ", accuracy_score(labels_true, labels_pred))
            for nr, (row, label) in enumerate(zip(cm, one_hot_labels.columns)):
                accuracy = row[nr] / np.sum(row)
                print("NN accuracy sanity check ", label, accuracy)

            print("Cm with no sanity check")
            cm = confusion_matrix(labels_true, labels_pred_raw)
            print_cm(cm, [i[6:] for i in one_hot_labels.columns])
            print("NN accuracy no sanity check: ", accuracy_score(labels_true, labels_pred_raw))
            for nr, (row, label) in enumerate(zip(cm, one_hot_labels.columns)):
                accuracy = row[nr] / np.sum(row)
                print("NN accuracy no sanity check ", label, accuracy)

    return model, model_second_layer

