import numpy as np


def gabor(t, s, t0, f0, phase=0.0, segment_length_s=20.0):
    """
    Generates values for Gabor atom with unit amplitude.
    t0 - global, t - also global,
    """
    t_segment = t % segment_length_s
    t0_segment = t0 % segment_length_s
    result = np.exp(-np.pi*((t_segment-t0_segment)/s)**2) * np.cos(2*np.pi*f0*(t_segment-t0_segment) + phase)
    return result


def gabor_envelope(t, s, t0, f0, phase=0.0, segment_length_s=20.0):
    """
    Generates values for Gabor atom with unit amplitude.
    t0 - global, t - also global,
    """
    t_segment = t % segment_length_s
    t0_segment = t0 % segment_length_s
    return np.exp(-np.pi*((t_segment-t0_segment)/s)**2)
