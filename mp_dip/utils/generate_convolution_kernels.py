# all with inverse!
# quartiles ✔
# half of head (front/ back / left right) ✔
# poca dots - multiple rotations/spacings?
# singular dot (whole head)
# multiple dots (less than whole head)
# center of mass (position, x y)
# rings (different sized?) ✔
# gradients (different directions)
# make a kernel per electrode - nah
# make a kernel per eletrode line
# kernel with lines
# dipoles?

# check phases?
# phase variability inside 50% of power?
from pathlib import Path

import numpy as np
from PIL import Image
import os

from mp_dip.utils.visualise_atoms import interpolate_feature_for_visualisation
from mp_dip.utils.data_preprocessing_utils import ATOM_FEATURES

channels = 'Fp1,Fp2,F3,F4,C3,C4,P3,P4,O1,O2,F7,F8,T3,T4,T5,T6,Fz,Cz,Pz'.split(',')


def gen_per_channel():
    atom_template = np.zeros([1, len(channels), len(ATOM_FEATURES)])
    for channel_nr, channel in enumerate(channels):
        atom = atom_template.copy()
        atom[0, channel_nr, ATOM_FEATURES.index('amplitude')] = 255
        datas = interpolate_feature_for_visualisation(atom,
                                                      channels,
                                                      )
        img = Image.fromarray(np.flip(datas, axis=0))
        img.convert('RGB').save(Path(os.path.join(os.path.dirname(__file__), 'kernels', 'temp', '{}.png'.format(channel))))


def gen_lines():
    lines = [[['Fp1', 'Fp2'], 'horFp'],
             ['F7,F3,Fz,F4,F8'.split(','), 'horFx'],
             ['T3,F7,Fp1,Fp2,F8,T4'.split(','), 'horFront'],
             ['T3,T5,O1,O2,T6,T4'.split(','), 'horBack'],
             ['T3,C3,Cz,C4,T4'.split(','), 'horCx'],
             ['T5,P3,Pz,P4,T6'.split(','), 'horPx'],
             ['O1,O2'.split(','), 'horOx'],
             ['F7,T3,T5'.split(','), 'verL1'],
             ['Fp1,F7,T3,T5,O1'.split(','), 'verL2'],
             ['Fp1,F3,C3,P3,O1'.split(','), 'verX3'],
             ['Fz,Cz,Pz'.split(','), 'verXz'],
             ['Fp2,F4,C4,P4,O2'.split(','), 'verX4'],
             ['Fp2,F8,T4,T6,O2'.split(','), 'verR2'],
             ['F8,T4,T6'.split(','), 'verR1'],
             ['Fp1,Fp2,F8,T4,T6,O1,O2,T5,T3,F7'.split(','), 'circlebig'],
             ['F3,Fz,F4,C4,P4,Pz,P3,C3'.split(','), 'circlesmall'],
             ['Cz'.split(','), 'center'],
             ['Fp1,F7,F3,T3,C3,T5,P3,O1'.split(','), 'left'],
             ['Fp2,F8,F4,T4,C4,T6,P4,O2'.split(','), 'right'],
             ['Fp1,Fp2,F7,F3,Fz,F4,F8'.split(','), 'front'],
             ['O1,O2,T5,P3,Pz,P4,T6'.split(','), 'back'],
             ['Fp1,F7,F3,T3,C3'.split(','), 'quartFL'],
             ['Fp2,F8,F4,T4,C4'.split(','), 'quartFR'],
             ['O1,T5,P3,T3,C3'.split(','), 'quartBL'],
             ['O2,T6,P4,T4,C4'.split(','), 'quartBR'],
             ]
    atom_template = np.zeros([1, len(channels), len(ATOM_FEATURES)])
    for line_channels, name in lines:
        atom = atom_template.copy()
        for channel in line_channels:
            atom[0, channels.index(channel), ATOM_FEATURES.index('amplitude')] = 255
        datas = interpolate_feature_for_visualisation(atom,
                                                      channels,
                                                      )
        img = Image.fromarray(np.flip(datas, axis=0))
        img.convert('RGB').save(Path(os.path.join(os.path.dirname(__file__), 'kernels', '{}.png'.format(name))))


if __name__ == '__main__':
    gen_per_channel()
    gen_lines()

