from mp_dip.utils.utils import classes_d
import numpy as np


classes_d_in = {j: i for i, j in classes_d.items()}


def sanity_check_swa(atom, class_probabilities):
    # Slow waves 0.2–2 0.5–6
    amplitude = (40 <= atom['amplitude'] <= 1e30)  # minimum amplitude? 30 might be a bit too much but leave it for now   # dla plików z "dorosłym" amplituda - za mała są spindle 15 mV, mminimum 12.5 peak to peak uznaje się... (klasycznie 25)
    freq = (0.2 <= atom['frequency'] <= 2.0)  # 11-16 Hz z podręcznika???
    width = (0.5 <= atom['width'] <= 6)  # half gauss
    amplitude_z_score = atom['amplitude_z_score'] > 7

    # minimum_amount_of_cycles = 2.0
    # try:
    #     cycles = atom['width'] / (1.0 / atom['frequency'])
    # except ZeroDivisionError:
    #     cycles = 0.5
    # is_minimum_cycles = (cycles >= minimum_amount_of_cycles)
    # good_dipole = atom['dip_gof'] > 90
    # dipole_in_cortex = -2.5 < atom['dip_distance_to_cortex_voxel'] < 5 # in milimeters
    # return freq and amplitude and amplitude_z_score and width and is_minimum_cycles and good_dipole and dipole_in_cortex
    return freq and amplitude and width# and amplitude_z_score # todo check if it's actually good?


def sanity_check_alpha(atom, class_probabilities):
    amplitude = (0 <= atom['amplitude'] <= 1e30)  # minimum amplitude? 30 might be a bit too much but leave it for now   # dla plików z "dorosłym" amplituda - za mała są spindle 15 mV, mminimum 12.5 peak to peak uznaje się... (klasycznie 25)
    freq = (8.0 <= atom['frequency'] <= 13.0)  # 11-16 Hz z podręcznika???
    width = (0.2 <= atom['width'] <= 100000)  # half gauss

    minimum_amount_of_cycles = 2.0
    try:
        cycles = atom['width'] / (1.0 / atom['frequency'])
    except ZeroDivisionError:
        cycles = 0.5
    is_minimum_cycles = (cycles >= minimum_amount_of_cycles)
    # good_dipole = atom['dip_gof'] > 90
    # dipole_in_cortex = -2.5 < atom['dip_distance_to_cortex_voxel'] < 5 # in milimeters
    # return freq and amplitude and amplitude_z_score and width and is_minimum_cycles and good_dipole and dipole_in_cortex
    return freq and amplitude and width and is_minimum_cycles


# ATOM AMPLITUDES ARE P2P!!!!
def sanity_check_spikes_slow_politech004(atom, class_probabilities):
    amplitude = (40 <= atom['amplitude_normalized_to_textbook'] <= 300)
    freq = (2 <= atom['frequency'] <= 4)
    width = (1 <= atom['width'] <= 10)
    return freq and amplitude and width


def sanity_check_none(atom, class_probabilities):
    return True
    # amplitudes
    # amplitude z scores
    # freq
    # width


def sanity_check_k_complex(atom, class_probabilities):
    amplitude = (100 <= atom['amplitude'] <= 750)
    amplitude_z_score = (0.3 <= atom['amplitude_z_score'] <= 20.1)
    freq = (0 <= atom['frequency'] <= 3)
    width = (0 <= atom['width'] <= 6)
    return freq and amplitude and amplitude_z_score and width
    # amplitudes 100 - 750
    # amplitude z scores 0.3 - 20.1
    # freq 0 - 3
    # width 0 - 6 s


def sanity_check_spindle_compound_candidate(atom, class_probabilities):
    amplitude = (12.5 <= atom['amplitude'] <= 150)  # minimum amplitude? 30 might be a bit too much but leave it for now   # dla plików z "dorosłym" amplituda - za mała są spindle 15 mV, mminimum 12.5 peak to peak uznaje się... (klasycznie 25)
    # amplitude = (0 <= atom['amplitude'] <= 150)  # minimum amplitude? 30 might be a bit too much but leave it for now   # dla plików z "dorosłym" amplituda - za mała są spindle 15 mV, mminimum 12.5 peak to peak uznaje się... (klasycznie 25)
    amplitude_z_score = (0 <= atom['amplitude_z_score'] <= 5.2)
    freq = (11 <= atom['frequency'] <= 16)  # 11-16 Hz z podręcznika???
    # freq = (9 <= atom['frequency'] <= 16)  # 9-16 Hz z praca z nature
    # https://www.nature.com/articles/ncomms15930?origin=ppub
    # Frequency-dependent spindle analyses
    #
    # Up to this point, we have only considered spindles from the canonical wavelet analyses that targeted activity
    # broadly centred around 13.5 Hz. There is neither consensus nor objective data on what constitutes the true
    # range of frequencies for spindles; however, human studies have used different criteria, with lower bounds as
    # low as 9 Hz37,38,39,40,41,42,43,44,45,46,47,48,49,50,51 and upper bounds as high as 18 Hz52; animal studies
    # report spindle frequencies as low as 6 or 7 Hz53,54. Furthermore, multiple studies have argued for two types
    # of sleep spindles, with qualitatively distinct topographical and functional associations: fast spindles
    # (above ∼13 Hz) occurring primarily over centroparietal derivations, and slow spindles (below ∼13 Hz)
    # occurring more often at frontal derivations10,55,56,57. Functional magnetic resonance imaging also suggests
    # that fast and slow spindles have different cortical sources58.


    good_dipole = atom['dip_gof'] > 75
    dipole_in_cortex = -2.5 < atom['dip_distance_to_cortex_voxel'] < 5 # in milimeters
    # if not(freq and amplitude and amplitude_z_score and width and is_minimum_cycles):
    #     import IPython
    #     IPython.embed()
    #     amplitude, amplitude_z_score, freq, width, cycles, is_minimum_cycles
    # todo add snr?? (-30??)
    snr = atom['snr'] >= -30
    return freq and amplitude and amplitude_z_score and snr# and good_dipole and dipole_in_cortex


def sanity_check_spindle(atom, class_probabilities):
    amplitude = (12.5 <= atom['amplitude'] <= 150)  # minimum amplitude? 30 might be a bit too much but leave it for now   # dla plików z "dorosłym" amplituda - za mała są spindle 15 mV, mminimum 12.5 peak to peak uznaje się... (klasycznie 25)
    # amplitude = (0 <= atom['amplitude'] <= 150)  # minimum amplitude? 30 might be a bit too much but leave it for now   # dla plików z "dorosłym" amplituda - za mała są spindle 15 mV, mminimum 12.5 peak to peak uznaje się... (klasycznie 25)
    amplitude_z_score = (0 <= atom['amplitude_z_score'] <= 5.2)
    freq = (11 <= atom['frequency'] <= 16)  # 11-16 Hz z podręcznika???
    # freq = (9 <= atom['frequency'] <= 16)  # 9-16 Hz z praca z nature
    # https://www.nature.com/articles/ncomms15930?origin=ppub
    # Frequency-dependent spindle analyses
    #
    # Up to this point, we have only considered spindles from the canonical wavelet analyses that targeted activity
    # broadly centred around 13.5 Hz. There is neither consensus nor objective data on what constitutes the true
    # range of frequencies for spindles; however, human studies have used different criteria, with lower bounds as
    # low as 9 Hz37,38,39,40,41,42,43,44,45,46,47,48,49,50,51 and upper bounds as high as 18 Hz52; animal studies
    # report spindle frequencies as low as 6 or 7 Hz53,54. Furthermore, multiple studies have argued for two types
    # of sleep spindles, with qualitatively distinct topographical and functional associations: fast spindles
    # (above ∼13 Hz) occurring primarily over centroparietal derivations, and slow spindles (below ∼13 Hz)
    # occurring more often at frontal derivations10,55,56,57. Functional magnetic resonance imaging also suggests
    # that fast and slow spindles have different cortical sources58.

    width = (0.5 <= atom['width'] <= 3.5) # half gauss

    minimum_amount_of_cycles = 3.0
    try:
        cycles = atom['width'] / (1.0 / atom['frequency'])
    except ZeroDivisionError:
        cycles = 0.5
    is_minimum_cycles = (cycles >= minimum_amount_of_cycles)
    good_dipole = atom['dip_gof'] > 60
    dipole_in_cortex = -20 < atom['dip_distance_to_cortex_voxel'] < 20 # in milimeters
    # if not(freq and amplitude and amplitude_z_score and width and is_minimum_cycles):
    #     import IPython
    #     IPython.embed()
    #     amplitude, amplitude_z_score, freq, width, cycles, is_minimum_cycles
    # todo add snr?? (-30??)
    snr = atom['snr'] >= -30
    return freq and amplitude and amplitude_z_score and width and is_minimum_cycles and good_dipole and dipole_in_cortex # and snr
    # amplitudes 0 - 150
    # amplitude z scores 0 - 5.2
    # freq  9 -15.5
    # width 0-3.5


def sanity_check_vertex_sharp(atom, class_probabilities):
    amplitude = (60 <= atom['amplitude'] <= 600)
    amplitude_z_score = (0 <= atom['amplitude_z_score'] <= 12.5)
    freq = (0 <= atom['frequency'] <= 5.5)
    width = (0 <= atom['width'] <= 7.5)
    return freq and amplitude and amplitude_z_score and width
    # amplitudes 60 - 600
    # amplitude z scores 0-12.5
    # freq 0-5.5 Hz
    # width 0-7.5


classes_sanity_heuristics = {'Kompleks K': sanity_check_k_complex,
                             'None': sanity_check_none,
                             'Spindle (wrzeciono)': sanity_check_spindle,
                             'Wierzchołkowa fala ostra': sanity_check_vertex_sharp}


def probability_change(class_probabilities, class_name, amount):
    class_probabilities = class_probabilities.copy()
    class_to_change = classes_d[class_name]
    class_probabilities[class_to_change] += amount
    if np.min(class_probabilities) < 0:
        class_probabilities -= np.min(class_probabilities)
    class_probabilities = class_probabilities / np.sum(class_probabilities)
    return class_probabilities



def context_aware_probabilities(macroatom, proposed_class,
                                second_best_class, class_probabilities,
                                all_macroatoms,
                                all_macroatoms_preds, sureness_threshold):
    '''all macroatoms - df after splitting atoms to macroatoms and parametrising, macroatom - list of atoms in a macroatom'''
    LOOK_AHEAD = 1.5  # seconds
    if proposed_class == 'Wierzchołkowa fala ostra' or proposed_class == 'Kompleks K':
        absolute_position = macroatom.absolute_position.values[0]
        atoms_in_front_mask = (all_macroatoms.absolute_position > absolute_position) & (all_macroatoms.absolute_position <= (absolute_position + LOOK_AHEAD))
        preds = np.array(all_macroatoms_preds)[atoms_in_front_mask.values]
        proposed_look_ahead_classes = []
        proposed_classes_sureness = []
        for class_probabilities_look_ahead in preds:
            classes_sorted = np.argsort(class_probabilities_look_ahead)
            proposed_class_look_ahead = classes_d_in[classes_sorted[-1]]
            proposed_look_ahead_classes.append(proposed_class_look_ahead)
            proposed_classes_sureness.append(np.max(class_probabilities_look_ahead))

        if 'Spindle (wrzeciono)' in proposed_look_ahead_classes:
            if proposed_class == 'Kompleks K':
                surenesses = np.array(proposed_classes_sureness)
                highest_looks_ahead = np.max(surenesses[np.array(proposed_look_ahead_classes) == 'Spindle (wrzeciono)'])
                # change = highest_looks_ahead - N_CLASSES
                if highest_looks_ahead >= (sureness_threshold * 0.8):
                    class_probabilities = probability_change(class_probabilities, 'Kompleks K', 0.8)
                    class_probabilities = probability_change(class_probabilities, 'Wierzchołkowa fala ostra', -0.8)

        if 'Spindle (wrzeciono)' in proposed_look_ahead_classes:
            if proposed_class == 'Wierzchołkowa fala ostra':
                surenesses = np.array(proposed_classes_sureness)
                highest_looks_ahead = np.max(surenesses[np.array(proposed_look_ahead_classes) == 'Spindle (wrzeciono)'])
                if highest_looks_ahead >= (sureness_threshold * 0.8):
                    class_probabilities = probability_change(class_probabilities, 'Wierzchołkowa fala ostra', -0.8)
                    class_probabilities = probability_change(class_probabilities, 'Kompleks K', 0.8)

        classes_sorted = np.argsort(class_probabilities)
        proposed_class = classes_d_in[classes_sorted[-1]]
        second_best_class = classes_d_in[classes_sorted[-2]]

    return proposed_class, second_best_class, class_probabilities, np.max(class_probabilities)


def atom_sanity_check(macroatom, class_probabilities, all_macroatoms, all_macroatoms_preds,
                      sureness_threshold=0.8, second_best_sureness_thr=0.4, debug=True,
                      only_use_sureness=False):
    '''all macroatoms - df after splitting atoms to macroatoms and parametrising, macroatom - list of atoms in a macroatom, list of dataframes'''
    main_atom_id = np.argmax(macroatom.amplitude.values)
    main_atom = macroatom.iloc[main_atom_id]
    classes_sorted = np.argsort(class_probabilities)

    proposed_class = classes_d_in[classes_sorted[-1]]
    second_best_class = classes_d_in[classes_sorted[-2]]
    sureness_best = np.max(class_probabilities)
    # proposed_class, second_best_class, class_probabilities, sureness_best = context_aware_probabilities(macroatom, proposed_class,
    #                                                                                    second_best_class, class_probabilities,
    #                                                                                    all_macroatoms,
    #                                                                                    all_macroatoms_preds, sureness_threshold)


    heuristic_test = (classes_sanity_heuristics[proposed_class](main_atom, class_probabilities) or only_use_sureness)

    if (proposed_class != 'None') and (sureness_best >= sureness_threshold) and heuristic_test:
        return proposed_class, sureness_best, class_probabilities
    # elif (proposed_class == 'None') and classes_sanity_heuristics[second_best_class](main_atom, class_probabilities) and (sureness_second_best > second_best_sureness_thr):
    #     print("\n\nForcing atom in: None -> {}\n"
    #               "f \t{} \n"
    #               "amp \t{}\n"
    #               "amp z \t{}\n"
    #               "w \t{}\n ".format(second_best_class,
    #                                                                main_atom[ATOM_FEATURES.index('frequency')],
    #                                                                main_atom[ATOM_FEATURES.index('amplitude')],
    #                                                                main_atom[ATOM_FEATURES.index('amplitude_z_score')],
    #                                                                main_atom[ATOM_FEATURES.index('width')],
    #                                                                ))
    #     return second_best_class, sureness_second_best
    else:
        if proposed_class != 'None':
            if debug:
                cycles = main_atom['width'] / (1.0 / main_atom['frequency'])
                print("\n\nDenying atom: {}\n"
                      "f \t{} \n"
                      "amp \t{}\n"
                      "amp z \t{}\n"
                      "w \t{}\n"
                      "cycles: \t {}\n"
                      "sureness:\t{}\n"
                      " failed sanity check".format(proposed_class,
                                                                       main_atom['frequency'],
                                                                       main_atom['amplitude'],
                                                                       main_atom['amplitude_z_score'],
                                                                       main_atom['width'],
                                                                       cycles,
                                                                       sureness_best,
                                                                       ))
                # import IPython
                # IPython.embed()
        proposed_class = 'None'
        sureness = 0
        return proposed_class, sureness, class_probabilities
