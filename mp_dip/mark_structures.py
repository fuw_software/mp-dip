import glob
from copy import copy
import numpy as np
import argparse
from tqdm import tqdm
from tqdm.contrib.concurrent import process_map
import pickle
from mp_dip.utils.multivariate_atom_analysis import add_dipole_label
from mp_dip.utils.data_preprocessing_utils import convert_montage_channel_nr_to_raw
from mp_dip.obci_readmanager.signal_processing.tags import tags_file_writer as tags_writer

from mp_dip.utils.sanity_checking import sanity_check_spindle, sanity_check_spikes_slow_politech004, \
    sanity_check_alpha, sanity_check_swa
from mp_dip.utils.utils import TAG_DEFS, classes_d, get_mp_book_paths, get_atoms_with_primary_parametrisation, split_to_macroatoms
import os


class RawChannelStandin():
    def __init__(self, raw):
        self.ch_names = copy(raw.ch_names)


def analyze_macroatoms(macroatom):
    main_atom_id = np.argmax(macroatom.amplitude.values)
    # main_atom_id = np.where(macroatom.channel_name == 'C3')[0][0]
    main_atom = macroatom.iloc[main_atom_id]
    # todo atom can be only one thing now?
    if sanity_check_spindle(main_atom, None):
        tagname = 'Spindle (wrzeciono)'
        return [macroatom, tagname]

    # if sanity_check_swa(main_atom, None):
    #     tagname = 'SWA'
    #     return [macroatom, tagname]

    # if sanity_check_alpha(main_atom, None):
    #     tagname = 'alpha'
    #     return [macroatom, tagname]


def create_tag(label, macroatom, nr, preanalysed_raw, raw):
    start = macroatom.iloc[0]["absolute_position"] - macroatom.iloc[0][
        "width"] / 2
    stop = macroatom.iloc[0]["absolute_position"] + macroatom.iloc[0][
        "width"] / 2
    max_amp = np.max(macroatom['amplitude'].values)
    frequency = macroatom['frequency'].values[0]
    # todo decide what to do with this later
    amp_threshold = max_amp * 0.5 * 0  # want to mark on every channel for now to then use SNR analysis

    amp_mask = (macroatom['amplitude'].values > amp_threshold)
    # amp_mask = (macroatom['snr'].values > -30)  # -30 db is the visibility treshold?/
    # channels where structure is strong enough to count as visible
    channels = np.arange(0, len(preanalysed_raw.ch_names))[amp_mask]

    iteration = macroatom['iteration'].iloc[0]
    common_desc = {
                   'example_nr': str(nr),
                   'frequency': str(frequency),
                   'amplitude_P2P': str(max_amp),
                   'width': str(macroatom['width'].values[0]),
                   'iteration': iteration,
                   }

    # don't add dipole labels to "nones" - takes to long

    add_dipole_label(common_desc, macroatom.iloc[0])

    tags_detected = []
    for channel in channels:

        snr = macroatom.snr.values[channel]
        desc = common_desc.copy()

        channel_for_tag = convert_montage_channel_nr_to_raw(raw,
                                                            preanalysed_raw,
                                                            channel)
        channel_montaged = preanalysed_raw.ch_names[channel]
        desc['referenceAsReadable'] = str(channel_montaged)
        desc['snr'] = str(snr)

        tag = {'channelNumber': channel_for_tag,
               'start_timestamp': start,
               'end_timestamp': stop,
               'name': label,
               'desc': desc,
               }
        tags_detected.append(tag)
    return tags_detected


def main():
    np.set_printoptions(precision=3)
    np.set_printoptions(suppress=True)
    np.set_printoptions(linewidth=120)
    parser = argparse.ArgumentParser(description="Mark_using nn")
    parser.add_argument('-d', '--mp-dir', nargs='?', type=str, help='MP Output directory')
    parser.add_argument('-m', '--montage', action='store', help='Montages: none, ears, transverse', default='none')
    parser.add_argument('-mp', '--mp-type', action='store', help='mp type: smp, mmp3', default='smp')
    parser.add_argument('-o', '--output',  action='store', help='tags_output', default='')
    parser.add_argument('-s', '--save-marked-macroatoms',  action='store',
                        help='Saves a pickle of selected macroatoms along tags',
                        default='y')
    parser.add_argument('-u', '--unique-file-names', action='store', type=str,
                        default='y',
                        help='are raw filenames unique? If no MP folder will contain subfolders')

    parser.add_argument('files', nargs='+', metavar='file', help="path to *.raw")

    namespace = parser.parse_args()
    if len(namespace.files) == 1:
        files_to_work = glob.glob(namespace.files[0])
    else:
        files_to_work = namespace.files
    print(files_to_work)
    for file in files_to_work:

        if namespace.unique_file_names == 'y':
            output_dir = namespace.output
        else:
            patiend_id = os.path.basename(os.path.abspath(os.path.join(os.path.dirname(file), '..')))
            output_dir = os.path.join(namespace.output, patiend_id)
        file_out_name_base = os.path.join(output_dir,
                                          os.path.splitext(
                                              os.path.basename(
                                                  file))[
                                              0])
        # check for overwriting
        if os.path.exists(file_out_name_base + '.tag'):
            print("Will not everwrite ", file_out_name_base + '.tag', 'skipping')
            continue



        # check if dipoles exist

        path_to_book, path_to_book_params = get_mp_book_paths(namespace.mp_dir, file,
                                                              namespace.montage, namespace.mp_type,
                                                              unique_file_names=namespace.unique_file_names == 'y')
        dipole_path = os.path.join(os.path.dirname(path_to_book),
                                   'dipole_fits',
                                   os.path.basename(path_to_book) + '_dipole_fits.pickle')
        if not os.path.exists(dipole_path):
            print("No dipole fits for ", file, 'skipping', "Dipole path:", dipole_path)
            continue

        if not os.path.exists(path_to_book_params):
            print("no mp for ", file, 'skipping', path_to_book_params)
            continue

        ##############################

        try:
            atoms, raw, preanalysed_raw, mp_params = get_atoms_with_primary_parametrisation(
                file, None,
                namespace.montage,
                namespace.mp_dir,
                namespace.mp_type,
                '', max_iter=175, unique_file_names=namespace.unique_file_names == 'y', copy=True)
            preanalysed_raw_standin = RawChannelStandin(preanalysed_raw)
            raw_standin = RawChannelStandin(raw)
            del raw
            del preanalysed_raw
            preanalysed_raw = preanalysed_raw_standin
            raw = raw_standin
        except FileNotFoundError as e:
            print(e)
            continue

        macroatoms = split_to_macroatoms(atoms)



        tagged_macroatoms = process_map(analyze_macroatoms, macroatoms,
                                        desc='naming, classifying macroatoms', chunksize=1024)

        # for debugging
        # tagged_macroatoms = map(analyze_macroatoms, macroatoms)


        tagged_macroatoms = list(filter(lambda x: x is not None, tqdm(tagged_macroatoms, desc='filtering empty tags')))

        def create_tags_for_selected_macroatoms(item):
            macroatom, tagname = item
            tags_this = create_tag(tagname, macroatom, macroatom['mmp3_atom_ids'].values[0], preanalysed_raw, raw)
            return tags_this

        tags = sum(list(map(create_tags_for_selected_macroatoms, tqdm(tagged_macroatoms, desc='creating tags'))), [])
        #############################3


        # tags = []
        # tagged_macroatoms = []
        #
        # for macroatom_nr, macroatom in enumerate(tqdm(macroatoms, desc='analyzing')):
        #     main_atom_id = np.argmax(macroatom.amplitude.values)
        #     main_atom = macroatom.iloc[main_atom_id]
        #     if sanity_check_spindle(main_atom, None):
        #         tagname = 'Spindle (wrzeciono)'
        #         tags_this = create_tag(tagname, macroatom, macroatom_nr, preanalysed_raw, raw)
        #         tags.extend(tags_this)
        #         if namespace.save_marked_macroatoms == 'y':
        #             tagged_macroatoms.append([macroatom, tagname])

            # if sanity_check_spikes_slow_politech004(main_atom, None):
            #     tagname = 'epileptic'
            #     tags_this = create_tag(tagname, macroatom, macroatom_nr, preanalysed_raw, raw)
            #     tags.extend(tags_this)
            #     if namespace.save_marked_macroatoms == 'y':
            #         tagged_macroatoms.append([macroatom, tagname])

        def tag_sort_key(tag):
            return tag['start_timestamp']

        tags.sort(key=tag_sort_key)
        tags_all_filtered = []
        for tag in tags:
            if tag['start_timestamp'] > 0:
                tags_all_filtered.append(tag)


        if namespace.unique_file_names == 'y':
            output_dir = namespace.output
        else:
            patiend_id = os.path.basename(os.path.abspath(os.path.join(os.path.dirname(file), '..')))  # legacy
            # patiend_id = os.path.basename(os.path.abspath(os.path.join(os.path.dirname(file))))  # n400 slowa stuff
            output_dir = os.path.join(namespace.output, patiend_id)

        os.makedirs(output_dir, exist_ok=True)
        file_out_name_base = os.path.join(output_dir,
                                          os.path.splitext(
                                              os.path.basename(
                                                  file))[
                                              0])
        writer = tags_writer.TagsFileWriter(file_out_name_base + '.tag',
                                            p_defs=TAG_DEFS)
        for tag in tags_all_filtered:
            writer.tag_received(tag)
        writer.finish_saving(0.0)

        if namespace.save_marked_macroatoms == 'y':
            with open(file_out_name_base + '.pickle', 'wb') as macroatoms_save_file:
                pickle.dump(tagged_macroatoms, macroatoms_save_file)

# https://github.com/raphaelvallat/yasa


if __name__ == '__main__':
    # turn off gpu for now
    import os
    os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
    main()


   # python mark_structures_sanity_check_lite.py -d D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\mp -m none -mp mmp1 -o D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\tags_experiments\tags_test_temp_sanity_check_lite D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\*.raw
   # python mark_structures_sanity_check.py -d /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/mp -m none -mp mmp3 -o /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/tags_experiments/tags_sanity_check_only_higher_EMPI_TEST -c /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/feature_cache_dip_avr_centers -c /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/feature_cache_dip_avr_centers/ /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/e1002348.raw
   # python mark_structures_sanity_check.py -d /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_stare_do_testu/converted/mp -m none -mp mmp1 -o /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_stare_do_testu/converted/tags_experiments/tags_sanity_check_only_higher_new_emp -c /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_stare_do_testu/converted/feature_cache_dip_avr_centers/ /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_stare_do_testu/converted/*.raw
   # python mark_structures_sanity_check_lite.py -d /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/mp_v1.0.x/ -m none -mp mmp1 -o /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/tags_experiments/tagged_sleep_spindles2 /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/*.bin
   # python mark_structures_sanity_check_lite.py -d /repo/coma/dane_pomiarowe_nocne_do_topografii_wrzecion/mp_v1.0.x/ -m none -mp mmp1 -o /repo/coma/dane_pomiarowe_nocne_do_topografii_wrzecion/tags_experiments/tagged_sleep_spindles2 /repo/coma/dane_pomiarowe_nocne_do_topografii_wrzecion/*.raw

   # python mark_structures_sanity_check_lite.py -d /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/mp/ -m none -mp mmp1 -o /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/tags_experiments_yasa/tagged_sleep_spindles_MMP /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/*.raw
   # python mark_structures_sanity_check_lite.py -d /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/mp/ -m none -mp mmp1 -o /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/tags_experiments_yasa/tagged_sleep_spindles_MMP /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/*.raw
   # python mark_structures_sanity_check_lite.py -d /repo/coma/NOWY_OPUS_COMA/DANE_POLITECHNIKA/DANE_PACJENTOW_CLEAN/DANE/MP/ -u n -m none -mp mmp1 -o /repo/coma/NOWY_OPUS_COMA/DANE_POLITECHNIKA/DANE_PACJENTOW_CLEAN/DANE/tags_experimental_all_data_normalized_amplitude/ /repo/coma/NOWY_OPUS_COMA/DANE_POLITECHNIKA/DANE_PACJENTOW_CLEAN/DANE/PW-EEG*/*/*.raw
# yasa_tagger.py -m none -o /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/tags_experiments_yasa/tagged_sleep_spindles_YASA /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/*.raw

   # nice python mark_structures_sanity_check_lite.py -d /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/mp -u y -m none -mp mmp1 -o /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/tags_experiments/tags_spindles "/repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/01-05-0014 PSG.raw" "/repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/01-05-0015 PSG.raw" "/repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/01-05-0016 PSG.raw" "/repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/01-05-0017 PSG.raw" "/repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/01-05-0019 PSG.raw" "/repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/01-05-0020 PSG.raw" "/repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/01-05-0021 PSG.raw" "/repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/01-05-0022 PSG.raw" "/repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/01-05-0023 PSG.raw" "/repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/01-05-0024 PSG.raw" "/repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/01-05-0025 PSG.raw" "/repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/01-05-0026 PSG.raw"
   # nice python mark_structures_sanity_check_lite.py -d /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/mp -u y -m none -mp mmp1 -o /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/tags_experiments/tags_spindles_no_dipole_gof_location /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/*.raw

    # nice python mark_structures_sanity_check_lite.py -d /repo/coma/NOWY_OPUS_COMA/DANE_NOWOWIEJSKA/KONTROLA/converted/MP -u n -m none -mp mmp1 -o /repo/coma/NOWY_OPUS_COMA/DANE_NOWOWIEJSKA/KONTROLA/converted/tags_experiments/tags_swa_no_zscore /repo/coma/NOWY_OPUS_COMA/DANE_NOWOWIEJSKA/KONTROLA/converted/*.raw &
    # nice python mark_structures_sanity_check_lite.py -d /repo/coma/NOWY_OPUS_COMA/DANE_NOWOWIEJSKA/SOMNAMBULIZM/converted/MP -u n -m none -mp mmp1 -o /repo/coma/NOWY_OPUS_COMA/DANE_NOWOWIEJSKA/SOMNAMBULIZM/converted/tags_experiments/tags_swa_no_score /repo/coma/NOWY_OPUS_COMA/DANE_NOWOWIEJSKA/SOMNAMBULIZM/converted/*.raw &
