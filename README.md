# Installation

Works on most modern Linux distributions. Matching pursuit implementation empi (github.com/develancer/empi) is compiled for Linux, statically; currently (2022.11) there are no recent builds for Windows or Mac.

- Install Python3.10

pyenv and virtualenv useage is strongly advised.

- Install requirements using

`pip install -r requirements.txt`

- install this package

`pip install -e .`

# Usage

Pipeline:
- if needed: convert_edf_to_raw
- decompose_raw - (can use multi_ssh_processing) to decompose signals to MP atoms using empi
- fit_dipole_to_atoms (can use multi_ssh_processing) - to fit dipole to non transverse montage atoms
- convert_mass_edf_annotations_to_tags.py - to convert MASS hypnograms to useable form
- convert_moda_consensus_files_to_tags.py - to convert MODA tags to useable form
- mark_structures_sanity_check_lite.py - detect spindles
- research/tagged_spindle_dipole_analysis.py - to draw 3d heads and sleep profiles and do spindle gradient calculations
- filter_tags_ground_truth_accordance.py - to seperate TP and FP detections
- research_tag_features_folder_compare.py - to compare histograms of FP and TP spindles
- calculate_tag_parameters.py - to calculate F1, histograms, SNR vs F1

This package uses Svarog native signal and annotations format---floating point binary files with xml metadata and xml annotations (tags). Readable using any xml reader and numpy or obci-readmanager pypi package. Svarog is an Open Source application for EEG recording and analysis, available at `http://svarog.pl`. 

# Example/Tutorial

## Preprocessing

As an example, suppose that we have data files in EDF format in the folder:

`/repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/original_data/`

First we convert it to the Svarog native format:

`convert_edf_to_raw -m /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/original_data/*PSG.edf -o /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/`

Convert sleep annotations to tags:

`convert_mass_annotations /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/original_data/*Base.edf -o /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/`

Decompose files using matching pursuit (may take few hours per overnight sleep recording):

`decompose_raw -m none -mp mmp1 -u y /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/*.raw -o /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/mp`

This will create .db files --- MMP decomposition books, containing parameters of all the waveforms (time-frequency) fitted to the signal in sqlite database, viewable interactively using Svarog.

Calculate dipole positions for the time-frequency MP atoms, this might take dozens of hours per overnight recording:

`fit_dipoles_to_atoms -d /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/mp -u y -m none -mp mmp1 /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/*.raw `

This will create pandas dataframe pickles with time-frequency-dipole_location atoms in `/repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/mp/none_mmp1/dipole_fits` folder. 
They are also viewable using `atom_visualiser` script:

`atom_visualiser -u y -mp mmp1 -m none -d /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/mp /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/01-01-0001\ PSG.raw`

Left - right arrows to switch EEG signal pages, Page-Up, Page-Down - switches previewed channel, plus, minus on numpad - scales signal.
Circles visualise MP atoms, size - MP atom frequency.
Right click - visualises dipole of the associated atom. Left click - switches the atom reconstruction on and off.
Filters on the right allow to highlight structurs, like spindles, alpha, swa. Definitions are in
`mp_dip.utils.sanity_checking.sanity_check_spindle` module.



## Marking structures

Time frequency atoms are used to generate spindles xml tags and pickles, which are also viewable in Svarog

To generate tags and pickles use:

`mark_structures -u y -mp mmp1 -m none -d /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/mp /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/*.raw -o /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/tags_prediction/`

Pickles contain time-frequency-position MP atoms properties of the detected graphoelements (for now - spindles) as list of pandas dataframes.

Please keep in mind, that for a full overnight recording there may be a lot of tags, so Svarog will take a while reading them. You can reduce the amount of tags to most visible ones, based on the local signal-to-noise ratio (SNR) using:

`filter_tags -s=-30 /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/tags_prediction/ /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/tags_prediction/filtered_-30`

to make the files open faster in Svarog.


By modifying `mark_structures.py` `analyze_macroatoms` function you can add tagging of any other graphoelements, based on MP-dip atom parameters.

We can use these pickles and tags to create sleep profiles and calculate spindle gradient across whole recordings, or just for given sleep stages:

## Structures spatio-frequency-temporal profiles

```
analyse_spindles_profiles -f "" -t /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/ /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/tags_prediction/*PSG.pickle
analyse_spindles_profiles -f "w" -t /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/ /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/tags_prediction/*PSG.pickle
analyse_spindles_profiles -f "r" -t /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/ /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/tags_prediction/*PSG.pickle
analyse_spindles_profiles -f "1" -t /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/ /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/tags_prediction/*PSG.pickle
analyse_spindles_profiles -f "2" -t /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/ /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/tags_prediction/*PSG.pickle
analyse_spindles_profiles -f "3" -t /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/ /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/tags_prediction/*PSG.pickle
```

This will create summaries and images in `/repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/tags_prediction/img_tag_/` folder.
Then you can also analyse spatial and time distribution of these graphoelements using `analyse_spindles_profiles` and setting appropriate `ANALYZED_TAG_NAME` input argument.

All these scripts have built-in help, which will be displayed if launched with `--help` parameter.

## Comparing with MODA

First we need to convert MODA files. Let's assume that moda annotations from `https://github.com/klacourse/MODA_GC/tree/master/output/exp/annotFiles`
are in `/repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/annotFiles/` folder. Let's convert them:

`convert_moda_consensus /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/annotFiles/*.txt -o /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/tags_moda`


Now we can compare set of tags:

`calculate_tags_comparison -s="-200;-50;-40;-30;-20;-10;-5" -l=0.5 -c=C3 /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/tags_moda /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/tags_prediction`

This will create folders `/repo/coma/NOWY_OPUS_COMA/MASS_DATASET/example_tutorial/converted/tags_prediction/tag_params_compare_summary_channel_C3_snr_-60.0_min_length_0.5` for every
SNR treshold with tags comparison summaries for given SNR treshold, and the last (-5) will contain also the overall SNR treshold summary.




