import glob

import numpy as np
import argparse

import yasa
from mp_dip.utils.data_preprocessing_utils import convert_montage_channel_nr_to_raw
from tqdm import tqdm
import pickle

from mp_dip.obci_readmanager.signal_processing.tags import tags_file_writer as tags_writer
from mp_dip.utils.utils import TAG_DEFS, preanalyse_raw
from mp_dip.obci_readmanager.signal_processing.read_manager import ReadManager


def create_tag_from_yase(pandas_yase_row, raw, preanalysed_raw, label):
    channel = pandas_yase_row.IdxChannel
    channel_for_tag = convert_montage_channel_nr_to_raw(raw,
                                                        preanalysed_raw,
                                                        channel)
    channel_montaged = preanalysed_raw.ch_names[channel]

    frequency = pandas_yase_row.Frequency
    max_amp = pandas_yase_row.Amplitude
    width = pandas_yase_row.End - pandas_yase_row.Start

    start = pandas_yase_row.Start
    stop = pandas_yase_row.End

    desc = {'frequency': str(frequency),
            'amplitude_P2P': str(max_amp * 2),
            'width': str(width),
            }

    desc['referenceAsReadable'] = str(channel_montaged)

    tag = {'channelNumber': channel_for_tag,
           'start_timestamp': start,
           'end_timestamp': stop,
           'name': label,
           'desc': desc,
           }
    return [tag]


def read_raw(path_to_raw, tags=None):
    if path_to_raw[-3:] in ['raw', 'bin']:
        rm = ReadManager(path_to_raw[:-3] + 'xml', path_to_raw, tags)
        raw = rm.get_mne_raw()

    elif path_to_raw[-3:] in ["edf"]:
        raw = mne.io.read_raw_edf(path_to_raw, preload=True)
        maping = {}
        for chnl in raw.ch_names:
            if chnl.startswith('EEG'):
                maping[chnl] = 'eeg'
            elif chnl.startswith('ECG'):
                maping[chnl] = 'ecg'
            elif chnl.startswith('EOG'):
                maping[chnl] = 'eog'
            elif chnl.startswith('Resp'):
                maping[chnl] = 'resp'
            elif chnl.startswith('EMG'):
                maping[chnl] = 'emg'
            else:
                maping[chnl] = 'misc'
        raw.set_channel_types(maping)
        raw = raw.drop_channels(["EEG A1", "EEG A2"])
    else:
        raise Exception("Unknown file type")
    return raw


def main():
    np.set_printoptions(precision=3)
    np.set_printoptions(suppress=True)
    np.set_printoptions(linewidth=120)
    parser = argparse.ArgumentParser(description="Mark_using nn")
    parser.add_argument('-m', '--montage', action='store', help='Montages: none, ears, transverse', default='none')
    parser.add_argument('-o', '--output',  action='store', help='tags_output', default='')


    parser.add_argument('files', nargs='+', metavar='file', help="path to *.raw")

    namespace = parser.parse_args()
    if len(namespace.files) == 1:
        files_to_work = glob.glob(namespace.files[0])
    else:
        files_to_work = namespace.files
    print(files_to_work)
    for file in files_to_work:

        os.makedirs(namespace.output, exist_ok=True)
        file_out_name_base = os.path.join(namespace.output,
                                          os.path.splitext(
                                              os.path.basename(
                                                  file))[
                                              0])
        tag_file_path = file_out_name_base + '.tag'

        if os.path.exists(tag_file_path):
            print('tag already exists, skipping', tag_file_path)
            continue

        montage = namespace.montage
        raw = read_raw(file)
        print('preanalyse raw')
        preanalysed_raw = preanalyse_raw(raw, montage, copy=True)

        print('yasa detect start')
        sp = yasa.spindles_detect(preanalysed_raw)
        print('yasa detect finish')
        tags = []
        if sp is not None:

            all_spindles = sp.summary()
            for macroatom_nr, macroatom in tqdm(all_spindles.iterrows(), desc='analyzing'):
                    tagname = 'Spindle (wrzeciono)'
                    tags_this = create_tag_from_yase(macroatom, raw, preanalysed_raw, tagname)
                    tags.extend(tags_this)

        del raw
        del preanalysed_raw

        def tag_sort_key(tag):
            return tag['start_timestamp']

        tags.sort(key=tag_sort_key)
        tags_all_filtered = []
        for tag in tags:
            if tag['start_timestamp'] > 0:
                tags_all_filtered.append(tag)

        writer = tags_writer.TagsFileWriter(tag_file_path,
                                            p_defs=TAG_DEFS)
        for tag in tags_all_filtered:
            writer.tag_received(tag)
        writer.finish_saving(0.0)

if __name__ == '__main__':
    # turn off gpu for now
    import os
    os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
    main()


   # python yasa_tagger.py -m none -o /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/tags_experiments/tagged_sleep_spindles_YASA /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/*.bin